﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 8. 字符串转换整数 (atoi)
/// </summary>
public class MyAtoiSolution
{
    private const char zero = '0';
    private const char one = '1';
    private const char two = '2';
    private const char three = '3';
    private const char four = '4';
    private const char five = '5';
    private const char six = '6';
    private const char seven = '7';
    private const char eight = '8';
    private const char nine = '9';
    private const char negativeSymbol = '-';
    private const char positiveSymbol = '+';
    public void Test()
    {
        var s = "words and 987";
        Console.WriteLine(MyAtoi(s));
        var s0 = "4193 with words";
        Console.WriteLine(MyAtoi(s0));
        var s1 = "-91283472332";
        Console.WriteLine(MyAtoi(s1));
        var s2 = "   -42";
        Console.WriteLine(MyAtoi(s2));
        var s3 = "+-12";
        Console.WriteLine(MyAtoi(s3));
        var s4 = "21474836460";
        Console.WriteLine(MyAtoi(s4));
        var s5 = "-5-";
        Console.WriteLine(MyAtoi(s5));
        var s6 = "00000-42a1234";
        Console.WriteLine(MyAtoi(s6));
        var s7 = "  +  413";
        Console.WriteLine(MyAtoi(s7));
    }
    public int MyAtoi(string s)
    {
        bool? isPositive = null;
        var ns = new List<char>();
        for (int i = 0; i < s.Length; i++)
        {
            var cn = s[i];
            if (cn == ' ')
            {
                if (ns.Count() == 0 && isPositive == null) continue;
                else break;
            }
            else
            {
                if (ns.Count() == 0)
                {
                    switch (cn)
                    {
                        case negativeSymbol:
                            if (isPositive != null) return 0;
                            isPositive = false;
                            break;
                        case positiveSymbol:
                            if (isPositive != null) return 0;
                            isPositive = true;
                            break;
                        case zero:
                        case one:
                        case two:
                        case three:
                        case four:
                        case five:
                        case six:
                        case seven:
                        case eight:
                        case nine:
                            ns.Add(cn);
                            break;
                        default:
                            return 0;
                    }
                }
                else
                {
                    bool isEnd = false;
                    switch (cn)
                    {
                        case negativeSymbol:
                        case positiveSymbol:
                            isEnd = true;
                            break;
                        case zero:
                        case one:
                        case two:
                        case three:
                        case four:
                        case five:
                        case six:
                        case seven:
                        case eight:
                        case nine:
                            ns.Add(cn);
                            break;
                        default:
                            isEnd = true;
                            break;
                    }
                    if (isEnd) break;
                }
            }
        }
        if (ns.Count() == 0) return 0;
        var nss = string.Join("", ns);
        var nsNum = double.Parse(nss);
        if (isPositive == null || isPositive == true)
        {
            if (nsNum >= int.MaxValue) return int.MaxValue;
            return (int)nsNum;
        }
        else
        {
            if (nsNum < int.MinValue) return int.MinValue;
            return (int)nsNum * -1;
        }
    }
}
