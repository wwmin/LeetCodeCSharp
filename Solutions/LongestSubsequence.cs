namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 1330. 最长定差子序列
/// </summary>
public class LongestSubsequenceSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), 1, 4 };
            //yield return new object[] { StringTo<int[]>("[1,3,5,7]").ToArray(), 1, 1 };
            yield return new object[] { StringTo<int[]>("[1,5,7,8,5,3,4,2,1]").ToArray(), -2, 4 };

        }
    }
    //动态规划解法
    [Data]
    public int LongestSubsequence(int[] arr, int difference)
    {
        int ans = 0;
        Dictionary<int, int> dp = new Dictionary<int, int>();
        foreach (var v in arr)
        {
            int prev = dp.ContainsKey(v - difference) ? dp[v - difference] : 0;
            if (dp.ContainsKey(v))
            {
                dp[v] = prev + 1;
            }
            else
            {
                dp.Add(v, prev + 1);
            }
            ans = Math.Max(ans, dp[v]);
        }
        return ans;
    }

    //[Data]
    //暴力解法
    //此解超时
    public int LongestSubsequence2(int[] arr, int difference)
    {
        int n = arr.Length;
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            if (n - i <= ans) break;
            List<int> temp_arr = new List<int> { arr[i] };
            for (int j = i; j < n - 1; j++)
            {
                var current_value = arr[j];
                var next_value = arr[j + 1];
                if (next_value - current_value == difference)
                {
                    temp_arr.Add(next_value);
                }
                else
                {
                    int k = j + 2;
                    while (k < n)
                    {
                        var last_value = arr[k];
                        if (last_value - current_value == difference)
                        {
                            temp_arr.Add(last_value);
                            current_value = last_value;
                        }
                        k++;
                    }
                    if (temp_arr != null && temp_arr.Count > ans)
                    {
                        ans = temp_arr.Count;
                    }
                    break;
                }
            }
            if (temp_arr != null && temp_arr.Count > ans)
            {
                ans = temp_arr.Count;
            }
        }

        return ans;
    }
}