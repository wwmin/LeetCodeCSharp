namespace Solutions;
/// <summary>
/// 292. Nim 游戏
/// </summary>
public class CanWinNimSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, false };
            yield return new object[] { 1, true };
            yield return new object[] { 2, true };
        }
    }

    [Data]
    public bool CanWinNim(int n)
    {
        return n % 4 != 0;
    }
}