namespace Solutions;
/// <summary>
/// 2116. 差的绝对值为 K 的数对数目
/// </summary>
public class CountKDifferenceSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,2,1]").ToArray(), 1, 4 };
            yield return new object[] { StringTo<int[]>("[1,3]").ToArray(), 3, 0 };
            yield return new object[] { StringTo<int[]>("[3,2,1,5,4]").ToArray(), 2, 3 };
        }
    }

    [Data]
    public int CountKDifference(int[] nums, int k)
    {
        int ans = 0;
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length; i++)
        {
            if (map.ContainsKey(nums[i] - k))
            {
                ans += map[nums[i] - k];
            }
            if (map.ContainsKey(nums[i] + k))
            {
                ans += map[nums[i] + k];
            }
            if (map.ContainsKey(nums[i]))
            {
                map[nums[i]]++;
            }
            else
            {
                map.Add(nums[i], 1);
            }
        }
        return ans;
    }

    public int CountKDifference_1(int[] nums, int k)
    {
        int ans = 0;
        for (int i = 0; i < nums.Length - 1; i++)
        {
            for (int j = i + 1; j < nums.Length; j++)
            {
                if (Math.Abs(nums[i] - nums[j]) == k) ans++;
            }
        }
        return ans;
    }
}