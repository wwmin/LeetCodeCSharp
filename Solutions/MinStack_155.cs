namespace Solutions;
/// <summary>
/// 155. 最小栈
/// </summary>
public class MinStack
{

    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"MinStack\",\"push\",\"push\",\"push\",\"getMin\",\"pop\",\"top\",\"getMin\"]", "[[],[-2],[0],[-3],[],[],[],[]]", "[null,null,null,null,-3,null,0,-2]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
    }
    NodeWithMin head = null;
    public MinStack()
    {
        //head = new NodeWithMin(0, 0);
    }

    public void Push(int val)
    {
        if (head == null)
        {
            head = new NodeWithMin(val, val);
        }
        else
        {
            head = new NodeWithMin(val, Math.Min(head.min, val), head);
        }
    }

    public void Pop()
    {
        head = head.next;
    }

    public int Top()
    {
        return head.val;
    }

    public int GetMin()
    {
        return head.min;
    }

    class NodeWithMin
    {
        public int val;
        public int min;
        public NodeWithMin next;
        public NodeWithMin(int val, int min) : this(val, min, null)
        {
        }

        public NodeWithMin(int val, int min, NodeWithMin node)
        {
            this.val = val;
            this.min = min;
            this.next = node;
        }
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.Push(val);
 * obj.Pop();
 * int param_3 = obj.Top();
 * int param_4 = obj.GetMin();
 */