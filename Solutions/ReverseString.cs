namespace Solutions;
/// <summary>
/// 344. 反转字符串
/// </summary>
public class ReverseStringSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<char[]>("[\'h\',\'e\',\'l\',\'l\',\'o\']").ToArray(), StringTo<char[]>("[\'o\',\'l\',\'l\',\'e\',\'h\']").ToArray() };
            yield return new object[] { StringTo<char[]>("[\'H\',\'a\',\'n\',\'n\',\'a\',\'h\']").ToArray(), StringTo<char[]>("[\'h\',\'a\',\'n\',\'n\',\'a\',\'H\']").ToArray() };
        }
    }

    [Data]
    public void ReverseString(char[] s)
    {
        int n = s.Length;
        if (n == 0) return;
        int left = 0;
        int right = n - 1;
        while (left <= right)
        {
            char temp = s[left];
            s[left] = s[right];
            s[right] = temp;
            left++;
            right--;
        }
    }
}