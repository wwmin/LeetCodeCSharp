namespace Solutions;
/// <summary>
/// 1827. 最少操作使数组递增
/// difficulty: Easy
/// https://leetcode.cn/problems/minimum-operations-to-make-the-array-increasing/
/// </summary>
public class MinOperations_1827_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,1,1]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[1,5,2,4,1]").ToArray(), 14 };
            yield return new object[] { StringTo<int[]>("[8]").ToArray(), 0 };
        }
    }

    [Data]
    public int MinOperations(int[] nums)
    {
        var count = 0;
        for (int i = 1; i < nums.Length; i++)
        {
            if (nums[i] <= nums[i - 1])
            {
                count += nums[i - 1] - nums[i] + 1;
                nums[i] = nums[i - 1] + 1;
            }
        }
        return count;
    }
}