namespace Solutions;
/// <summary>
/// 401. 二进制手表
/// </summary>
public class ReadBinaryWatchSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, StringTo<List<string>>("[\"0:01\",\"0:02\",\"0:04\",\"0:08\",\"0:16\",\"0:32\",\"1:00\",\"2:00\",\"4:00\",\"8:00\"]") };
            yield return new object[] { 9, StringTo<List<string>>("[]") };
        }
    }

    [Data]
    public IList<string> ReadBinaryWatch(int turnedOn)
    {
        IList<string> data = new List<string>();
        //直接遍历 0:00 -> 12:00 每个时间有多少1
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 60; j++)
            {
                if (count1(i) + count1(j) == turnedOn)
                {
                    data.Add(i.ToString() + ":" + (j < 10 ? "0" + j.ToString() : j.ToString()));
                }
            }
        }
        return data;
    }

    //计算二级制1的个数
    private int count1(int num)
    {
        int count = 0;
        while (num > 0)
        {
            count++;
            num = num & (num - 1);
        }
        return count;
    }
}