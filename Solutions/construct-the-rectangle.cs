namespace Solutions;
/// <summary>
/// 492. 构造矩形
/// </summary>
public class ConstructRectangleSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, StringTo<int[]>("[2,2]").ToArray() };
            yield return new object[] { 37, StringTo<int[]>("[37,1]").ToArray() };
            yield return new object[] { 122122, StringTo<int[]>("[427,286]").ToArray() };
        }
    }

    [Data]
    public int[] ConstructRectangle(int area)
    {
        int w = (int)Math.Sqrt(area);
        while (area % w != 0)
        {
            w--;
        }
        return new[] { area / w, w };
    }
}