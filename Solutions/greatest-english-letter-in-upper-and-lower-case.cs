namespace Solutions;
/// <summary>
/// 2309. 兼具大小写的最好英文字母
/// difficulty: Easy
/// https://leetcode.cn/problems/greatest-english-letter-in-upper-and-lower-case/
/// </summary>
public class GreatestLetter_2309_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "lEeTcOdE", "E" };
            yield return new object[] { "arRAzFif", "R" };
            yield return new object[] { "AbCdEfGhIjK", "" };
        }
    }

    [Data]
    public string GreatestLetter(string s)
    {
        bool[] cl = new bool[26];
        bool[] cu = new bool[26];
        for (int i = 0; i < s.Length; i++)
        {
            if (char.IsLower(s[i]))
            {
                cl[s[i] - 'a'] = true;
            }
            else
            {
                cu[s[i] - 'A'] = true;
            }
        }
        for (int i = 25; i >= 0; i--)
        {
            if (cl[i] && cu[i])
            {
                return ((char)('A' + i)).ToString();
            }
        }
        return "";
    }
}