﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 12. 整数转罗马数字
/// </summary>
public class IntToRomanSolution
{
    private static readonly Dictionary<int, string> RN = new()
    {
        { 1, "I" },
        { 4, "IV" },
        { 5, "V" },
        { 9, "IX" },
        { 10, "X" },
        { 40, "XL" },
        { 50, "L" },
        { 90, "XC" },
        { 100, "C" },
        { 400, "CD" },
        { 500, "D" },
        { 900, "CM" },
        { 1000, "M" }
    };


    [InlineData(3, "III")]
    [InlineData(4, "IV")]
    [InlineData(9, "IX")]
    [InlineData(58, "LVIII")]
    public string IntToRoman(int num)
    {
        var res = "";
        var ns = num.ToString().AsSpan();

        int nsl = ns.Length;
        for (int i = nsl - 1; i >= 0; i--)
        {
            var s = ns[i].ToString().PadRight(nsl - i, '0');
            var n = int.Parse(s);
            var ss = "";
            if (n >= 1 && n < 4)
            {
                ss = RepeatN(RN[1], n - 0);
            }
            else if (n == 4)
            {
                ss = RN[n];
            }
            else if (n >= 5 && n < 9)
            {
                ss = RN[5] + RepeatN(RN[1], n - 5);
            }
            else if (n == 9)
            {
                ss = RN[n];
            }
            else if (n >= 10 && n < 40)
            {
                ss = RepeatN(RN[10], n / 10);
            }
            else if (n == 40)
            {
                ss = RN[n];
            }
            else if (n >= 50 && n < 90)
            {
                ss = RN[50] + RepeatN(RN[10], (n - 50) / 10);
            }
            else if (n == 90)
            {
                ss = RN[n];
            }
            else if (n >= 100 && n < 400)
            {
                ss = RepeatN(RN[100], n / 100);
            }
            else if (n == 400)
            {
                ss = RN[n];
            }
            else if (n >= 500 && n < 900)
            {
                ss = RN[500] + RepeatN(RN[100], (n - 500) / 100);
            }
            else if (n == 900)
            {
                ss = RN[n];
            }
            else if (n >= 1000 && n < 4000)
            {
                ss = RepeatN(RN[1000], n / 1000);
            }
            res = ss + res;
        }
        return res;
    }

    //[InlineData(3, "III")]
    //[InlineData(4, "IV")]
    //[InlineData(9, "IX")]
    //[InlineData(58, "LVIII")]
    public string IntToRoman2(int num)
    {
        var res = "";
        var ns = num.ToString().AsSpan();

        int nsl = ns.Length;
        for (int i = nsl - 1; i >= 0; i--)
        {
            var s = ns[i].ToString().PadRight(nsl - i, '0');
            var n = int.Parse(s);

            var ss = n switch
            {
                >= 1 and < 4 => RepeatN(RN[1], n - 0),
                4 => RN[n],
                >= 5 and < 9 => RN[5] + RepeatN(RN[1], n - 5),
                9 => RN[n],
                >= 10 and < 40 => RepeatN(RN[10], n / 10),
                40 => RN[n],
                >= 50 and < 90 => RN[50] + RepeatN(RN[10], (n - 50) / 10),
                90 => RN[n],
                >= 100 and < 400 => RepeatN(RN[100], n / 100),
                400 => RN[n],
                >= 500 and < 900 => RN[500] + RepeatN(RN[100], (n - 500) / 100),
                900 => RN[n],
                >= 1000 and < 4000 => RepeatN(RN[1000], n / 1000),
                _ => ""
            };
            res = ss + res;
        }
        return res;
    }

    /// <summary>
    /// 字符重复n此
    /// </summary>
    /// <param name="s"></param>
    /// <param name="n"></param>
    /// <returns></returns>
    private string RepeatN(string s, int n)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++)
        {
            sb.Append(s);
        }
        return sb.ToString();
    }
}
