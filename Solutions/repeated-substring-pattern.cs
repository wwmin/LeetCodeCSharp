namespace Solutions;
/// <summary>
/// 459. 重复的子字符串
/// </summary>
public class RepeatedSubstringPatternSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abab", true };
            yield return new object[] { "aba", false };
            yield return new object[] { "abcabcabcabc", true };
            yield return new object[] { "abbbaabbba", true };
            yield return new object[] { "ababab", true };
            yield return new object[] { "abcabcabc", true };
        }
    }

    [Data]
    public bool RepeatedSubstringPattern(string s)
    {
        //int[] all = new int[26];
        Dictionary<char, int> map = new Dictionary<char, int>();
        for (int i = 0; i < s.Length; i++)
        {
            if (map.ContainsKey(s[i])) map[s[i]]++;
            else map.Add(s[i], 1);
        }
        char[] keys = map.Keys.ToArray();
        if (keys.All(p => p == map[keys[0]])) return true;
        foreach (var key in keys)
        {
            if (map[key] % 2 != 0) return false;
        }

        return true;
    }
}