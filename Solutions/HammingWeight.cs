namespace Solutions;
/// <summary>
/// 191. 位1的个数
/// </summary>
public class HammingWeightSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { BinaryStringTo<uint>("00000000000000000000000000001011"), 3 };
            yield return new object[] { BinaryStringTo<uint>("00000000000000000000000010000000"), 1 };
            yield return new object[] { BinaryStringTo<uint>("11111111111111111111111111111101"), 31 };
        }
    }

    [Data]
    public int HammingWeight(uint n)
    {
        int ans = 0;
        for (int i = 0; i < 32; i++)
        {
            if ((n & (1 << i)) != 0) ans++;
        }
        return ans;
    }

    public int HammingWeight1(uint n)
    {
        int ans = 0;
        string s = Convert.ToString(n, toBase: 2);
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '1') ans++;
        }
        return ans;
    }
}