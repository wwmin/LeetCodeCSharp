﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 5846. 找到数组的中间位置
/// </summary>
public class FindMiddleIndexSolution
{
    [InlineData(new int[] { 2, 3, -1, 8, 4 }, 3)]
    [InlineData(new int[] { 1, -1, 4 }, 2)]
    [InlineData(new int[] { 2, 5 }, -1)]
    [InlineData(new int[] { 1 }, 0)]
    [InlineData(new int[] { 1, 1 }, -1)]
    [InlineData(new int[] { 4, 0 }, 0)]
    public int FindMiddleIndex(int[] nums)
    {
        int n = nums.Length;
        if (n == 1) return 0;
        int leftSum = 0;
        int rightSum = nums.Sum() - nums[0];
        if (rightSum == leftSum) return 0;
        for (int i = 1; i < n; i++)
        {
            var m = nums[i];
            var prem = nums[i - 1];
            leftSum += prem;
            rightSum -= m;
            if (rightSum == leftSum) return i;
        }
        return -1;
    }
}
