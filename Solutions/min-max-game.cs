namespace Solutions;
/// <summary>
/// 2293. 极大极小游戏
/// difficulty: Easy
/// https://leetcode.cn/problems/min-max-game/
/// </summary>
public class MinMaxGame_2293_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,3,5,2,4,8,2,2]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[3]").ToArray(), 3 };
        }
    }

    [Data]
    public int MinMaxGame(int[] nums)
    {
        int n = nums.Length;
        if (n == 1)
        {
            return nums[0];
        }
        int[] newNums = new int[n / 2];
        for (int i = 0; i < newNums.Length; i++)
        {
            if (i % 2 == 0)
            {
                newNums[i] = Math.Min(nums[2 * i], nums[2 * i + 1]);
            }
            else
            {
                newNums[i] = Math.Max(nums[2 * i], nums[2 * i + 1]);
            }
        }
        return MinMaxGame(newNums);
    }
}