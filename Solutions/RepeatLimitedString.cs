﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 6014. 构造限制重复的字符串
    /// </summary>
    public class RepeatLimitedStringSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { "cczazcc", 3, "zzcccac" };
                yield return new object[] { "aababab", 2, "bbabaa" };
            }
        }
        [Data]
        public string RepeatLimitedString(string s, int repeatLimit)
        {
            int[] count = new int[26];
            for (int k = 0; k < s.Length; k++)
            {
                count[s[k] - 'a']++;
            }
            StringBuilder sb = new StringBuilder();
            int c = 0;
            int i = 25;
            while (true)
            {
                while (i >= 0 && count[i] == 0)
                {
                    i--;
                }
                if (i == -1) break;
                while (count[i] > 0 && c < repeatLimit)
                {
                    c++;
                    sb.Append((char)(i + 'a'));
                    count[i]--;
                }
                c = 0;
                if (count[i] == 0) continue;
                else
                {
                    int j = i - 1;
                    while (j >= 0 && count[j] == 0) j--;
                    if (j >= 0)
                    {
                        sb.Append((char)(j + 'a'));
                        count[j]--;
                    }
                    else break;
                }
            }
            return sb.ToString();
        }

    }
}
