namespace Solutions;
/// <summary>
/// 1910. 检查二进制字符串字段
/// https://leetcode.cn/problems/check-if-binary-string-has-at-most-one-segment-of-ones/
/// </summary>
public class CheckOnesSegmentSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "1001", false };
            yield return new object[] { "110", true };
        }
    }

    [Data]
    public bool CheckOnesSegment(string s)
    {
        bool hasZero = false;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '0') hasZero = true;
            else if (s[i] == '1' && hasZero) return false;
        }
        return true;
    }
}