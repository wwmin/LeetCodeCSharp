namespace Solutions;
/// <summary>
/// 563. 二叉树的坡度
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class FindTiltSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,2,3]"), 1 };
            yield return new object[] { ToTreeNodeWithString("[4,2,9,3,5,null,7]"), 15 };
            yield return new object[] { ToTreeNodeWithString("[21,7,14,1,1,2,2,3,3]"), 9 };
        }
    }
    [Data]
    public int FindTilt(TreeNode root)
    {
        Console.WriteLine(root.ToString());
        return default;
    }
}