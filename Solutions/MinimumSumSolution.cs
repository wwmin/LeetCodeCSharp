﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5984. 拆分数位后四位数字的最小和
    /// </summary>
    public class MinimumSumSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { 2932, 52 };
                yield return new object[] { 4009, 13 };
            }
        }
        [Data]
        public int MinimumSum(int num)
        {
            char[] num_str = num.ToString().ToCharArray();
            int[] list = new int[4];
            for (int i = 0; i < 4; i++)
            {
                list[i] = Convert.ToInt32(num_str[i].ToString());
            }
            Array.Sort(list);
            int num1 = Convert.ToInt32(list[0].ToString() + list[2].ToString());
            int num2 = Convert.ToInt32(list[1].ToString() + list[3].ToString());
            return num1 + num2;
        }
    }
}