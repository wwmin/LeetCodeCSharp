namespace Solutions;
/// <summary>
/// 1000037. 第 k 个数
/// https://leetcode.cn/problems/get-kth-magic-number-lcci/
/// </summary>
public class GetKthMagicNumberSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 5, 9 };
        }
    }

    [Data]
    public int GetKthMagicNumber(int k)
    {
        int[] factors = { 3, 5, 7 };
        ISet<long> seen = new HashSet<long>();
        PriorityQueue<long, long> heap = new PriorityQueue<long, long>();
        seen.Add(1);
        heap.Enqueue(1, 1);
        int magic = 0;
        for (int i = 0; i < k; i++)
        {
            long curr = heap.Dequeue();
            magic = (int)curr;
            foreach (var factor in factors)
            {
                long next = curr * factor;
                if (seen.Add(next))
                {
                    heap.Enqueue(next, next);
                }
            }
        }
        return magic;
    }
}