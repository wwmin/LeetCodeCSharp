namespace Solutions;
/// <summary>
/// 1807. 替换字符串中的括号内容
/// difficulty: Medium
/// https://leetcode.cn/problems/evaluate-the-bracket-pairs-of-a-string/
/// </summary>
public class Evaluate_1807_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "(name)is(age)yearsold", StringTo<List<List<string>>>("[[\"name\",\"bob\"],[\"age\",\"two\"]]").ToArray(), "bobistwoyearsold" };
            yield return new object[] { "hi(name)", StringTo<List<List<string>>>("[[\"a\",\"b\"]]").ToArray(), "hi?" };
            yield return new object[] { "(a)(a)(a)aaa", StringTo<List<List<string>>>("[[\"a\",\"yes\"]]").ToArray(), "yesyesyesaaa" };
        }
    }

    [Data]
    public string Evaluate(string s, IList<IList<string>> knowledge)
    {
        var dict = new Dictionary<string, string>();
        foreach (var item in knowledge)
        {
            dict[item[0]] = item[1];
        }
        StringBuilder sb = new StringBuilder();
        bool hasLeft = false;
        for (int i = 0; i < s.Length; i++)
        {
            if (hasLeft == false)
            {
                if (s[i] == '(')
                {
                    hasLeft = true;
                }
                else
                {
                    sb.Append(s[i]);
                }
            }
            else
            {
                var index = s.IndexOf(')', i);
                if (index == -1) continue;
                hasLeft = false;
                var ss = s.Substring(i, index - i);
                if (dict.TryGetValue(ss, out var value))
                {
                    sb.Append(value);
                }
                else
                {
                    sb.Append('?');
                }
                i = index;
            }
        }
        return sb.ToString();
    }
}