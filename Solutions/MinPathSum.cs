﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 64. 最小路径和
/// </summary>
public class MinPathSumSolution
{

    class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,3,1],[1,5,1],[4,2,1]]").ToArray(), 7 };
        }
    }
    [Data]
    public int MinPathSum(int[][] grid)
    {
        if (grid == null || grid.Length == 0 || grid[0].Length == 0) return 0;
        int m = grid.Length;
        int n = grid[0].Length;

        int[,] data = new int[m, n];
        data[0, 0] = grid[0][0];
        for (int i = 1; i < m; i++)
        {
            data[i, 0] = grid[i][0] + data[i - 1, 0];
        }
        for (int j = 1; j < n; j++)
        {
            data[0, j] = grid[0][j] + data[0, j - 1];
        }
        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++)
            {
                data[i, j] = Math.Min(data[i - 1, j], data[i, j - 1]) + grid[i][j];
            }
        }
        return data[m - 1, n - 1];
    }
}
