﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 881. 救生艇
/// </summary>
public class NumRescueBoatsSolution
{

    [InlineData(new int[] { 1, 2 }, 3, 1)]
    [InlineData(new int[] { 3, 2, 2, 1 }, 3, 3)]
    [InlineData(new int[] { 3, 5, 3, 4 }, 5, 4)]
    [InlineData(new int[] { 5, 1, 4, 2 }, 6, 2)]
    public int NumRescueBoats(int[] people, int limit)
    {
        int num = 0;
        //先从小到大排列
        Array.Sort(people);
        int left = 0;
        int right = people.Length - 1;
        while (left <= right)
        {
            if (people[right] == limit)
            {
                num++;
                right--;
            }
            else
            {
                if (people[left] + people[right] <= limit)
                {
                    num++;
                    right--;
                    left++;
                }
                else
                {
                    num++;
                    right--;
                }
            }
        }
        return num;
    }

    //此解超时
    //[InlineData(new int[] { 1, 2 }, 3, 1)]
    //[InlineData(new int[] { 3, 2, 2, 1 }, 3, 3)]
    //[InlineData(new int[] { 3, 5, 3, 4 }, 5, 4)]
    //[InlineData(new int[] { 5, 1, 4, 2 }, 6, 2)]
    public int NumRescueBoats2(int[] people, int limit)
    {
        int num = 0;
        //先从小到大排列
        Array.Sort(people);
        //尽可能的使最大值配一个极限最小中的最大值
        int right = people.Length - 1;
        int left = right - 1;
        while (right >= 0)
        {
            if (people[right] == 0) { right--; left = right - 1; continue; }
            if (people[right] == limit)
            {
                //res.Add(new int[] { people[right] });
                num++;
                right--;
                left = right - 1;
                continue;
            }
            num++;
            //然后将第一个可以匹配的值置为0
            while (left >= 0)
            {
                if (people[left] == 0)
                {
                    left--;
                    continue;
                }
                if (people[right] + people[left] <= limit)
                {
                    people[left] = 0;
                    break;
                }
                left--;
            }
            right--;
            left = right - 1;
        }
        return num;
    }
}
