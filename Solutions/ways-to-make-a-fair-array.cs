namespace Solutions;
/// <summary>
/// 1664. 生成平衡数组的方案数
/// difficulty: Medium
/// https://leetcode.cn/problems/ways-to-make-a-fair-array/
/// </summary>
public class WaysToMakeFair_1664_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,1,6,4]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,1,1]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), 0 };
        }
    }

    [Data]
    public int WaysToMakeFair(int[] nums)
    {
        int ans = 0;
        int n = nums.Length;
        bool isEvent = true;
        long oddSumAfter = 0;
        long eventSumAfter = 0;
        long oddSumBefore = 0;
        long eventSumBefore = 0;
        for (int i = 0; i < n; i++)
        {
            if (isEvent)
            {
                eventSumAfter += nums[i];
            }
            else
            {
                oddSumAfter += nums[i];
            }
            isEvent = !isEvent;
        }
        isEvent = true;
        for (int i = 0; i < n; i++)
        {
            if (isEvent)
            {
                eventSumAfter -= nums[i];
            }
            else
            {
                oddSumAfter -= nums[i];
            }

            if (eventSumBefore + oddSumAfter == eventSumAfter + oddSumBefore)
            {
                ans++;
            }

            if (isEvent)
            {
                eventSumBefore += nums[i];
            }
            else
            {
                oddSumBefore += nums[i];
            }
            isEvent = !isEvent;
        }
        return ans;
    }
}