﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 6013. 合并零之间的节点
    /// </summary>
    public class MergeNodesSolution
    {

        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { ToListNodeWithString("[0,3,1,0,4,5,2,0]"), ToListNodeWithString("[4,11]") };
            }
        }
        [Data]
        public ListNode MergeNodes(ListNode head)
        {
            ListNode ans = new ListNode(0);
            ListNode temp = ans;
            while (head != null)
            {
                if (head.val == 0)
                {
                    head = head.next;
                    int sum = 0;
                    while (head != null && head.val != 0)
                    {
                        sum += head.val;
                        head = head.next;
                    }
                    if (sum > 0)
                    {
                        temp.next = new ListNode(sum);
                        temp = temp.next;
                    }
                }
                else
                {
                    head = head.next;
                }
            }
            return ans.next;
        }
    }
}
