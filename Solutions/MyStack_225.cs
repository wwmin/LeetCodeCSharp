namespace Solutions;
/// <summary>
/// 225. 用队列实现栈
/// </summary>
public class MyStack
{


    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"MyStack\",\"push\",\"push\",\"top\",\"pop\",\"empty\"]", "[[],[1],[2],[],[],[]]", "[null,null,null,2,2,false]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
        MyStack stack = new MyStack();
        stack.Push(1);
        stack.Push(2);
        var top = stack.Top();
        Console.WriteLine(top);
        var p = stack.Pop();
        Console.WriteLine(p);
        var empty = stack.Empty();
        Console.WriteLine(empty);
    }
    Queue<int> _queue1 { get; set; }
    Queue<int> _queue2 { get; set; }
    public MyStack()
    {
        _queue1 = new Queue<int>();
        _queue2 = new Queue<int>();
    }

    public void Push(int x)
    {
        _queue2.Enqueue(x);
        while (_queue1.Count > 0)
        {
            int a = _queue1.Dequeue();
            _queue2.Enqueue(a);
        }
        Queue<int> temp = _queue1;
        _queue1 = _queue2;
        _queue2 = temp;
    }

    public int Pop()
    {
        return _queue1.Dequeue();
    }

    public int Top()
    {
        return _queue1.Peek();
    }

    public bool Empty()
    {
        return _queue1.Count == 0;
    }
}

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack obj = new MyStack();
 * obj.Push(x);
 * int param_2 = obj.Pop();
 * int param_3 = obj.Top();
 * bool param_4 = obj.Empty();
 */