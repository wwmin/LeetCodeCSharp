namespace Solutions;
/// <summary>
/// 1817. 查找用户活跃分钟数
/// difficulty: Medium
/// https://leetcode.cn/problems/finding-the-users-active-minutes/
/// </summary>
public class FindingUsersActiveMinutes_1817_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,5],[1,2],[0,2],[0,5],[1,3]]").ToArray(), 5, StringTo<int[]>("[0,2,0,0,0]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,1],[2,2],[2,3]]").ToArray(), 4, StringTo<int[]>("[1,1,0,0]").ToArray() };
        }
    }

    [Data]
    public int[] FindingUsersActiveMinutes(int[][] logs, int k)
    {
        IDictionary<int, ISet<int>> activeMinutes = new Dictionary<int, ISet<int>>();
        foreach (int[] log in logs)
        {
            int id = log[0], time = log[1];
            activeMinutes.TryAdd(id, new HashSet<int>());
            activeMinutes[id].Add(time);
        }
        int[] answer = new int[k];
        foreach (ISet<int> minutes in activeMinutes.Values)
        {
            int activeCount = minutes.Count;
            answer[activeCount - 1]++;
        }
        return answer;
    }
}