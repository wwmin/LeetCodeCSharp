namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 598. 范围求和 II
/// </summary>
public class MaxCountSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, 3, StringTo<List<int[]>>("[[2,2],[3,3]]").ToArray(), 4 };
            yield return new object[] { 3, 3, StringTo<List<int[]>>("[]").ToArray(), 9 };
        }
    }
    [Data]
    public int MaxCount(int m, int n, int[][] ops)
    {
        if (ops.Length == 0) return m * n;

        //最大操作数的个数等同于操作矩阵的所有能覆盖的范围
        int minX = ops.Min(p => p[0]);
        int minY = ops.Min(p => p[1]);

        return minX * minY;
    }
}