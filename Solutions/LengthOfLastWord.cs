﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 58. 最后一个单词的长度
/// </summary>
public class LengthOfLastWordSolution
{
    [InlineData("Hello World", 5)]
    [InlineData("   fly me   to   the moon  ", 4)]
    public int LengthOfLastWord(string s)
    {
        var ss = s.TrimEnd().Split(" ");
        return ss[ss.Length - 1].Length;
    }
}
