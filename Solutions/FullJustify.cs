﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 68. 文本左右对齐
/// </summary>
public class FullJustifySolution
{

    class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"This\", \" is \", \"an\", \"example\", \"of\", \"text\", \"justification.\"]"), 16, StringTo<List<string>>("[\"This is an\",\"example  of text\",\"justification.  \"]") };
        }
    }
    //对齐数组
    [Data]
    public IList<string> FullJustify(string[] words, int maxWidth)
    {
        //定义相应的变量
        int n = words.Length;
        int[] nList = new int[n];
        IList<string> ans = new List<string>();
        //统计所有字符串长度
        for (int i = 0; i < n; i++)
        {
            nList[i] = words[i].Length;
        }
        //遍历长度数组进行分组
        int start = 0;
        int sLen = 0;
        for (int i = 0; i < n; i++)
        {
            sLen += nList[i] + 1;
            //如果加入当前字符后长度刚好符合则进行排列
            if (sLen == maxWidth || sLen - 1 == maxWidth)
            {
                ans.Add(Rank(words, start, i, nList, maxWidth));
                start = i + 1;
                sLen = 0;
            }
            //如果加入当前字符串后长度超出则对[start,end-1]进行排列
            else if (sLen - 1 > maxWidth)
            {
                ans.Add(Rank(words, start, i - 1, nList, maxWidth));
                start = i;
                sLen = nList[i] + 1;
            }
        }
        //对于最后一行元素
        if (sLen != 0)
        {
            ans.Add(Rank(words, start, n - 1, nList, maxWidth, true));
        }
        return ans;
    }
    /// <summary>
    /// 对序列进行排序
    /// </summary>
    private string Rank(string[] words, int start, int end, int[] nList, int maxvalue, bool last = false)
    {
        string ans = "";
        if (last)
        {
            for (int i = start; i < end; i++)
            {
                ans += words[i] + " ";
            }
            ans += words[end];
            //对于最后一行补空格
            while (ans.Length != maxvalue)
            {
                ans += " ";
            }
        }
        //不是最后一行则进行相应排序方法
        else
        {
            //统计字符串个数,长度
            int n = end - start + 1;

            //如果n==1
            if (n == 1)
            {
                ans = words[start];
                while (ans.Length != maxvalue)
                {
                    ans += " ";
                }
                return ans;
            }

            int nums = 0;
            for (int i = start; i <= end; i++)
            {
                nums += nList[i];
            }
            //计算间隔
            int k = maxvalue - nums;
            //平均间隔
            int meank = k / Math.Max((n - 1), 1);
            string x = "";
            for (int i = 0; i < meank; i++)
            {
                x += " ";
            }
            int upk = k % Math.Max((n - 1), 1);
            for (int i = 0; i < n; i++)
            {
                if (i < upk)
                {
                    ans += words[i + start] + x + " ";
                }
                else if (i >= upk && i < n - 1)
                {
                    ans += words[i + start] + x;
                }
                else
                {
                    ans += words[i + start];
                }
            }
        }
        return ans;
    }
}