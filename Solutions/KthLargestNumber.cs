﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 5855. 找出数组中的第 K 大整数
/// </summary>
public class KthLargestNumberSolution
{
    [InlineData(new string[] { "3", "6", "7", "10" }, 4, "3")]
    public string KthLargestNumber(string[] nums, int k)
    {
        Array.Sort(nums, ((o1, o2) =>
        {
            if (o1.Length > o2.Length) return -1;
            else if (o1.Length < o2.Length) return 1;
            else return o2.CompareTo(o1);
        }));
        return nums[k - 1];
    }
}
