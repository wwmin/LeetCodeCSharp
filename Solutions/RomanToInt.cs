﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 13. 罗马数字转整数
/// </summary>
public class RomanToIntSolution
{

    private static readonly Dictionary<string, int> RN = new()
    {
        { "I", 1 },
        { "IV", 4 },
        { "V", 5 },
        { "IX", 9 },
        { "X", 10 },
        { "XL", 40 },
        { "L", 50 },
        { "XC", 90 },
        { "C", 100 },
        { "CD", 400 },
        { "D", 500 },
        { "CM", 900 },
        { "M", 1000 }
    };

    [InlineData("III", 3)]
    [InlineData("IV", 4)]
    [InlineData("IX", 9)]
    [InlineData("LVIII", 58)]
    [InlineData("MCMXCIV", 1994)]
    public int RomanToInt(string s)
    {
        var keys = RN.Keys;
        var numString = new List<string>();
        var rs = "";
        for (int i = 0; i < s.Length; i++)
        {

            var c = s[i];
            if (rs.Length == 0) rs = c.ToString();
            else if (rs[^1] == c) rs += c;
            else
            {
                if (keys.Contains(rs + c))
                {
                    numString.Add(rs + c);
                    rs = "";
                }
                else
                {
                    numString.Add(rs);
                    rs = c.ToString();
                }
            }
        }
        if (rs.Length > 0) numString.Add(rs);
        var num = 0;
        foreach (var ns in numString)
        {
            if (ns.All(p => p == ns[0]))
            {
                var c = ns[0].ToString();
                var n = RN[c];
                num += n * ns.Length;
            }
            else
            {
                num += RN[ns];
            }
        }
        return num;
    }
}
