namespace Solutions;
/// <summary>
/// 119. 杨辉三角 II
/// </summary>
public class GetRowSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, StringTo<List<int>>("[1,3,3,1]") };
            yield return new object[] { 0, StringTo<List<int>>("[1]") };
            yield return new object[] { 1, StringTo<List<int>>("[1,1]") };
        }
    }

    [Data]
    public IList<int> GetRow(int rowIndex)
    {
        IList<IList<int>> ans = new List<IList<int>>();
        ans.Add(new List<int>() { 1 });
        for (int i = 1; i <= rowIndex; i++)
        {
            IList<int> row = new List<int>() { 1 };
            for (int j = 1; j < i; j++)
            {
                row.Add(ans[i - 1][j - 1] + ans[i - 1][j]);
            }
            row.Add(1);
            ans.Add(row);
        }
        return ans[rowIndex];
    }
}