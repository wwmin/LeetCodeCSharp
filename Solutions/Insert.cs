﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 57. 插入区间
/// </summary>
public class InsertSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[6,10],[13,16],[19,19],[23,25],[34,39],[41,43],[49,51]]").ToArray(), StringTo<int[]>("[27,27]"), StringTo<List<int[]>>("[[6,10],[13,16],[19,19],[23,25],[27,27],[34,39],[41,43],[49,51]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[3,5],[12,15]]").ToArray(), StringTo<int[]>("[6,6]"), StringTo<List<int[]>>("[[3,5],[6,6],[12,15]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[0,10]]").ToArray(), StringTo<int[]>("[2,2]"), StringTo<List<int[]>>("[[0,10]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,5]]").ToArray(), StringTo<int[]>("[0,1]"), StringTo<List<int[]>>("[[0,5]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[2,5],[6,7],[8,9]]").ToArray(), StringTo<int[]>("[0,1]"), StringTo<List<int[]>>("[[0,1],[2,5],[6,7],[8,9]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,5],[6,8]]").ToArray(), StringTo<int[]>("[0,9]"), StringTo<List<int[]>>("[[0,9]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,5]]").ToArray(), StringTo<int[]>("[0,0]"), StringTo<List<int[]>>("[[0,0],[1,5]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,5]]").ToArray(), StringTo<int[]>("[0,3]"), StringTo<List<int[]>>("[[0,5]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[]").ToArray(), StringTo<int[]>("[5,7]"), StringTo<List<int[]>>("[[5,7]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,5]]").ToArray(), StringTo<int[]>("[6,8]"), StringTo<List<int[]>>("[[1,5],[6,8]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,3],[6,9]]").ToArray(), StringTo<int[]>("[2,5]"), StringTo<List<int[]>>("[[1,5],[6,9]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,2],[3,5],[6,7],[8,10],[12,16]]").ToArray(), StringTo<int[]>("[4,8]"), StringTo<List<int[]>>("[[1,2],[3,10],[12,16]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,5]]").ToArray(), StringTo<int[]>("[2,3]"), StringTo<List<int[]>>("[[1,5]]").ToArray() };
        }
    }
    [Data]
    public int[][] Insert(int[][] intervals, int[] newInterval)
    {
        List<int[]> res = new List<int[]>();
        int n = intervals.Length;
        if (n == 0) { res.Add(newInterval); return res.ToArray(); }
        bool inner = false;
        for (int i = 0; i < n; i++)
        {
            var c = intervals[i];
            if (c[1] >= newInterval[0] && c[0] <= newInterval[1])
            {
                inner = true;
                res.AddRange(intervals[0..(i + 1)]);
                //更新左侧位置
                if (c[0] > newInterval[0])
                {
                    res[res.Count - 1][0] = newInterval[0];
                }
                //判断右侧的位置
                if (c[1] < newInterval[1])
                {
                    res[res.Count - 1][1] = newInterval[1];

                    int j = i + 1;
                    while (j < n)
                    {
                        if (intervals[j][0] <= newInterval[1])
                        {
                            if (res[res.Count - 1][1] < intervals[j][1]) res[res.Count - 1][1] = intervals[j][1];
                            j++;
                            continue;
                        }
                        else break;
                    }
                    i = j;
                    res.AddRange(intervals[i..]);
                }
                else
                {
                    if (i + 1 < n) res.AddRange(intervals[(i + 1)..]);
                }

                break;
            }
        }
        if (inner == false)
        {
            if (newInterval[0] < intervals[0][0])
            {
                res.Add(newInterval);
                res.AddRange(intervals);
            }
            else if (newInterval[0] > intervals[intervals.Length - 1][0])
            {
                res.AddRange(intervals);
                res.Add(newInterval);
            }
            else
            {
                int i = 0;
                while (i < n)
                {
                    if (newInterval[0] > intervals[i][0]) { i++; continue; }
                    else break;
                }
                res.AddRange(intervals[0..i]);
                res.Add(newInterval);
                res.AddRange(intervals[i..]);
            }
        }
        return res.ToArray();
    }
}