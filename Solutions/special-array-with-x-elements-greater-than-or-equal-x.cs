namespace Solutions;
/// <summary>
/// 1730. 特殊数组的特征值
/// https://leetcode.cn/problems/special-array-with-x-elements-greater-than-or-equal-x/
/// </summary>
public class SpecialArraySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[3,5]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[0,0]").ToArray(), -1 };
            yield return new object[] { StringTo<int[]>("[0,4,3,0,4]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[3,6,7,7,0]").ToArray(), -1 };
        }
    }

    [Data]
    public int SpecialArray(int[] nums)
    {
        Array.Sort(nums);
        int n = nums.Length;
        int last = -1;
        for (int i = 0; i < n; i++)
        {
            int x = n - i;
            if (x <= nums[i])
            {
                if (last >= 0)
                {
                    if (x > last) return x;
                }
                else
                {
                    return x;
                }
            }
            last = nums[i];
        }
        return -1;
    }
}