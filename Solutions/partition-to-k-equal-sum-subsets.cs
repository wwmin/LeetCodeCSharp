namespace Solutions;
/// <summary>
/// 698. 划分为k个相等的子集
/// difficulty: Medium
/// https://leetcode.cn/problems/partition-to-k-equal-sum-subsets/
/// </summary>
public class CanPartitionKSubsets_698_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[4,3,2,3,5,2,1]").ToArray(), 4, true };
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), 3, false };
        }
    }

    int[] nums;
    int per, n;
    bool[] dp;

    [Data]
    public bool CanPartitionKSubsets(int[] nums, int k) {
        this.nums = nums;
        int all = nums.Sum();
        if (all % k != 0) {
            return false;
        }
        per = all / k;
        Array.Sort(nums);
        n = nums.Length;
        if (nums[n - 1] > per) {
            return false;
        }
        dp = new bool[1 << n];
        Array.Fill(dp, true);
        return DFS((1 << n) - 1, 0);
    }

    public bool DFS(int s, int p) {
        if (s == 0) {
            return true;
        }
        if (!dp[s]) {
            return dp[s];
        }
        dp[s] = false;
        for (int i = 0; i < n; i++) {
            if (nums[i] + p > per) {
                break;
            }
            if (((s >> i) & 1) != 0) {
                if (DFS(s ^ (1 << i), (p + nums[i]) % per)) {
                    return true;
                }
            }
        }
        return false;
    }
}