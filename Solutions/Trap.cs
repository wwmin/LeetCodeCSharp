﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 42. 接雨水
/// </summary>
public class TrapSolution
{
    [InlineData(new int[] { 0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1 }, 6)]
    //[InlineData(new int[] { 4, 2, 0, 3, 2, 5 }, 9)]
    public int Trap(int[] height)
    {
        int n = height.Length;
        if (n == 0) return 0;
        int[] leftMax = new int[n];
        leftMax[0] = height[0];
        for (int i = 1; i < n; i++)
        {
            leftMax[i] = Math.Max(leftMax[i - 1], height[i]);
        }
        int[] rightMax = new int[n];
        rightMax[n - 1] = height[n - 1];
        for (int i = n - 2; i >= 0; i--)
        {
            rightMax[i] = Math.Max(rightMax[i + 1], height[i]);
        }
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            ans += Math.Min(leftMax[i], rightMax[i]) - height[i];
        }
        return ans;
    }
}
