﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 75. 颜色分类
/// </summary>
public class SortColorsSolution
{
    //[InlineData(new int[] { 2, 0, 2, 1, 1, 0 }, new int[] { 0, 0, 1, 1, 2, 2 })]
    [InlineData(new int[] { 2, 0, 1 }, new int[] { 0, 1, 2 })]
    public void SortColors(int[] nums)
    {
        int n = nums.Length;
        if (n < 2) return;
        //双指针
        int left = 0;
        int right = n - 1;

        for (int i = 0; i <= right; ++i)
        {
            //遇到0 跟nums[left]换
            //遇到2 跟nums[right]换
            if (nums[i] == 0)
            {
                nums[i] = nums[left];
                nums[left] = 0;
                left++;
            }
            if (nums[i] == 2)
            {
                nums[i] = nums[right];
                nums[right] = 2;
                right--;
                //此时判断nums[i] 是否为1 若不是1 则回退重新判断
                if (nums[i] != 1)
                {
                    --i;
                }
            }

        }
    }

    private void Swap(int[] nums, int i, int j)
    {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
