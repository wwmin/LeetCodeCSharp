﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 34. 在排序数组中查找元素的第一个和最后一个位置
/// </summary>
public class SearchRangeSolution
{
    [InlineData(new int[] { 5, 7, 7, 8, 8, 10 }, 8, new int[] { 3, 4 })]
    [InlineData(new int[] { 5, 7, 7, 8, 8, 10 }, 6, new int[] { -1, -1 })]
    public int[] SearchRange(int[] nums, int target)
    {
        //先二分,在两侧延展
        int[] res = new int[2] { -1, -1 };
        int n = nums.Length;
        if (n == 0) return res;
        int index = -1;
        int left = 0;
        int right = n - 1;
        while (left <= right)
        {
            if (nums[left] == target)
            {
                index = left;
                break;
            };
            if (nums[right] == target)
            {
                index = right;
                break;
            }
            int mid = (left + right) / 2;
            if (nums[mid] == target)
            {
                index = mid;
                break;
            }
            if (nums[mid] > target)
            {
                right = mid - 1;
            }
            else
            {
                left = mid + 1;
            }
        }
        if (index > -1)
        {
            res[0] = index;
            res[1] = index;
            left = index - 1;
            right = index + 1;
            while (left > 0)
            {
                if (nums[left] == target)
                {
                    res[0] = left;
                    left--;
                }
                else break;
            }
            while (right < n)
            {
                if (nums[right] == target)
                {
                    res[1] = right;
                    right++;
                }
                else break;
            }
        }
        return res;
    }
}
