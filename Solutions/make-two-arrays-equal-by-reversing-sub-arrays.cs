namespace Solutions;
/// <summary>
/// 1556. 通过翻转子数组使两个数组相等
/// </summary>
public class CanBeEqualSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), StringTo<int[]>("[2,4,1,3]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[7]").ToArray(), StringTo<int[]>("[7]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[3,7,9]").ToArray(), StringTo<int[]>("[3,7,11]").ToArray(), false };
        }
    }

    [Data]
    public bool CanBeEqual(int[] target, int[] arr)
    {
        Dictionary<int, int> dic = new Dictionary<int, int>();
        for (int i = 0; i < target.Length; i++)
        {
            if (dic.ContainsKey(target[i]))
            {
                dic[target[i]]++;
            }
            else
            {
                dic.Add(target[i], 1);
            }
        }

        for (int i = 0; i < arr.Length; i++)
        {
            if (!dic.ContainsKey(arr[i]) || dic[arr[i]] <= 0)
            {
                return false;
            }
            dic[arr[i]]--;
        }
        return true;
    }
}