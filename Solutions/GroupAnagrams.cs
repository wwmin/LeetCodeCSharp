﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 49. 字母异位词分组
/// </summary>
public class GroupAnagramsSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"\"]").ToArray(), StringTo<List<string[]>>("[[\"\"]]") };
            yield return new object[] { StringTo<string[]>("[\"a\"]").ToArray(), StringTo<List<string[]>>("[[\"a\"]]") };
            yield return new object[] { StringTo<string[]>("[\"eat\", \"tea\", \"tan\", \"ate\", \"nat\", \"bat\"]").ToArray(),
                StringTo<List<string[]>>("[[\"bat\"],[\"nat\",\"tan\"],[\"ate\",\"eat\",\"tea\"]]").ToArray()};
        }
    }
    [Data]
    public IList<IList<string>> GroupAnagrams(string[] strs)
    {
        Dictionary<string, IList<string>> res = new Dictionary<string, IList<string>>();
        for (int i = 0; i < strs.Length; i++)
        {
            var c = strs[i];
            var oc = string.Join("", c.OrderBy(p => p));
            if (res.TryGetValue(oc, out IList<string> data))
            {
                data.Add(c);
            }
            else
            {
                res.Add(oc, new List<string>() { c });
            };
        }
        return res.Values.ToList();
    }
}
