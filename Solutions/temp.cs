﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 18. 四数之和
/// </summary>
public class fourSumSolution
{

    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,0,-1,0,-2,2]").ToArray(), 0, StringTo<List<List<int>>>("[[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]") };
            yield return new object[] { StringTo<int[]>("[2,2,2,2,2]").ToArray(), 8, StringTo<List<List<int>>>("[[2,2,2,2]]") };
        }
    }
    [Data]
    public IList<IList<int>> FourSum(int[] nums, int target)
    {
        return null;
    }
}