﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 51. N 皇后
/// </summary>
public class SolveNQueensSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, StringTo<List<string[]>>("[[\".Q..\",\"...Q\",\"Q...\",\"..Q.\"],[\"..Q.\",\"Q...\",\"...Q\",\".Q..\"]]") };
        }
    }
    [Data]
    public IList<IList<string>> SolveNQueens(int n)
    {
        List<IList<string>> solutions = new List<IList<string>>();
        int[] queens = new int[n];
        Array.Fill(queens, -1);
        HashSet<int> columns = new HashSet<int>();
        HashSet<int> diagonals1 = new HashSet<int>();
        HashSet<int> diagonals2 = new HashSet<int>();
        backtrack(solutions, queens, n, 0, columns, diagonals1, diagonals2);
        return solutions;
    }
    //深度+回溯法
    private void backtrack(List<IList<string>> solutions, int[] queens, int n, int row, HashSet<int> columns, HashSet<int> diagonals1, HashSet<int> diagonals2)
    {
        if (row == n)
        {
            List<string> board = generateBoard(queens, n);
            solutions.Add(board);
            return;
        }
        for (int i = 0; i < n; i++)
        {
            if (columns.Contains(i)) continue;
            int diagonal1 = row - i;
            if (diagonals1.Contains(diagonal1)) continue;
            int diagonal2 = row + i;
            if (diagonals2.Contains(diagonal2)) continue;
            queens[row] = i;
            columns.Add(i);
            diagonals1.Add(diagonal1);
            diagonals2.Add(diagonal2);
            backtrack(solutions, queens, n, row + 1, columns, diagonals1, diagonals2);//一行一行扫描尝试
            queens[row] = -1;
            columns.Remove(i);
            diagonals1.Remove(diagonal1);
            diagonals2.Remove(diagonal2);
        }

    }

    private List<string> generateBoard(int[] queens, int n)
    {
        List<string> board = new List<string>();
        for (int i = 0; i < n; i++)
        {
            char[] row = new char[n];
            Array.Fill(row, '.');
            row[queens[i]] = 'Q';
            board.Add(new string(row));
        }
        return board;
    }
}
