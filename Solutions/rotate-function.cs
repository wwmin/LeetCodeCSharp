namespace Solutions;
/// <summary>
/// 396. 旋转函数
/// difficulty: Medium
/// https://leetcode.cn/problems/rotate-function/
/// </summary>
public class MaxRotateFunction_396_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[4,3,2,6]").ToArray(), 26 };
            yield return new object[] { StringTo<int[]>("[100]").ToArray(), 0 };
        }
    }

    [Data]
    public int MaxRotateFunction(int[] nums)
    {
        //F(k) = F(k-1) + numSum - n * nums[n-k]
        int f = 0;
        int n = nums.Length;
        int numSum = nums.Sum();
        //F(0)情况
        for (int i = 0; i < n; i++)
        {
            f += i * nums[i];
        }
        int ans = f;
        for (int k = 1; k < n; k++)
        {
            //求F(k)
            f = f + numSum - n * nums[n - k];
            //求最大值
            ans = Math.Max(ans, f);
        }
        return ans;
    }

    //暴力法，超时
    public int MaxRotateFunction1(int[] nums)
    {
        int ans = 0;
        int len = nums.Length;
        for (int i = 0; i < len; i++)
        {
            int sum = 0;
            for (int j = i; j < len + i; j++)
            {
                var index = j % len;
                sum += index * nums[j - i];
            }
            ans = Math.Max(ans, sum);
        }
        return ans;
    }
}