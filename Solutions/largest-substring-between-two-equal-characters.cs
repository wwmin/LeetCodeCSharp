namespace Solutions;
/// <summary>
/// 1746. 两个相同字符之间的最长子字符串
/// https://leetcode.cn/problems/largest-substring-between-two-equal-characters/
/// </summary>
public class MaxLengthBetweenEqualCharactersSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "aa", 0 };
            yield return new object[] { "abca", 2 };
            yield return new object[] { "cbzxy", -1 };
            yield return new object[] { "cabbac", 4 };
        }
    }

    [Data]
    public int MaxLengthBetweenEqualCharacters(string s)
    {
        int res = -1;
        Dictionary<char, int[]> dic = new Dictionary<char, int[]>();
        for (int i = 0; i < s.Length; i++)
        {
            if (dic.ContainsKey(s[i]))
            {
                dic[s[i]][1] = i;
            }
            else
            {
                var arr = new int[2];
                arr[0] = i;
                dic.Add(s[i], arr);
            }
        }
        var keys = dic.Keys.ToArray();
        for (int i = 0; i < keys.Length; i++)
        {
            if (dic[keys[i]][1] > 0)
            {
                res = Math.Max(res, dic[keys[i]][1] - dic[keys[i]][0] - 1);
            }
        }
        return res;
    }
}