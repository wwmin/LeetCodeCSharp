namespace Solutions;
/// <summary>
/// 1454. 删除回文子序列
/// </summary>
public class RemovePalindromeSubSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "ababa", 1 };
            yield return new object[] { "abb", 2 };
            yield return new object[] { "baabb", 2 };
        }
    }

    [Data]
    public int RemovePalindromeSub(string s)
    {
        int n = s.Length;
        int mid = n / 2;
        for (int i = 0; i < mid; i++)
        {
            if (s[i] != s[n - i - 1]) return 2;
        }
        return 1;
    }
}