namespace Solutions;
/// <summary>
/// 118. 杨辉三角
/// </summary>
public class GenerateSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 5, StringTo<List<List<int>>>("[[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]").ToArray() };
            yield return new object[] { 1, StringTo<List<List<int>>>("[[1]]").ToArray() };
        }
    }

    [Data]
    public IList<IList<int>> Generate(int numRows)
    {
        IList<IList<int>> ans = new List<IList<int>>();
        ans.Add(new List<int>() { 1 });
        for (int i = 1; i < numRows; i++)
        {
            IList<int> list = new List<int>() { 1 };
            for (int j = 1; j < i; j++)
            {
                list.Add(ans[i - 1][j - 1] + ans[i - 1][j]);
            }
            list.Add(1);
            ans.Add(list);
        }
        return ans;
    }
}