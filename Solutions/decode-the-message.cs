namespace Solutions;
/// <summary>
/// 2325. 解密消息
/// difficulty: Easy
/// https://leetcode.cn/problems/decode-the-message/
/// </summary>
public class DecodeMessage_2325_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "the quick brown fox jumps over the lazy dog", "vkbs bs t suepuv", "this is a secret" };
            yield return new object[] { "eljuxhpwnyrdgtqkviszcfmabo", "zwx hnfx lqantp mnoeius ycgk vcnjrdb", "the five boxing wizards jump quickly" };
        }
    }

    [Data]
    public string DecodeMessage(string key, string message)
    {
        var dict = new Dictionary<char, char>();
        int n = 0;
        for (int i = 0; i < key.Length; i++)
        {
            if (key[i] != ' ' && !dict.ContainsKey(key[i])) dict.Add(key[i], (char)('a' + n++));
            if (n == 26) break;
        }
        var sb = new StringBuilder();
        foreach (var c in message)
        {
            if (c == ' ') sb.Append(' ');
            else sb.Append(dict[c]);
        }
        return sb.ToString();
    }
}