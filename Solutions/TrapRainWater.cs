namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 407. 接雨水 II
/// </summary>
public class TrapRainWaterSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,4,3,1,3,2],[3,2,1,3,2,4],[2,3,3,2,3,1]]").ToArray(), 4 };
            yield return new object[] { StringTo<List<int[]>>("[[3,3,3,3,3],[3,2,2,2,3],[3,2,1,2,3],[3,2,2,2,3],[3,3,3,3,3]]").ToArray(), 10 };
        }
    }
    [Data]
    public int TrapRainWater(int[][] heightMap)
    {
        int m = heightMap.Length;
        int n = heightMap[0].Length;
        int[] dirs = { -1, 0, 1, 0, -1 };
        int maxHeight = 0;

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                maxHeight = Math.Max(maxHeight, heightMap[i][j]);
            }
        }
        int[,] water = new int[m, n];
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                water[i, j] = maxHeight;
            }
        }

        Queue<int[]> qu = new Queue<int[]>();
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (i == 0 || i == m - 1 || j == 0 || j == n - 1)
                {
                    if (water[i, j] > heightMap[i][j])
                    {
                        water[i, j] = heightMap[i][j];
                        qu.Enqueue(new int[] { i, j });
                    }
                }
            }
        }

        while (qu.Count > 0)
        {
            int[] curr = qu.Dequeue();
            int x = curr[0];
            int y = curr[1];
            for (int i = 0; i < 4; ++i)
            {
                int nx = x + dirs[i], ny = y + dirs[i + 1];
                if (nx < 0 || nx >= m || ny < 0 || ny >= n)
                {
                    continue;
                }
                if (water[x, y] < water[nx, ny] && water[nx, ny] > heightMap[nx][ny])
                {
                    water[nx, ny] = Math.Max(water[x, y], heightMap[nx][ny]);
                    qu.Enqueue(new int[] { nx, ny });
                }
            }
        }

        int res = 0;
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                res += water[i, j] - heightMap[i][j];
            }
        }
        return res;
    }
}

/// <summary>
/// 优先队列-自定义
/// <para>自定义优先队列, 仿java中的PriorityQueue</para>
/// </summary>
/// <typeparam name="T"></typeparam>
public class PriorityQueue<T>
{
    private SortedList<T, int> list = new SortedList<T, int>();
    private int count = 0;

    public void Add(T item)
    {
        if (list.ContainsKey(item)) list[item]++;
        else list.Add(item, 1);

        count++;
    }

    public T PopFirst()
    {
        if (Size() == 0) return default(T);
        T result = list.Keys[0];
        if (--list[result] == 0)
            list.RemoveAt(0);

        count--;
        return result;
    }

    public T PopLast()
    {
        if (Size() == 0) return default(T);
        int index = list.Count - 1;
        T result = list.Keys[index];
        if (--list[result] == 0)
            list.RemoveAt(index);

        count--;
        return result;
    }

    public int Size()
    {
        return count;
    }

    public T PeekFirst()
    {
        if (Size() == 0) return default(T);
        return list.Keys[0];
    }

    public T PeekLast()
    {
        if (Size() == 0) return default(T);
        int index = list.Count - 1;
        return list.Keys[index];
    }

    public IList<T> Keys => list.Keys;
    public IList<int> Values => list.Values;
}
public class MyComparer : IComparer<int[]>
{
    public int Compare(int[] x, int[] y)
    {
        if (x[0] > y[0]) return -1;
        return 1;
    }
}