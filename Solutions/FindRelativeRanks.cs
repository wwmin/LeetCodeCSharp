namespace Solutions;
/// <summary>
/// 506. 相对名次
/// </summary>
public class FindRelativeRanksSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[]").ToArray(), StringTo<string[]>("[]").ToArray() };
            yield return new object[] { StringTo<int[]>("[5]").ToArray(), StringTo<string[]>("[\"Gold Medal\"]").ToArray() };
            yield return new object[] { StringTo<int[]>("[9,1]").ToArray(), StringTo<string[]>("[\"Gold Medal\",\"Silver Medal\"]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,2]").ToArray(), StringTo<string[]>("[\"Silver Medal\",\"Gold Medal\"]").ToArray() };
            yield return new object[] { StringTo<int[]>("[5,9,1]").ToArray(), StringTo<string[]>("[\"Silver Medal\",\"Gold Medal\",\"Bronze Medal\"]").ToArray() };
            yield return new object[] { StringTo<int[]>("[5,4,3,2,1]").ToArray(), StringTo<string[]>("[\"Gold Medal\",\"Silver Medal\",\"Bronze Medal\",\"4\",\"5\"]").ToArray() };
            yield return new object[] { StringTo<int[]>("[10,3,8,9,4]").ToArray(), StringTo<string[]>("[\"Gold Medal\",\"5\",\"Bronze Medal\",\"Silver Medal\",\"4\"]").ToArray() };
        }
    }
    const string gold = "Gold Medal";
    const string silver = "Silver Medal";
    const string bronze = "Bronze Medal";
    [Data]
    public string[] FindRelativeRanks(int[] score)
    {
        int n = score.Length;
        string[] ans = new string[score.Length];
        if (n == 0) return ans;
        if (n == 1)
        {
            ans[0] = gold;
            return ans;
        }
        if (n == 2)
        {
            if (score[0] > score[1]) { ans[0] = gold; ans[1] = silver; }
            else { ans[0] = silver; ans[1] = gold; }
            return ans;
        }
        SortedDictionary<int, string> map = new SortedDictionary<int, string>();
        for (int i = 0; i < n; i++)
        {
            map[score[i]] = (i + 1) + "";
        }
        var keys = map.Keys.ToArray();
        var first = keys[n - 1];
        var second = keys[n - 2];
        var third = keys[n - 3];
        map[first] = gold;
        map[second] = silver;
        map[third] = bronze;
        for (int i = 0; i < keys.Length - 3; i++)
        {
            map[keys[i]] = n - i + "";
        }
        for (int i = 0; i < n; i++)
        {
            ans[i] = map[score[i]];
        }
        return ans;
    }
}