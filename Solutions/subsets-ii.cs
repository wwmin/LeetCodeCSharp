namespace Solutions;
/// <summary>
/// 90. 子集 II
/// https://leetcode.cn/problems/subsets-ii/
/// </summary>
public class SubsetsWithDupSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,2]").ToArray(), StringTo<List<List<int>>>("[[],[1],[1,2],[1,2,2],[2],[2,2]]").ToArray() };
            yield return new object[] { StringTo<int[]>("[0]").ToArray(), StringTo<List<List<int>>>("[[],[0]]").ToArray() };
        }
    }

    [Data]
    public IList<IList<int>> SubsetsWithDup(int[] nums)
    {
        Array.Sort(nums);
        List<IList<int>> ans = new List<IList<int>>();
        //backtrack(nums, ans, new Stack<int>(), 0);
        backtrack1(nums, ans, new List<int>(), 0);
        return ans;
    }

    private void backtrack(int[] nums, List<IList<int>> res, Stack<int> tempList, int level)
    {
        //每条路径上所选的元素组成的数组都是子集，所以都要添加到集合res中
        res.Add(new List<int>(tempList));
        //这里遍历的时候每次都是从之前选择元素的下一个开始，所以这里i的初始值是level
        for (int i = level; i < nums.Length; i++)
        {
            //剪枝，过滤掉重复的
            if (i > level && nums[i] == nums[i - 1]) continue;
            //选择当前元素
            tempList.Push(nums[i]);
            //递归到下一层
            backtrack(nums, res, tempList, i + 1);
            //撤销选择
            tempList.Pop();
        }
    }

    private void backtrack1(int[] nums, List<IList<int>> res, List<int> tempList, int level)
    {
        //每条路径上所选的元素组成的数组都是子集，所以都要添加到集合res中
        res.Add(new List<int>(tempList));
        //这里遍历的时候每次都是从之前选择元素的下一个开始，所以这里i的初始值是level
        for (int i = level; i < nums.Length; i++)
        {
            //剪枝，过滤掉重复的
            if (i > level && nums[i] == nums[i - 1]) continue;
            //选择当前元素
            tempList.Add(nums[i]);
            //递归到下一层
            backtrack1(nums, res, tempList, i + 1);
            //撤销选择
            tempList.RemoveAt(tempList.Count - 1);
        }
    }
}