﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 5847. 找到所有的农场组
/// </summary>
public class FindFarmlandSolution
{
    class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,0],[0,0]]").ToArray(), StringTo<List<int[]>>("[]").ToArray() };
            //yield return new object[] { StringTo<List<int[]>>("[[1,1],[1,1]]").ToArray(), StringTo<List<int[]>>("[[0,0,1,1]]").ToArray() };
            //yield return new object[] { StringTo<List<int[]>>("[[1,0,0],[0,1,1],[0,1,1]]").ToArray(), StringTo<List<int[]>>("[[0,0,0,0],[1,1,2,2]]").ToArray() };
            //yield return new object[] { StringTo<List<int[]>>("[[1,0,0],[0,1,1],[0,1,1]]").ToArray(), StringTo<List<int[]>>("[[0,0,0,0],[1,1,2,2]]").ToArray() };
        }
    }
    [Data]
    public int[][] FindFarmland(int[][] land)
    {
        List<int[]> res = new List<int[]>();
        int m = land.Length;
        int n = land[0].Length;
        if (land[0][0] == 1) res.Add(new int[4] { 0, 0, 0, 0 });
        //先处理左边和右边
        for (int i = 1; i < m; i++)
        {
            if (land[i][0] == 1 && land[i - 1][0] == 0) res.Add(new int[4] { i, 0, i, 0 });
        }
        for (int j = 1; j < n; j++)
        {
            if (land[0][j] == 1 && land[0][j - 1] == 0) res.Add(new int[4] { 0, j, 0, j });
        }
        //处理中间位置
        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++)
            {
                //主要寻找左上角,左和上都是0,则寻找到
                if (land[i - 1][j] == 0 && land[i][j - 1] == 0 && land[i][j] == 1)
                {
                    res.Add(new int[4] { i, j, i, j });
                }
            }
        }
        //寻找到所有起点位置之后开始寻找终点位置
        int l = res.Count;
        for (int k = 0; k < l; k++)
        {
            var c = res[k];
            int i = c[0];
            int j = c[1];
            int maxi = i;
            int maxj = j;
            while (maxi < m - 1)
            {
                if (land[maxi + 1][j] == 1) maxi++;
                else break;
            }
            while (maxj < n - 1)
            {
                if (land[i][maxj + 1] == 1) maxj++;
                else break;
            }
            c[2] = maxi;
            c[3] = maxj;
        }
        return res.ToArray();
    }
}
