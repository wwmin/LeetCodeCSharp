namespace Solutions;
/// <summary>
/// 901. 优势洗牌
/// https://leetcode.cn/problems/advantage-shuffle/
/// </summary>
public class AdvantageCountSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,7,11,15]").ToArray(), StringTo<int[]>("[1,10,4,11]").ToArray(), StringTo<int[]>("[2,11,7,15]").ToArray() };
            yield return new object[] { StringTo<int[]>("[12,24,8,32]").ToArray(), StringTo<int[]>("[13,25,32,11]").ToArray(), StringTo<int[]>("[24,32,8,12]").ToArray() };
        }
    }

    [Data]
    public int[] AdvantageCount(int[] nums1, int[] nums2)
    {
        //贪心思想
        int n = nums1.Length;
        int[] idx1 = new int[n];
        int[] idx2 = new int[n];
        for (int i = 0; i < n; i++)
        {
            idx1[i] = i;
            idx2[i] = i;
        }
        Array.Sort(idx1, (i, j) => nums1[i] - nums1[j]);
        Array.Sort(idx2, (i, j) => nums2[i] - nums2[j]);
        int[] ans = new int[n];
        int left = 0, right = n - 1;
        for (int i = 0; i < n; i++)
        {
            if (nums1[idx1[i]] > nums2[idx2[left]])
            {
                ans[idx2[left]] = nums1[idx1[i]];
                ++left;
            }
            else
            {
                ans[idx2[right]] = nums1[idx1[i]];
                --right;
            }
        }
        return ans;
    }
}