namespace Solutions;
/// <summary>
/// 1876. 地图中的最高点
/// </summary>
public class HighestPeakSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,1],[0,0]]").ToArray(), StringTo<List<int[]>>("[[1,0],[2,1]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[0,0,1],[1,0,0],[0,0,0]]").ToArray(), StringTo<List<int[]>>("[[1,1,0],[0,1,1],[1,2,2]]").ToArray() };
        }
    }
    private int[][] dirs = { new int[] { -1, 0 }, new int[] { 1, 0 }, new int[] { 0, -1 }, new int[] { 0, 1 } };
    [Data]
    public int[][] HighestPeak(int[][] isWater)
    {
        int m = isWater.Length;
        int n = isWater[0].Length;
        int[][] ans = new int[m][];
        for (int i = 0; i < m; i++)
        {
            ans[i] = new int[n];
            Array.Fill(ans[i], -1);//-1表示该格子尚未被访问过
        }
        Queue<Tuple<int, int>> queue = new Queue<Tuple<int, int>>();
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (isWater[i][j] == 1)
                {
                    ans[i][j] = 0;
                    queue.Enqueue(new Tuple<int, int>(i, j));//将所有水域入队
                }
            }
        }
        while (queue.Count > 0)
        {
            Tuple<int, int> p = queue.Dequeue();
            foreach (int[] dir in dirs)
            {
                int x = p.Item1 + dir[0];
                int y = p.Item2 + dir[1];
                if (0 <= x && x < m && 0 <= y && y < n && ans[x][y] == -1)
                {
                    ans[x][y] = ans[p.Item1][p.Item2] + 1;
                    queue.Enqueue(new Tuple<int, int>(x, y));
                }
            }
        }
        return ans;
    }

}