﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 33. 搜索旋转排序数组
/// </summary>
public class SearchSolution
{
    [InlineData(new int[] { 4, 5, 6, 7, 0, 1, 2 }, 0, 4)]
    [InlineData(new int[] { 4, 5, 6, 7, 0, 1, 2 }, 5, 1)]
    public int Search(int[] nums, int target)
    {
        int n = nums.Length;
        if (n == 0) return -1;
        if (n == 1) return nums[0] == target ? 0 : -1;
        int left = 0;
        int right = n - 1;
        while (left <= right)
        {
            if (nums[left] == target) return left;
            if (nums[right] == target) return right;
            int mid = (left + right) / 2;
            if (nums[mid] == target) return mid;
            //左侧小于中间值 说明左侧是有序的
            if (nums[left] < nums[mid])
            {
                if (nums[left] < target && nums[mid] > target)
                {
                    right = mid - 1;
                }
                else
                {
                    left = mid + 1;
                }
            }
            else
            {
                //此部分不是连续数
                if (nums[right] > target && nums[mid] < target)
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }
        }
        return -1;
    }
}
