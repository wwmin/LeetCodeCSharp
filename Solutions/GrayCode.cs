namespace Solutions;
/// <summary>
/// 89. 格雷编码
/// </summary>
public class GrayCodeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, StringTo<List<int>>("[0,1,3,2]") };
            //yield return new object[] { 1, StringTo<List<int>>("[0,1]") };
        }
    }

    [Data]
    public IList<int> GrayCode(int n)
    {
        IList<int> ans = new List<int>();
        ans.Add(0);
        for (int i = 1; i <= n; i++)
        {
            int m = ans.Count;
            for (int j = m - 1; j >= 0; j--)
            {
                //将1 向左移动 i-1 位 然后与 ans[j] 取反
                //Console.WriteLine($"1 << ({i} - 1) => " + (1 << (i - 1)).ToBase2());
                //Console.WriteLine($"ans[{j}] => " + ans[j].ToBase2());
                //Console.WriteLine($"(ans[{j}] | (1 << ({i} - 1)) => " + (ans[j] | (1 << (i - 1))).ToBase2());
                ans.Add(ans[j] | (1 << (i - 1)));
            }
        }
        return ans;
    }
}

public static class Utils
{
    public static string ToBase2(this int i)
    {
        return Convert.ToString(i, toBase: 2);
    }
}