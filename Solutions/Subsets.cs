namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 78. 子集
/// </summary>
public class SubsetsSolution
{

    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), StringTo<List<List<int>>>("[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]").ToArray() };
            yield return new object[] { StringTo<int[]>("[0]").ToArray(), StringTo<List<List<int>>>("[[],[0]]").ToArray() };
        }
    }

    private List<int> dataList = null;
    private List<List<int>> res = null;
    [Data]
    public IList<IList<int>> Subsets(int[] nums)
    {
        dataList = new List<int>();
        res = new List<List<int>>(); ;
        dfs(0, nums);
        return res.ToArray();
    }

    private void dfs(int cur, int[] nums)
    {
        if (cur == nums.Length)
        {
            res.Add(new List<int>(dataList));
            return;
        }
        dataList.Add(nums[cur]);
        dfs(cur + 1, nums);
        dataList.RemoveAt(dataList.Count - 1);
        dfs(cur + 1, nums);
    }
}