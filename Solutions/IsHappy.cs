namespace Solutions;
/// <summary>
/// 202. 快乐数
/// </summary>
public class IsHappySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 19, true };
            yield return new object[] { 2, false };
        }
    }

    [Data]
    public bool IsHappy(int n)
    {
        //保存每次的字符串数字, 用来判断是否无限循环了
        Dictionary<string, bool> dic = new Dictionary<string, bool>();
        dic.Add(n.ToString(), true);
        while (n != 1)
        {
            string ns = n.ToString();
            int len = ns.Length;
            int sum = 0;
            for (int i = 0; i < len; i++)
            {
                int ni = Convert.ToInt32(ns[i].ToString());
                sum += ni * ni;
            }
            if (sum == 1) return true;
            if (dic.ContainsKey(sum.ToString()))
            {
                return false;
            }
            else
            {
                dic.Add(sum.ToString(), true);
            }
            n = sum;
        }
        return true;
    }
}