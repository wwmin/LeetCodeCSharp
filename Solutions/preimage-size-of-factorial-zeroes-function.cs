namespace Solutions;
/// <summary>
/// 809. 阶乘函数后 K 个零
/// https://leetcode.cn/problems/preimage-size-of-factorial-zeroes-function/
/// </summary>
public class PreimageSizeFZFSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 0, 5 };
            yield return new object[] { 5, 0 };
            yield return new object[] { 3, 5 };
        }
    }

    [Data]
    public int PreimageSizeFZF(int k)
    {
        return (int)(Help(k + 1) - Help(k));
    }

    public long Help(int k)
    {
        long right = 5L * k;
        long left = 0;
        while (left <= right)
        {
            long mid = (left + right) / 2;
            if (Zeta(mid) < k)
            {
                left = mid + 1;
            }
            else
            {
                right = mid - 1;
            }
        }
        return right + 1;
    }

    public long Zeta(long x)
    {
        long res = 0;
        while (x != 0)
        {
            res += x / 5;
            x /= 5;
        }
        return res;
    }
}