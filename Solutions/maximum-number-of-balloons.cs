namespace Solutions;
/// <summary>
/// 1297. “气球” 的最大数量
/// </summary>
public class MaxNumberOfBalloonsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "nlaebolko", 1 };
            yield return new object[] { "loonbalxballpoon", 2 };
            yield return new object[] { "leetcode", 0 };
        }
    }

    [Data]
    public int MaxNumberOfBalloons(string text)
    {
        Dictionary<char, int> map = new Dictionary<char, int>();
        map.Add('a', 0);
        map.Add('b', 0);
        map.Add('l', 0);
        map.Add('o', 0);
        map.Add('n', 0);
        foreach (var c in text)
        {
            if (map.ContainsKey(c)) map[c]++;
        }
        int ans = 0;
        while (true)
        {
            if (map['a'] >= 1 && map['b'] >= 1 && map['l'] >= 2 && map['o'] >= 2 && map['n'] >= 1)
            {
                ans++;
                map['a'] -= 1;
                map['b'] -= 1;
                map['l'] -= 2;
                map['o'] -= 2;
                map['n'] -= 1;
            }
            else
            {
                break;
            }
        }
        return ans;
    }
}