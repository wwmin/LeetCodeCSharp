namespace Solutions;
/// <summary>
/// 81. 搜索旋转排序数组 II
/// </summary>
public class Search_81Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { StringTo<int[]>("[2,5,6,0,0,1,2]").ToArray(), 0, true };
            yield return new object[] { StringTo<int[]>("[2,5,6,0,0,1,2]").ToArray(), 3, false };
        }
    }

    [Data]
    public bool Search_81(int[] nums, int target)
    {
        int n = nums.Length;
        if (n == 0) return false;
        if (n == 1) return nums[0] == target;
        int left = 0;
        int right = n - 1;
        while (left <= right)
        {
            int mid = (left + right) / 2;
            if (nums[mid] == target)
            {
                return true;
            }
            if (nums[left] == nums[mid] && nums[mid] == nums[right])
            {
                left++;
                right--;
            }
            else if (nums[left] <= nums[mid])
            {
                if (nums[left] <= target && target < nums[mid])
                {
                    right = mid - 1;
                }
                else
                {
                    left = mid + 1;
                }
            }
            else
            {
                if (nums[mid] < target && target <= nums[n - 1])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }
        }
        return false;
    }
}