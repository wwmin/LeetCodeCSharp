namespace Solutions;
/// <summary>
/// 1580. 重新排列数组
/// https://leetcode.cn/problems/shuffle-the-array/
/// </summary>
public class ShuffleSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,5,1,3,4,7]").ToArray(), 3, StringTo<int[]>("[2,3,5,4,1,7]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,2,3,4,4,3,2,1]").ToArray(), 4, StringTo<int[]>("[1,4,2,3,3,2,4,1]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,1,2,2]").ToArray(), 2, StringTo<int[]>("[1,2,1,2]").ToArray() };
        }
    }

    [Data]
    public int[] Shuffle(int[] nums, int n)
    {
        int[] res = new int[2 * n];
        for (int i = 0; i < n; i++)
        {
            res[2 * i] = nums[i];
            res[2 * i + 1] = nums[i + n];
        }
        return res;
    }
}