namespace Solutions;
/// <summary>
/// 657. 机器人能否返回原点
/// https://leetcode.cn/problems/robot-return-to-origin/
/// </summary>
public class JudgeCircleSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{"UD", true};
            yield return new object[]{"LL", false};
        }
    }

    [Data]
    public bool JudgeCircle(string moves)
    {
        Dictionary<char, int> dic = new Dictionary<char, int>();
        dic.Add('U', 0);
        dic.Add('D', 0);
        dic.Add('L', 0);
        dic.Add('R', 0);
        for (int i = 0; i < moves.Length; i++)
        {
            dic[moves[i]]++;
        }
        if (dic['U'] == dic['D'] && dic['L'] == dic['R']) return true;
        return false;
    }
}