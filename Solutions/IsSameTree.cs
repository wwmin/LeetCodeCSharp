namespace Solutions;
/// <summary>
/// 100. 相同的树
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class IsSameTreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,2,3]"), ToTreeNodeWithString("[1,2,3]"), true };
            yield return new object[] { ToTreeNodeWithString("[1,2]"), ToTreeNodeWithString("[1,null,2]"), false };
            yield return new object[] { ToTreeNodeWithString("[1,2,1]"), ToTreeNodeWithString("[1,1,2]"), false };
        }
    }
    [Data]
    public bool IsSameTree(TreeNode p, TreeNode q)
    {
        if (p == null && q == null) return true;
        if (p == null || q == null) return false;
        if (p.val != q.val) return false;
        return IsSameTree(p.left, q.left) && IsSameTree(p.right, q.right);
    }
}