namespace Solutions;
/// <summary>
/// 383. 赎金信
/// </summary>
public class CanConstructSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "a", "b", false };
            yield return new object[] { "aa", "ab", false };
            yield return new object[] { "aa", "aab", true };
            yield return new object[] { "abcdefghijklmnopqrstuvwxyzz", "abcdefghijklmnopqrstuvwxyz", false };
        }
    }
    [Data]
    public bool CanConstruct(string ransomNote, string magazine)
    {
        int n = 'z' - 'a' + 1;
        int[] ransomNoteArr = new int[n];
        int[] magazineArr = new int[n];
        for (int i = 0; i < ransomNote.Length; i++)
        {
            ransomNoteArr[ransomNote[i] - 'a']++;
        }
        for (int i = 0; i < magazine.Length; i++)
        {
            magazineArr[magazine[i] - 'a']++;
        }
        for (int i = 0; i < n; i++)
        {
            if (ransomNoteArr[i] > magazineArr[i]) return false;
        }
        return true;
    }
}