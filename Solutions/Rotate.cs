﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 48. 旋转图像
/// </summary>
public class RotateSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2,3],[4,5,6],[7,8,9]]").ToArray(), StringTo<List<int[]>>("[[1,2,3],[4,5,6],[7,8,9]]").ToArray() };
            //yield return new object[] { StringTo<List<int[]>>("[[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]").ToArray(), StringTo<List<int[]>>("[[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]").ToArray() };
        }
    }
    [Data]
    public void Rotate(int[][] matrix)
    {
        int n = matrix.Length;
        if (n == 1) return;
        for (int i = 0; i < n; i++)
        {
            for (int j = i; j < n - i - 1; j++)
            {
                var a = matrix[i][j];
                var b = matrix[j][n - i - 1];
                var c = matrix[n - i - 1][n - j - 1];
                var d = matrix[n - j - 1][i];

                matrix[i][j] = d;
                matrix[j][n - i - 1] = a;
                matrix[n - i - 1][n - j - 1] = b;
                matrix[n - j - 1][i] = c;
            }
        }

        //Utils.PrintTwoDimensionArray(matrix);
    }

    private int[] RowN(int[][] matrix, int rowN)
    {
        int n = matrix.Length;
        int rowLen = rowN > n / 2 ? n - (n - rowN - 1) / 2 : n - rowN * 2;
        int[] row = new int[rowLen];
        for (int i = 0; i < rowLen; i++)
        {
            int start = i + (n - rowLen) / 2;
            //row[i] = matrix[]
        }
        return row;
    }

    private int[] ColN(int[][] matrix, int colN)
    {
        int n = matrix.Length;
        int colLen = colN > n / 2 ? n - (n - colN - 1) / 2 : n - colN * 2;
        int[] col = new int[colLen];
        for (int i = 0; i < colLen; i++)
        {
            int start = i + (n - colLen) / 2;
            col[i] = matrix[start][colN];
        }
        return col;
    }
}
