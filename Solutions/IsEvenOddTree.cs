namespace Solutions;
/// <summary>
/// 1731. 奇偶树
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class IsEvenOddTreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,10,4,3,null,7,9,12,8,6,null,null,2]"), true };
            yield return new object[] { ToTreeNodeWithString("[5,4,2,3,3,7]"), false };
            yield return new object[] { ToTreeNodeWithString("[5,9,1,3,5,7]"), false };
            yield return new object[] { ToTreeNodeWithString("[1]"), true };
            yield return new object[] { ToTreeNodeWithString("[11,8,6,1,3,9,11,30,20,18,16,12,10,4,2,17]"), true };
        }
    }
    [Data]
    public bool IsEvenOddTree(TreeNode root)
    {
        Queue<TreeNode> queue = new Queue<TreeNode>();
        queue.Enqueue(root);
        int level = 0;
        while (queue.Count > 0)
        {
            int size = queue.Count;
            int prev = level % 2 == 0 ? int.MinValue : int.MaxValue;
            for (int i = 0; i < size; i++)
            {
                var node = queue.Dequeue();
                int value = node.val;
                if (level % 2 == value % 2) return false;
                if ((level % 2 == 0 && value <= prev) || (level % 2 == 1 && value >= prev)) return false;
                prev = value;
                if (node.left != null)
                {
                    queue.Enqueue(node.left);
                }
                if (node.right != null)
                {
                    queue.Enqueue(node.right);
                }
            }
            level++;
        }
        return true;
    }
}