﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 21. 合并两个有序链表
/// </summary>
public class MergeTwoListsSolution
{
    private class Data : DataAttribute
    {


        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1,2,4]"), ToListNodeWithString("[1,3,4]"), ToListNodeWithString("[1,1,2,3,4,4]") };
            yield return new object[] { ToListNodeWithString("[]"), ToListNodeWithString("[0]"), ToListNodeWithString("[0]") };
            yield return new object[] { ToListNodeWithString("[2]"), ToListNodeWithString("[1]"), ToListNodeWithString("[1,2]") };
        }
    }

    [Data]
    public ListNode MergeTwoLists(ListNode l1, ListNode l2)
    {
        ListNode res = null;


        if (l1 == null && l2 == null) return res;
        else if (l1 == null) return l2;
        else if (l2 == null) return l1;
        ListNode currentL1 = l1;
        ListNode currentL2 = l2;
        if (l1.val > l2.val)
        {
            res = new ListNode(l2.val);
            currentL2 = currentL2.next;
        }
        else
        {
            res = new ListNode(l1.val);
            currentL1 = currentL1.next;
        }

        ListNode cn = res;
        while (currentL1 != null || currentL2 != null)
        {
            if (currentL1 == null)
            {
                cn.next = new ListNode(currentL2.val);
                currentL2 = currentL2.next;
            }
            else if (currentL2 == null)
            {
                cn.next = new ListNode(currentL1.val);
                currentL1 = currentL1.next;
            }
            else if (currentL1.val <= currentL2.val)
            {
                cn.next = new ListNode(currentL1.val);
                currentL1 = currentL1.next;
            }
            else
            {
                cn.next = new ListNode(currentL2.val);
                currentL2 = currentL2.next;
            }
            cn = cn.next;
        }

        return res;
    }

    /// <summary>
    /// 递归解法
    /// </summary>
    /// <param name="l1"></param>
    /// <param name="l2"></param>
    /// <returns></returns>
    [Data]
    public ListNode MergeTwoLists2(ListNode l1, ListNode l2)
    {
        if (l1 == null)
        {
            return l2;
        }
        else if (l2 == null)
        {
            return l2;
        }
        else if (l1.val < l2.val)
        {
            l1.next = MergeTwoLists2(l1.next, l2);
            return l1;
        }
        else
        {
            l2.next = MergeTwoLists2(l2.next, l1);
            return l2;
        }
    }
}