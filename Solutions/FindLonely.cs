﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5990. 找出数组中的所有孤独数字
    /// </summary>
    public class FindLonelySolution
    {
        private class Data : DataAttribute
        {
            public Data()
            {
                IgnoreOrder = true;
            }
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { StringTo<int[]>("[0]").ToArray(), StringTo<int[]>("[0]").ToArray() };
                yield return new object[] { StringTo<int[]>("[0,1000000]").ToArray(), StringTo<int[]>("[0,1000000]").ToArray() };
                yield return new object[] { StringTo<int[]>("[10,6,5,8]").ToArray(), StringTo<int[]>("[10,8]").ToArray() };
                yield return new object[] { StringTo<int[]>("[1,3,5,3]").ToArray(), StringTo<int[]>("[1,5]").ToArray() };
                yield return new object[] { StringTo<int[]>("[62,35,59,55,84,61,38,87,55,82]").ToArray(), StringTo<int[]>("[35,59,84,38,87,82] ").ToArray() };
                yield return new object[] { StringTo<int[]>("[75,35,59,66,69,53,37,16,60,98,11,33,3,85,59,65,59,44,34,89,72,47]").ToArray(), StringTo<int[]>("[75,69,53,37,16,98,11,3,85,44,89,72,47]").ToArray() };
                yield return new object[] { StringTo<int[]>("[623,163,881,178,203,414,987,708,93,666,797,58,716,143,733,933,275,431,708,760,230,986,183,745,199,780,828,902,686,951,727,706,348,35,942,567,94,451,35,39,312,865,958,607,825,404,370,462,654,334,547,961,694,644,811,728,295,162,494,946,958,684,951,452,262,502,614,295,775,881,816,748,48,47,90,410,250,740,681,636,69,594,748,694,809,991,214,748,669,343,24,701,276,121,659,841,828,978,844,785,792,392,460,149,260,669,961,72,78,750,51,267,618,35,21,414,892,951,666,934,175,706,934,654,275,880,742,485,284,735,540,78,469,14,657,62,488,465,421,626,251,254,915,626,131,261,998,733,488,379,189,589,387,96,486,230,401,991,607,813,703,421,379,339,201,370,797,865,376,199,666,267,946,16,676,689,795,786,344,61,412,222,636,119,892,642,540,391,145,182,378,245,15,545,687,846,866,712,72,571,836,17,254,70,508,978,215,183,908,694,348,8,530,15,846,61,461,977,162,483,794,922,642,607,484,469,987,890,189,687,335,725,251,131,842,191,117,196,146,345,767,463,837,431,117,172,57,435,321,792,262,741,626,564,485,937,267,3,998,479,214,955,353,579,720,495,72,841,844,276,492,401,24,813,626,178,508,479,191,927,502,853,451,504,607,461,672,110,633,650,367,60,461,983,933,182,805]").ToArray(), StringTo<int[]>("[623,203,716,143,760,745,780,902,942,567,39,312,825,404,547,644,811,684,614,775,816,90,410,681,594,809,701,121,659,149,750,51,618,21,175,284,735,657,465,915,589,387,96,703,339,201,376,676,689,412,222,119,245,545,712,571,908,8,530,922,890,725,196,767,172,435,321,564,937,3,955,353,579,720,492,927,853,504,672,110,633,650,367,983,805]").ToArray() };
            }
        }
        [Data]
        public IList<int> FindLonely(int[] nums)
        {
            int n = nums.Length;
            if (n == 1) return nums;
            if (n == 2 && nums[0] != nums[1] && nums[0] != nums[1] + 1 && nums[0] != nums[1] - 1) return nums;
            Dictionary<int, int> map = new Dictionary<int, int>();
            foreach (var num in nums)
            {
                if (map.ContainsKey(num))
                {
                    map[num]++;
                }
                else
                {
                    map[num] = 1;
                }
            }

            List<int> ans = new List<int>();
            var keys = map.Keys.ToArray();
            Array.Sort(keys);
            int kn = keys.Length;
            for (int i = 0; i < kn - 1; i++)
            {
                int cur = map[keys[i]];
                if (cur > 1) continue;
                if (keys[i] != keys[i + 1] - 1)
                {
                    if (i > 0 && keys[i - 1] != keys[i] - 1)
                    {
                        ans.Add(keys[i]);
                    }
                    else if (i == 0)
                    {
                        ans.Add(keys[i]);
                    }
                }
            }
            //判断最后一个数
            if (kn - 2 > 0)
            {
                if (map[keys[kn - 1]] == 1)
                {
                    if (keys[kn - 1] != keys[kn - 2] + 1)
                    {
                        ans.Add(keys[kn - 1]);
                    }
                }
            }
            return ans.ToArray();
        }
    }
}