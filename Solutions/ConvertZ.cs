﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 6. Z 字形变换
/// </summary>
public class ConvertZ
{
    public void Test()
    {
        var t0 = "A";
        var n0 = 1;
        var s0 = Convert(t0, n0);
        Console.WriteLine(s0);
        var t1 = "PAYPALISHIRING";
        var n1 = 4;
        var s1 = Convert(t1, n1);
        Console.WriteLine(s1);
        var t2 = "PAYPALISHIRING";
        var n2 = 3;
        var s2 = Convert(t2, n2);
        Console.WriteLine(s2);
    }
    public string Convert(string s, int numRows)
    {
        var sCols = new List<List<char>>();
        for (int i = 0; i < numRows; i++)
        {
            sCols.Add(new List<char>());
        }
        var sl = s.Length;
        var j = 0;
        for (int i = 0; i < sl; i++)
        {
            if (j >= numRows + numRows - 2) j = 0;
            if (j < numRows)
            {
                var scl = sCols[j];
                scl.Add(s[i]);
                j++;
            }
            else
            {
                var n = j - numRows;
                var scl = sCols[numRows - (n + 2)];
                scl.Add(s[i]);
                j++;
            }
        }
        var res = new StringBuilder();
        for (int i = 0; i < numRows; i++)
        {
            var si = sCols[i];
            var sil = si.Count();
            for (int k = 0; k < sil; k++)
            {
                res.Append(si[k]);
            }
        }
        return res.ToString();
    }
}
