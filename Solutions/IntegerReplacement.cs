namespace Solutions;
/// <summary>
/// 397. 整数替换
/// </summary>
public class IntegerReplacementSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 8, 3 };
            yield return new object[] { 7, 4 };
            yield return new object[] { 4, 2 };
        }
    }

    private Dictionary<long, int> map = new Dictionary<long, int>();
    [Data]
    public int IntegerReplacement(int n)
    {
        return dfs(n * 1L);
    }

    private int dfs(long n)
    {
        //n==1 不能再继续除2, 故直接返回结果0
        if (n == 1) return 0;
        //判断是否有遍历过此n的步骤,如果有则直接返回该n结果值
        if (map.ContainsKey(n)) return map[n];
        //遍历n的步骤,如果能整除2则继续遍历,如果不能则取n+1与n-1的步骤
        int ans = n % 2 == 0 ? dfs(n / 2) : Math.Min(dfs(n + 1), dfs(n - 1));
        //++ans 表示答案增加1, 而map[n]=ans表示记录该n时的步骤数,以便后面直接使用该值不重复dfs
        map[n] = ++ans;
        return ans;
    }
}