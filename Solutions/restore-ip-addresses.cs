namespace Solutions;
/// <summary>
/// 93. 复原 IP 地址
/// difficulty: Medium
/// https://leetcode.cn/problems/restore-ip-addresses/
/// </summary>
public class RestoreIpAddresses_93_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "25525511135", StringTo<List<string>>("[\"255.255.11.135\",\"255.255.111.35\"]") };
            yield return new object[] { "0000", StringTo<List<string>>("[\"0.0.0.0\"]") };
            yield return new object[] { "101023", StringTo<List<string>>("[\"1.0.10.23\",\"1.0.102.3\",\"10.1.0.23\",\"10.10.2.3\",\"101.0.2.3\"]") };
        }
    }

    private List<string> result;
    [Data]
    public IList<string> RestoreIpAddresses(string s)
    {
        result = new List<string>();
        List<string> path = new List<string>();
        DFS(s, 0, path, 0);
        return result;
    }

    private void DFS(string s, int depth, List<string> path, int index)
    {
        if (depth != 0)
        {
            var value = path[path.Count - 1];
            if (depth == 4)
            {
                if (index < s.Length)
                {
                    return;
                }
                else if (value.Length > 1 && value.IndexOf('0') == 0) return;
                else if (int.Parse(value) < 0 || int.Parse(value) > 255)
                {
                    return;
                }
                result.Add(string.Join(".", path));
                return;
            }
            else
            {
                if (value.Length > 1 && value.IndexOf('0') == 0) return;
                else if (int.Parse(value) < 0 || int.Parse(value) > 255)
                {
                    return;
                }
            }
        }

        for (var i = index; i < s.Length && i < index + 4; i++)
        {
            var number = s.Substring(index, i - index + 1);
            path.Add(number);

            DFS(s, depth + 1, path, i + 1);

            path.RemoveAt(path.Count - 1);
        }
    }
}