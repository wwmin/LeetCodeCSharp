namespace Solutions;
/// <summary>
/// 825. 保持城市天际线
/// </summary>
public class MaxIncreaseKeepingSkylineSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[3,0,8,4],[2,4,5,7],[9,2,6,3],[0,3,1,0]]").ToArray(), 35 };
            yield return new object[] { StringTo<List<int[]>>("[[0,0,0],[0,0,0],[0,0,0]]").ToArray(), 0 };
        }
    }

    [Data]
    public int MaxIncreaseKeepingSkyline(int[][] grid)
    {
        int ans = 0;
        int n = grid.Length;
        int[] rows = new int[n];
        int[] cols = new int[n];
        for (int i = 0; i < n; i++)
        {
            rows[i] = grid[i].Max();
            for (int j = 0; j < n; j++)
            {
                if (cols[i] < grid[j][i]) cols[i] = grid[j][i];
            }
        }
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] < rows[i] && grid[i][j] < cols[j])
                {
                    //Console.WriteLine("rows[i]:" + rows[i]);
                    //Console.WriteLine("cols[j]:" + cols[j]);
                    //Console.WriteLine("grid[i][j]:" + grid[i][j]);
                    var v = (rows[i] < cols[j] ? rows[i] : cols[j]) - grid[i][j];
                    //Console.WriteLine(v);
                    ans += v;
                }
            }
        }
        return ans;
    }
}