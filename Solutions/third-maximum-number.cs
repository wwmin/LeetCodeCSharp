namespace Solutions;
/// <summary>
/// 414. 第三大的数
/// </summary>
public class ThirdMaxSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[3,2,1]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,2]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[2,2,3,1]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[-2147483648,1,1]").ToArray(), 1 };
        }
    }

    [Data]
    public int ThirdMax(int[] nums)
    {
        Array.Sort(nums);
        IList<int> list = new List<int>();
        list.Add(nums[nums.Length - 1]);
        for (int i = nums.Length - 2; i >= 0; i--)
        {
            if (list.Count < 3 && list[list.Count - 1] != nums[i])
            {
                list.Add(nums[i]);
                if (list.Count == 3) break;
            }
        }
        return list.Count == 3 ? list[list.Count - 1] : list[0];
    }
}