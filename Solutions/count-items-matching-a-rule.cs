namespace Solutions;
/// <summary>
/// 1899. 统计匹配检索规则的物品数量
/// https://leetcode.cn/problems/count-items-matching-a-rule/
/// </summary>
public class CountMatchesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<List<string>>>("[[\"phone\",\"blue\",\"pixel\"],[\"computer\",\"silver\",\"lenovo\"],[\"phone\",\"gold\",\"iphone\"]]").ToArray(), "color", "silver", 1 };
            yield return new object[] { StringTo<List<List<string>>>("[[\"phone\",\"blue\",\"pixel\"],[\"computer\",\"silver\",\"phone\"],[\"phone\",\"gold\",\"iphone\"]]").ToArray(), "type", "phone", 2 };
        }
    }

    [Data]
    public int CountMatches(IList<IList<string>> items, string ruleKey, string ruleValue)
    {
        return items.Where(p =>
        {
            return ruleKey switch
            {
                "type" => p[0] == ruleValue,
                "color" => p[1] == ruleValue,
                "name" => p[2] == ruleValue,
                _ => false,
            };
        }).Count();
    }
}