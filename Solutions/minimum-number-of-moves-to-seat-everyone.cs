namespace Solutions;
/// <summary>
/// 2037. 使每位学生都有座位的最少移动次数
/// difficulty: Easy
/// https://leetcode.cn/problems/minimum-number-of-moves-to-seat-everyone/
/// </summary>
public class MinMovesToSeat_2037_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[3,1,5]").ToArray(), StringTo<int[]>("[2,7,4]").ToArray(), 4 };
            yield return new object[] { StringTo<int[]>("[4,1,5,9]").ToArray(), StringTo<int[]>("[1,3,2,6]").ToArray(), 7 };
            yield return new object[] { StringTo<int[]>("[2,2,6,6]").ToArray(), StringTo<int[]>("[1,3,2,6]").ToArray(), 4 };
        }
    }

    [Data]
    public int MinMovesToSeat(int[] seats, int[] students)
    {
        Array.Sort(seats);
        Array.Sort(students);
        var sum = 0;
        for (int i = 0; i < seats.Length; i++)
        {
            sum += Math.Abs(seats[i] - students[i]);
        }
        return sum;
    }
}