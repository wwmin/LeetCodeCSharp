﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5997. 找到和为给定整数的三个连续整数
    /// </summary>
    public class SumOfThreeSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                //yield return new object[] { 33, StringTo<int[]>("[10,11,12]").ToArray() };
                //yield return new object[] { 4, StringTo<int[]>("[]").ToArray() };
                //yield return new object[] { 3569285238, StringTo<long[]>("[1189761745,1189761746,1189761747]").ToArray() };
                yield return new object[] { 6675975537, StringTo<long[]>("[2225325178,2225325179,2225325180]").ToArray() };
            }
        }
        [Data]
        public long[] SumOfThree(long num)
        {
            //整数可以是负数,切记
            //if (num == 0) return new long[0];
            long c = num % 3;
            if (c != 0) return new long[0];
            long[] ans = new long[3];
            long a = num / 2;
            ans[0] = a - 1;
            ans[1] = a;
            ans[2] = a + 1;
            return ans;
        }
    }
}
