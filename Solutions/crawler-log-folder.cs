namespace Solutions;
/// <summary>
/// 1720. 文件夹操作日志搜集器
/// https://leetcode.cn/problems/crawler-log-folder/
/// </summary>
public class MinOperationsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"d1/\",\"d2/\",\"../\",\"d21/\",\"./\"]").ToArray(), 2 };
            yield return new object[] { StringTo<string[]>("[\"d1/\",\"d2/\",\"./\",\"d3/\",\"../\",\"d31/\"]").ToArray(), 3 };
            yield return new object[] { StringTo<string[]>("[\"d1/\",\"../\",\"../\",\"../\"]").ToArray(), 0 };
        }
    }

    [Data]
    public int MinOperations(string[] logs)
    {
        int dept = 0;
        var n = logs.Length;
        for (int i = 0; i < n; i++)
        {
            if (logs[i] == "../") dept = dept == 0 ? 0 : dept - 1;
            else if (logs[i] != "./") dept++;
        }
        return dept;
    }
}