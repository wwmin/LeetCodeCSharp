namespace Solutions;
/// <summary>
/// 258. 各位相加
/// </summary>
public class AddDigitsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 38, 2 };
        }
    }

    [Data]
    public int AddDigits(int num)
    {
        string ns = num.ToString();
        while (ns.Length > 1)
        {
            int n = ns.Length;
            int sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += Convert.ToInt32(ns[i].ToString());
            }
            if (sum < 10) return sum;
            ns = sum.ToString();
        }
        return num;
    }
}