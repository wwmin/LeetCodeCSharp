namespace Solutions;
/// <summary>
/// 415. 字符串相加
/// </summary>
public class AddStringsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "11", "123", "134" };
            yield return new object[] { "456", "77", "533" };
            yield return new object[] { "0", "0", "0" };
            yield return new object[] { "1", "9", "10" };
        }
    }

    [Data]
    public string AddStrings(string num1, string num2)
    {
        StringBuilder sb = new StringBuilder();
        int n1 = num1.Length - 1;
        int n2 = num2.Length - 1;
        int max_n = Math.Max(n1, n2) + 1;
        int hight_one = 0;
        for (int i = 0; i < max_n; i++)
        {
            int c1 = 0;
            int c2 = 0;
            if (n1 - i >= 0) c1 = num1[n1 - i] - '0';
            if (n2 - i >= 0) c2 = num2[n2 - i] - '0';
            int s = c1 + c2 + hight_one;
            if (s < 10)
            {
                hight_one = 0;
                sb.Append(s.ToString());
            }
            else
            {
                hight_one = 1;
                sb.Append((s - 10).ToString());
            }
        }
        if (hight_one == 1)
        {
            sb.Append('1');
        }
        string ans = sb.ToString();
        ans = string.Join("", ans.Reverse());
        return ans;
    }
}