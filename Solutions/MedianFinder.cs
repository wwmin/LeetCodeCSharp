﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 295. 数据流的中位数 
/// </summary>
public class MedianFinder
{
    private SortedList<int, int> nums;

    int n;
    int[] left;
    int[] right;
    /** initialize your data structure here. */
    public MedianFinder()
    {
        nums = new SortedList<int, int>();
        n = 0;
        left = new int[2];
        right = new int[2];
    }

    public void AddNum(int num)
    {
        if (nums.ContainsKey(num))
        {
            nums[num]++;
        }
        else
        {
            nums.Add(num, 1);
        }
        if (n == 0)
        {
            left[0] = num;
            right[0] = num;
            left[1] = 1;
            right[1] = 1;
        }
        else if ((n & 1) != 0)
        {
            if (num < left[0])
            {
                decrease(left);
            }
            else
            {
                increase(right);
            }
        }
        else
        {
            if (num > left[0] && num < right[0])
            {
                increase(left);
                decrease(right);
            }
            else if (num >= right[0])
            {
                increase(left);
            }
            else
            {
                decrease(right);
                Array.Copy(right, 0, left, 0, 2);
            }
        }
        n++;
    }

    public double FindMedian()
    {
        return (left[0] + right[0]) / 2.0;
    }

    private void increase(int[] iterator)
    {
        iterator[1]++;
        if (iterator[1] > nums[iterator[0]])
        {
            int keyIndex = nums.Keys.IndexOf(iterator[0]);

            iterator[0] = nums.Keys[keyIndex + 1];
            iterator[1] = 1;
        }
    }

    private void decrease(int[] iterator)
    {
        iterator[1]--;
        if (iterator[1] == 0)
        {
            int keyIndex = nums.Keys.IndexOf(iterator[0]);
            iterator[0] = nums.Keys[keyIndex - 1];
            iterator[1] = nums[iterator[0]];
        }
    }
}

/// <summary>
/// 295. 数据流的中位数 
/// 此解超时
/// </summary>
public class MedianFinder2
{
    private SortedList<int, int> _list = null;
    /** initialize your data structure here. */
    public MedianFinder2()
    {
        _list = new SortedList<int, int>();
    }

    public void AddNum(int num)
    {
        if (_list.ContainsKey(num)) _list[num]++;
        else _list.Add(num, 1);
    }

    public double FindMedian()
    {
        var n = _list.Values.Sum();
        if (n < 2)
        {
            if (n == 0) return 0;
            else return _list.Keys[0];
        };
        var ml = n % 2;
        var mm = n / 2;
        if (ml == 1) mm++;
        double sum = 0;
        int l = _list.Count;
        for (int i = 0; i < l; i++)
        {
            sum += _list[_list.Keys[i]];
            if (sum == mm)
            {
                if (ml == 0)
                {
                    return (_list.Keys[i] + _list.Keys[i + 1]) / 2.0;
                }
                else
                {
                    return _list.Keys[i];
                }
            }
            else if (sum > mm)
            {
                return _list.Keys[i];
            };
        }
        return 0;
    }
}


public class MedianFinderSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[],[1],[2],[],[3],[]]").ToArray(), new double?[] { null, null, null, 1.5, null, 2.0 } };
        }
    }

    [Data]
    public void TestMedianFinder(int[][] nums)
    {
        {
            MedianFinder obj = new MedianFinder();
            obj.AddNum(1);
            obj.AddNum(2);
            double param_2 = obj.FindMedian();
            Console.WriteLine(param_2.ToString());
            obj.AddNum(3);
            Console.WriteLine(obj.FindMedian());
        }

        {
            MedianFinder obj = new MedianFinder();
            obj.AddNum(0);
            obj.AddNum(0);
            double param_2 = obj.FindMedian();
            Console.WriteLine(param_2.ToString());
        }

        {
            MedianFinder obj = new MedianFinder();
            obj.AddNum(2);
            Console.WriteLine(obj.FindMedian());
            obj.AddNum(3);
            double param_2 = obj.FindMedian();
            Console.WriteLine(param_2.ToString());
        }
    }
}