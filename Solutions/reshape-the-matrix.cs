namespace Solutions;
/// <summary>
/// 566. 重塑矩阵
/// https://leetcode.cn/problems/reshape-the-matrix/
/// </summary>
public class MatrixReshapeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2],[3,4]]").ToArray(), 1, 4, StringTo<List<int[]>>("[[1,2,3,4]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,2],[3,4]]").ToArray(), 2, 4, StringTo<List<int[]>>("[[1,2],[3,4]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,2,3,4]]").ToArray(), 2, 2, StringTo<List<int[]>>("[[1,2],[3,4]]").ToArray() };
        }
    }

    [Data]
    public int[][] MatrixReshape(int[][] mat, int r, int c)
    {
        if (mat.Length * mat[0].Length != r * c)
        {
            return mat;
        }
        int[][] res = new int[r][];
        for (int i = 0; i < r; i++)
        {
            res[i] = new int[c];
        }
        int rr = 0;
        int cc = 0;
        for (int i = 0; i < mat.Length; i++)
        {
            for (int j = 0; j < mat[i].Length; j++)
            {
                if (cc == c)
                {
                    cc = 0;
                    rr++;
                }

                res[rr][cc] = mat[i][j];
                cc++;
            }
        }
        return res;
    }
}