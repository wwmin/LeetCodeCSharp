namespace Solutions;
/// <summary>
/// 1698. 替换所有的问号
/// </summary>
public class ModifyStringSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "?zs", "azs" };
            yield return new object[] { "?a?ub???w?b", "babubacawab" };
            yield return new object[] { "b?c", "bac" };
            yield return new object[] { "ubv?w", "ubvaw" };
            yield return new object[] { "j?qg??b", "jaqgacb" };
            yield return new object[] { "??yw?ipkj?", "abywaipkja" };
        }
    }
    [Data]
    public string ModifyString(string s)
    {
        int n = s.Length;
        if (n == 1 && s == "?") return "a";
        char[] sa = s.ToCharArray();
        if (sa[0] == '?')
        {
            if (sa[1] == 'a') sa[0] = 'b';
            else sa[0] = 'a';
        }
        if (sa[n - 1] == '?')
        {
            if (sa[n - 2] == 'a') sa[n - 1] = 'b';
            else sa[n - 1] = 'a';
        }
        for (int i = 1; i < n - 1; i++)
        {
            if (sa[i] == '?')
            {
                if ((sa[i + 1] == 'a' && sa[i - 1] == 'b') || (sa[i + 1] == 'b' && sa[i - 1] == 'a')) sa[i] = 'c';
                else if (sa[i + 1] == 'a' || sa[i - 1] == 'a') sa[i] = 'b';
                else sa[i] = 'a';
            }
        }

        return string.Join("", sa);
    }
}