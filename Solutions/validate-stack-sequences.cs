namespace Solutions;
/// <summary>
/// 983. 验证栈序列
/// https://leetcode.cn/problems/validate-stack-sequences/
/// </summary>
public class ValidateStackSequencesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,4,5]").ToArray(), StringTo<int[]>("[4,5,3,2,1]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[1,2,3,4,5]").ToArray(), StringTo<int[]>("[4,3,5,1,2]").ToArray(), false };
        }
    }

    [Data]
    public bool ValidateStackSequences(int[] pushed, int[] popped)
    {
        Stack<int> stake = new Stack<int>();
        int left = 0;
        //模拟
        for (int i = 0; i < pushed.Length; i++)
        {
            var push = pushed[i];
            stake.Push(push);
            var pop = popped[left];
            if (push != pop)
            {
                continue;
            }
            //向左，向右
            stake.Pop();
            left++;
            if (left >= popped.Length) break;
            pop = popped[left];
            while (stake.TryPeek(out push))
            {
                if (push != pop) break;
                stake.Pop();
                left++;
                if (left >= popped.Length) break;
                pop = popped[left];
            }
        }
        return stake.Count == 0;
    }
}