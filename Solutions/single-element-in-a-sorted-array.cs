namespace Solutions;
/// <summary>
/// 540. 有序数组中的单一元素
/// </summary>
public class SingleNonDuplicateSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,1,2,3,3,4,4,8,8]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[3,3,7,7,10,11,11]").ToArray(), 10 };
        }
    }

    [Data]
    public int SingleNonDuplicate(int[] nums)
    {
        int left = 0;
        int right = nums.Length - 1;
        while (left < right)
        {
            //int mid = (right - left) / 2;
            int mid = left + (right - left) / 2;
            if (mid % 2 == 0)
            {
                if (nums[mid] == nums[mid + 1])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid;
                }
            }
            else
            {
                if (nums[mid] == nums[mid - 1])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid;
                }
            }
        }
        return nums[left];
    }
}