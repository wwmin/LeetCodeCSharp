namespace Solutions;
/// <summary>
/// 1944. 截断句子
/// </summary>
public class TruncateSentenceSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "Hello how are you Contestant", 4, "Hello how are you" };
            yield return new object[] { "What is the solution to this problem", 4, "What is the solution" };
            yield return new object[] { "chopper is not a tanuki", 5, "chopper is not a tanuki" };
        }
    }

    private const char space_char = ' ';
    [Data]
    public string TruncateSentence(string s, int k)
    {
        StringBuilder sb = new StringBuilder();
        int n = s.Length;
        int i = 0;
        while (i < n)
        {
            if (s[i] == space_char)
            {
                k--;
                if (k == 0) break;
            }
            sb.Append(s[i]);
            i++;
        }
        return sb.ToString();
    }
}