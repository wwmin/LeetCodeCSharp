namespace Solutions;
/// <summary>
/// 212. 单词搜索 II
/// difficulty: Hard
/// https://leetcode.cn/problems/word-search-ii/
/// </summary>
public class FindWords_212_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<char[]>>("[[\'o\',\'a\',\'a\',\'n\'],[\'e\',\'t\',\'a\',\'e\'],[\'i\',\'h\',\'k\',\'r\'],[\'i\',\'f\',\'l\',\'v\']]").ToArray(), StringTo<string[]>("[\"oath\",\"pea\",\"eat\",\"rain\"]").ToArray(), StringTo<List<string>>("[\"eat\",\"oath\"]") };
            yield return new object[] { StringTo<List<char[]>>("[[\'a\',\'b\'],[\'c\',\'d\']]").ToArray(), StringTo<string[]>("[\"abcb\"]").ToArray(), StringTo<List<string>>("[]") };
        }
    }

    HashSet<string> set = new HashSet<string>();
    List<string> ans = new List<string>();
    char[][] board;
    int[][] dirs = new int[4][];
    int n;
    int m;
    bool[][] vis = new bool[15][];

    [Data]
    public IList<string> FindWords(char[][] _board, string[] words)
    {
        set.Clear();
        ans.Clear();
    
        dirs[0] = new int[2] { 1, 0 };
        dirs[1] = new int[2] { -1, 0 };
        dirs[2] = new int[2] { 0, 1 };
        dirs[3] = new int[2] { 0, -1 };
        for (int i = 0; i < vis.Length; i++)
        {
            vis[i] = new bool[15];
            for (int j = 0; j < vis[i].Length; j++)
            {
                vis[i][j] = false;
            }
        }
        board = _board;
        m = board.Length;
        n = board[0].Length;
        foreach (var item in words)
        {
            set.Add(item);
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                vis[i][j] = true;
                sb.Append(board[i][j]);
                dfs(i, j, sb);
                vis[i][j] = false;
                sb.Remove(sb.Length - 1, 1);

            }
        }
        return ans;
    }

    void dfs(int i, int j, StringBuilder sb)
    {
        if (sb.Length > 10) return;
        if (set.Contains(sb.ToString()))
        {
            ans.Add(sb.ToString());
            set.Remove(sb.ToString());
        }
        foreach (var d in dirs)
        {
            int dx = i + d[0], dy = j + d[1];
            if (dx < 0 || dx >= m || dy < 0 || dy >= n) continue;
            if (vis[dx][dy]) continue;
            vis[dx][dy] = true;
            sb.Append(board[dx][dy]);
            dfs(dx, dy, sb);
            vis[dx][dy] = false;
            sb.Remove(sb.Length - 1, 1);
        }
    }
}