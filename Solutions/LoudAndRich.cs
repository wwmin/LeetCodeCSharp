namespace Solutions;
/// <summary>
/// 881. 喧闹和富有
/// </summary>
public class LoudAndRichSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,0],[2,1],[3,1],[3,7],[4,3],[5,3],[6,3]]").ToArray(), StringTo<int[]>("[3,2,5,4,6,1,7,0]").ToArray(), StringTo<int[]>("[5,5,2,5,4,5,6,7]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[]").ToArray(), StringTo<int[]>("[0]").ToArray(), StringTo<int[]>("[0]").ToArray() };
        }
    }

    [Data]
    public int[] LoudAndRich(int[][] richer, int[] quiet)
    {
        int n = quiet.Length;
        IList<int>[] g = new List<int>[n];
        for (int i = 0; i < n; i++)
        {
            //初始化拓扑图
            g[i] = new List<int>();
        }
        //记录不是最有钱人度值,有钱人度值为0, 不是最有钱人度值大于0
        int[] inDegree = new int[n];
        foreach (int[] r in richer)
        {
            //记录有钱人指向没有他有钱的人
            g[r[0]].Add(r[1]);
            //不是最有钱人计数
            ++inDegree[r[1]];
        }
        int[] ans = new int[n];
        for (int i = 0; i < n; i++)
        {
            //和自己相等的有钱人且安静值不小于自己的就是自己,所以初始化使用自己的安静值
            ans[i] = i;
        }
        //最有钱人队列
        Queue<int> q = new Queue<int>();
        for (int i = 0; i < n; i++)
        {
            if (inDegree[i] == 0)
            {
                //最有钱人列表
                q.Enqueue(i);
            }
        }
        while (q.Count > 0)
        {
            //最有钱人依次出队
            int x = q.Dequeue();
            //最有钱人下指向的没他有钱的人列表
            foreach (int y in g[x])
            {
                if (quiet[ans[x]] < quiet[ans[y]])
                {
                    //如果此有钱人的安静值小于没他有钱人的安静值,则没钱人的值就是此时的有钱人
                    ans[y] = ans[x];
                }
                //没x有钱的人y度值减1,
                --inDegree[y];
                //如果此时y的度值变成了0,说明y前面的有钱人都遍历过了,轮到y时最有钱的了
                if (inDegree[y] == 0)
                {
                    //y前面没有比他有钱了,y成最有钱的了, 将y入队
                    q.Enqueue(y);
                }
            }
        }
        return ans;
    }
}