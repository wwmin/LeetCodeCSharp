﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 71. 简化路径
/// </summary>
public class SimplifyPathSolution
{
    [InlineData("/.", "/")]
    [InlineData("/home/", "/home")]
    [InlineData("/../", "/")]
    [InlineData("/home//foo/", "/home/foo")]
    [InlineData("/a/./b/../../c/", "/c")]
    public string SimplifyPath(string path)
    {
        List<string> res = new List<string>() { };
        var ps = path.Split("/");
        foreach (var p in ps)
        {
            switch (p)
            {
                case "":
                    break;
                case ".":
                    break;
                case "..":
                    if (res.Count > 0) res.RemoveAt(res.Count - 1);
                    break;
                default:
                    res.Add(p);
                    break;
            }
        }
        return "/" + string.Join("/", res);
    }
}
