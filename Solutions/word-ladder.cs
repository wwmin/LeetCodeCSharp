namespace Solutions;
/// <summary>
/// 127. 单词接龙
/// difficulty: Hard
/// https://leetcode.cn/problems/word-ladder/
/// </summary>
public class LadderLength_127_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "hit", "cog", StringTo<List<string>>("[\"hot\",\"dot\",\"dog\",\"lot\",\"log\",\"cog\"]"), 5 };
            yield return new object[] { "hit", "cog", StringTo<List<string>>("[\"hot\",\"dot\",\"dog\",\"lot\",\"log\"]"), 0 };
        }
    }


    int wordLength;

    [Data]
    public int LadderLength(string beginWord, string endWord, IList<string> wordList)
    {
        ISet<string> wordSet = new HashSet<string>();
        foreach (string word in wordList)
        {
            wordSet.Add(word);
        }
        if (!wordSet.Contains(endWord))
        {
            return 0;
        }
        wordLength = beginWord.Length;
        ISet<string> visited = new HashSet<string>();
        visited.Add(beginWord);
        Queue<string> queue = new Queue<string>();
        queue.Enqueue(beginWord);
        int wordCount = 0;
        while (queue.Count > 0)
        {
            wordCount++;
            int size = queue.Count;
            for (int i = 0; i < size; i++)
            {
                string word = queue.Dequeue();
                if (word.Equals(endWord))
                {
                    return wordCount;
                }
                IList<string> adjacentWords = GetAdjacentWords(word);
                foreach (string adjacent in adjacentWords)
                {
                    if (wordSet.Contains(adjacent) && visited.Add(adjacent))
                    {
                        queue.Enqueue(adjacent);
                    }
                }
            }
        }
        return 0;
    }

    public IList<string> GetAdjacentWords(string word)
    {
        IList<string> adjacentWords = new List<string>();
        char[] arr = word.ToCharArray();
        for (int i = 0; i < wordLength; i++)
        {
            char original = arr[i];
            for (char c = 'a'; c <= 'z'; c++)
            {
                if (c == original)
                {
                    continue;
                }
                arr[i] = c;
                adjacentWords.Add(new string(arr));
            }
            arr[i] = original;
        }
        return adjacentWords;
    }
}