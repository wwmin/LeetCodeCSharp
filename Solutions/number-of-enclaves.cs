namespace Solutions;
/// <summary>
/// 1073. 飞地的数量
/// </summary>
public class NumEnclavesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,0,0,0],[1,0,1,0],[0,1,1,0],[0,0,0,0]]").ToArray(), 3 };
            yield return new object[] { StringTo<List<int[]>>("[[0,1,1,0],[0,0,1,0],[0,0,1,0],[0,0,0,0]]").ToArray(), 0 };
            yield return new object[] { StringTo<List<int[]>>("[[0,0,1,1,1,0,1,1,1,0,1],[1,1,1,1,0,1,0,1,1,0,0],[0,1,0,1,1,0,0,0,0,1,0],[1,0,1,1,1,1,1,0,0,0,1],[0,0,1,0,1,1,0,0,1,0,0],[1,0,0,1,1,1,0,0,0,1,1],[0,1,0,1,1,0,0,0,1,0,0],[0,1,1,0,1,0,1,1,1,0,0],[1,1,0,1,1,1,0,0,0,0,0],[1,0,1,1,0,0,0,1,0,0,1]]").ToArray(), 7 };
        }
    }

    [Data]
    public int NumEnclaves(int[][] grid)
    {
        int m = grid.Length;
        int n = grid[0].Length;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                //在边缘上
                if (i == 0 || i == m - 1 || j == 0 || j == n - 1)
                {
                    dfs(grid, i, j);
                }
            }
        }
        int ans = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] == 1) ans++;
            }
        }
        return ans;
    }

    private void dfs(int[][] grid, int i, int j)
    {
        if (i < 0 || i >= grid.Length || j < 0 || j >= grid[0].Length || grid[i][j] == 0) return;
        grid[i][j] = 0;
        dfs(grid, i, j + 1);
        dfs(grid, i, j - 1);
        dfs(grid, i + 1, j);
        dfs(grid, i - 1, j);
        //grid[i][j] = 1;
    }
}