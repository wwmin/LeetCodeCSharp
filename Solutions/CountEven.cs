﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 6012. 统计各位数字之和为偶数的整数个数
    /// </summary>
    public class CountEvenSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { 4, 2 };
                yield return new object[] { 30, 14 };
            }
        }

        [Data]
        public int CountEven(int num)
        {
            int ans = 0;
            for (int i = 1; i <= num; i++)
            {
                if (isValidNum(i)) ans++;
            }
            return ans;
        }

        private bool isValidNum(int num)
        {
            string ns = num.ToString();
            int sum = 0;
            for (int i = 0; i < ns.Length; i++)
            {
                sum += int.Parse(ns[i].ToString());
            }
            return sum % 2 == 0;
        }
    }
}
