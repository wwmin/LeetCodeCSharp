namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 482. 密钥格式化
/// </summary>
public class LicenseKeyFormattingSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "5F3Z-2e-9-w", 4, "5F3Z-2E9W" };
            yield return new object[] { "2-5g-3-J", 2, "2-5G-3J" };
        }
    }
    [Data]
    public string LicenseKeyFormatting(string s, int k)
    {
        return default;
    }
}