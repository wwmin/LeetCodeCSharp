﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 38. 外观数列
/// </summary>
public class CountAndSaySolution
{
    [InlineData(1, "1")]
    [InlineData(2, "11")]
    [InlineData(3, "21")]
    [InlineData(4, "1211")]
    public string CountAndSay(int n)
    {
        if (n == 1) return "1";
        int i = 1;
        StringBuilder sb = new StringBuilder();
        sb.Append("1");
        while (i < n)
        {
            sb = CountGenerate(sb);
            i++;
        }
        return sb.ToString();
    }

    private StringBuilder CountGenerate(StringBuilder nums)
    {
        StringBuilder sb = new StringBuilder();
        int l = nums.Length;
        int right;
        for (int i = 0; i < l; i++)
        {
            var c = nums[i].ToString();
            right = i + 1;
            while (right < l)
            {
                if (c == nums[right].ToString()) right++;
                else break;
            }
            sb.Append((right - i) + c);
            i = right - 1;
        }
        return sb;
    }
}
