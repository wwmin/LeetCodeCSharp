﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 36. 有效的数独
/// </summary>
public class IsValidSudokuSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<char[]>>("[[\"5\",\"3\",\".\",\".\",\"7\",\".\",\".\",\".\",\".\"]\n,[\"6\",\".\",\".\",\"1\",\"9\",\"5\",\".\",\".\",\".\"]\n,[\".\",\"9\",\"8\",\".\",\".\",\".\",\".\",\"6\",\".\"]\n,[\"8\",\".\",\".\",\".\",\"6\",\".\",\".\",\".\",\"3\"]\n,[\"4\",\".\",\".\",\"8\",\".\",\"3\",\".\",\".\",\"1\"]\n,[\"7\",\".\",\".\",\".\",\"2\",\".\",\".\",\".\",\"6\"]\n,[\".\",\"6\",\".\",\".\",\".\",\".\",\"2\",\"8\",\".\"]\n,[\".\",\".\",\".\",\"4\",\"1\",\"9\",\".\",\".\",\"5\"]\n,[\".\",\".\",\".\",\".\",\"8\",\".\",\".\",\"7\",\"9\"]]").ToArray(), true };
            yield return new object[] { StringTo<List<char[]>>("[[\"8\",\"3\",\".\",\".\",\"7\",\".\",\".\",\".\",\".\"],[\"6\",\".\",\".\",\"1\",\"9\",\"5\",\".\",\".\",\".\"],[\".\",\"9\",\"8\",\".\",\".\",\".\",\".\",\"6\",\".\"],[\"8\",\".\",\".\",\".\",\"6\",\".\",\".\",\".\",\"3\"],[\"4\",\".\",\".\",\"8\",\".\",\"3\",\".\",\".\",\"1\"],[\"7\",\".\",\".\",\".\",\"2\",\".\",\".\",\".\",\"6\"],[\".\",\"6\",\".\",\".\",\".\",\".\",\"2\",\"8\",\".\"],[\".\",\".\",\".\",\"4\",\"1\",\"9\",\".\",\".\",\"5\"],[\".\",\".\",\".\",\".\",\"8\",\".\",\".\",\"7\",\"9\"]]").ToArray(), false };
            yield return new object[] { StringTo<List<char[]>>("[[\".\",\".\",\"4\",\".\",\".\",\".\",\"6\",\"3\",\".\"],[\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\"],[\"5\",\".\",\".\",\".\",\".\",\".\",\".\",\"9\",\".\"],[\".\",\".\",\".\",\"5\",\"6\",\".\",\".\",\".\",\".\"],[\"4\",\".\",\"3\",\".\",\".\",\".\",\".\",\".\",\"1\"],[\".\",\".\",\".\",\"7\",\".\",\".\",\".\",\".\",\".\"],[\".\",\".\",\".\",\"5\",\".\",\".\",\".\",\".\",\".\"],[\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\"],[\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\"]]").ToArray(), false };
        }
    }
    //状态转移
    [Data]
    public bool IsValidSudoku(char[][] board)
    {
        //定义行、列以及子数组的全排列,并且将出现的数字对应到行、列及子数独中,如果出现一次则设为true,下次如果又碰到已经设为true的位置,则说明重复了
        bool[,] rows = new bool[9, 9];
        bool[,] cols = new bool[9, 9];
        bool[,] grid = new bool[9, 9];
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (board[i][j] != '.')
                {
                    int num = board[i][j] - '1';
                    int grid_index = (i / 3) * 3 + (j / 3);
                    if (rows[i, num] || cols[j, num] || grid[grid_index, num]) return false;

                    rows[i, num] = true;
                    cols[j, num] = true;
                    grid[grid_index, num] = true;
                }
            }
        }
        return true;
    }

    //不够快
    //[Data]
    public bool IsValidSudoku2(char[][] board)
    {
        for (int i = 0; i < board.Length; i++)
        {
            //行遍历
            var r = board[i];
            if (!checkIsValid(r))
            {
                return false;
            };
            //列遍历
            var col = new char[9];
            for (int j = 0; j < 9; j++)
            {
                col[j] = board[j][i];
            }
            if (!checkIsValid(col))
            {
                return false;
            }
            //九宫格遍历
            var grid = new char[9];
            int gi = (i % 3) * 3;
            int gj = i / 3 * 3;
            int m = 0;
            for (int ii = gi; ii < gi + 3; ii++)
            {
                for (int jj = gj; jj < gj + 3; jj++)
                {
                    grid[m++] = board[ii][jj];
                }
            }
            if (!checkIsValid(grid))
            {
                return false;
            }
        }

        return true;
    }

    private bool checkIsValid(char[] nums)
    {
        Dictionary<char, int> numDic = new Dictionary<char, int>();
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] != '.')
            {
                if (!numDic.TryAdd(nums[i], 1))
                {
                    return false;
                };
            }
        }
        return true;
    }
}
