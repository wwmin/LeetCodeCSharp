﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 41. 缺失的第一个正数
/// </summary>
public class FirstMissingPositiveSolution
{
    private Dictionary<int, bool> positiveDic = new Dictionary<int, bool>();

    //整体思想:在原数组上修改,将数值和坐标值进行变换,做到想要的标记
    [InlineData(new int[] { 0 }, 1)]
    //[InlineData(new int[] { 1, 2, 0 }, 3)]
    //[InlineData(new int[] { 3, 4, -1, 1 }, 2)]
    //[InlineData(new int[] { 7, 8, 9, 11, 12 }, 1)]
    //[InlineData(new int[] { 1, 1000 }, 2)]
    public int FirstMissingPositive(int[] nums)
    {
        int n = nums.Length;
        //不满足的最大值为N+1,故可利用数组替换原则进行在空间上的优化
        for (int i = 0; i < n; i++)
        {
            var c = nums[i];
            if (c <= 0)
            {
                nums[i] = n + 1;
            }
        }
        //将<=n的正整数标记为相应坐标的负值
        for (int i = 0; i < n; i++)
        {
            int c = Math.Abs(nums[i]);
            if (c <= n)
            {
                nums[c - 1] = -Math.Abs(nums[c - 1]);
            }
        }
        for (int i = 0; i < n; i++)
        {
            var c = nums[i];
            if (c > 0) return i + 1;
        }
        return n + 1;
    }

    //此解可通过,但是空间复杂度时(O(n)),不满足要求
    //[InlineData(new int[] { 0 }, 1)]
    //[InlineData(new int[] { 1, 2, 0 }, 3)]
    //[InlineData(new int[] { 3, 4, -1, 1 }, 2)]
    //[InlineData(new int[] { 7, 8, 9, 11, 12 }, 1)]
    //[InlineData(new int[] { 1, 1000 }, 2)]
    public int FirstMissingPositive2(int[] nums)
    {
        positiveDic.Clear();
        for (int i = 0; i < nums.Length; i++)
        {
            var c = nums[i];
            if (c > 0) positiveDic.TryAdd(c, true);
        }
        if (positiveDic.Count == 0) return 1;
        if (positiveDic.Count == positiveDic.Keys.Max()) return positiveDic.Keys.Max() + 1;
        for (int i = 1; i < positiveDic.Count + 1; i++)
        {
            positiveDic.TryGetValue(i, out bool c);
            if (c == false) return i;
        }
        return 1;
    }
}
