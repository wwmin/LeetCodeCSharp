namespace Solutions;
/// <summary>
/// 1104. 边界着色
/// </summary>
public class ColorBorderSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,1],[1,2]]").ToArray(), 0, 0, 3, StringTo<List<int[]>>("[[3,3],[3,2]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,2,2],[2,3,2]]").ToArray(), 0, 1, 3, StringTo<List<int[]>>("[[1,3,3],[2,3,3]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,1,1],[1,1,1],[1,1,1]]").ToArray(), 1, 1, 2, StringTo<List<int[]>>("[[2,2,2],[2,1,2],[2,2,2]]").ToArray() };
        }
    }

    [Data]
    public int[][] ColorBorder(int[][] grid, int row, int col, int color)
    {
        int m = grid.Length;
        int n = grid[0].Length;
        bool[,] visited = new bool[m, n];
        IList<int[]> borders = new List<int[]>();
        int originalColor = grid[row][col];
        visited[row, col] = true;
        DFS(grid, row, col, visited, borders, originalColor);
        for (int i = 0; i < borders.Count; i++)
        {
            int x = borders[i][0];
            int y = borders[i][1];
            grid[x][y] = color;
        }
        return grid;
    }

    private void DFS(int[][] grid, int x, int y, bool[,] visited, IList<int[]> borders, int originalColor)
    {
        int m = grid.Length;
        int n = grid[0].Length;
        bool isBorder = false;
        int[][] direc = { new int[] { 0, 1 }, new int[] { 0, -1 }, new int[] { 1, 0 }, new int[] { -1, 0 } };
        for (int i = 0; i < 4; i++)
        {
            int nx = direc[i][0] + x;
            int ny = direc[i][1] + y;
            if (!(nx >= 0 && nx < m && ny >= 0 && ny < n && grid[nx][ny] == originalColor))
            {
                isBorder = true;
            }
            else if (!visited[nx, ny])
            {
                visited[nx, ny] = true;
                DFS(grid, nx, ny, visited, borders, originalColor);
            }
        }
        if (isBorder)
        {
            borders.Add(new int[] { x, y });
        }
    }
}