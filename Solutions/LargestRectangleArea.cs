namespace Solutions;
/// <summary>
/// 84. 柱状图中最大的矩形
/// </summary>
public class LargestRectangleAreaSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,1,5,6,2,3]").ToArray(), 10 };
            yield return new object[] { StringTo<int[]>("[2,4]").ToArray(), 4 };
        }
    }

    [Data]
    public int LargestRectangleArea(int[] heights)
    {
        int ans = Solution2(heights);
        return ans;
    }
    //暴力解法
    private int Solution1(int[] heights)
    {
        int ans = 0;
        int n = heights.Length;
        for (int i = 0; i < n; i++)
        {
            int height = heights[i];
            if (height == 0) continue;
            int length = 1;
            int left = i - 1;
            int right = i + 1;
            while (left >= 0)
            {
                if (heights[left] >= height) { length++; left--; }
                else break;
            }
            while (right < n)
            {
                if (heights[right] >= height) { length++; right++; }
                else break;
            }
            int area = height * length;
            if (ans < area) ans = area;
        }
        return ans;
    }
    private int Solution2(int[] heights)
    {
        int n = heights.Length;
        int[] left = new int[n];
        int[] right = new int[n];

        Stack<int> mono_stack = new Stack<int>();
        for (int i = 0; i < n; ++i)
        {
            while (mono_stack.Any() && heights[mono_stack.Peek()] >= heights[i])
            {
                mono_stack.Pop();
            }
            left[i] = (!mono_stack.Any() ? -1 : mono_stack.Peek());
            mono_stack.Push(i);
        }

        mono_stack.Clear();
        for (int i = n - 1; i >= 0; --i)
        {
            while (mono_stack.Any() && heights[mono_stack.Peek()] >= heights[i])
            {
                mono_stack.Pop();
            }
            right[i] = (!mono_stack.Any() ? n : mono_stack.Peek());
            mono_stack.Push(i);
        }

        int ans = 0;
        for (int i = 0; i < n; ++i)
        {
            ans = Math.Max(ans, (right[i] - left[i] - 1) * heights[i]);
        }
        return ans;
    }
}