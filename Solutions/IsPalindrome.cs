﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 9. 回文数
/// </summary>
public class IsPalindromeSolution
{
    public void Test()
    {
        var s0 = 121;
        Console.WriteLine(IsPalindrome(s0));

    }
    [InlineData(1)]
    public bool IsPalindrome(int x)
    {
        if (x < 0) return false;
        var s = x.ToString();
        string sr = string.Join("", s.Reverse());
        return s == sr;
    }
}