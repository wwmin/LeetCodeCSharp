﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 6001. 重排数字的最小值
    /// </summary>
    public class SmallestNumberSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { 310, 103 };
                yield return new object[] { -7605, -7650 };
                yield return new object[] { 95005, 50059 };
                yield return new object[] { 63, 36 };
            }
        }
        [Data]
        public long SmallestNumber(long num)
        {
            if (num == 0) return num;
            if (num > 0)
            {
                int[] arr = num.ToString().ToCharArray().Select(p => Convert.ToInt32(p.ToString())).ToArray();
                Array.Sort(arr);
                if (arr[0] == 0)
                {
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i] != 0)
                        {
                            arr[0] = arr[i];
                            arr[i] = 0;
                            break;
                        }
                    }
                }
                return long.Parse(string.Join("", arr));
            }
            else
            {
                int[] arr = (num * -1).ToString().ToCharArray().Select(p => Convert.ToInt32(p.ToString())).ToArray();
                Array.Sort(arr, (a, b) => b - a);
                return long.Parse(string.Join("", arr)) * -1;
            }
        }
    }
}