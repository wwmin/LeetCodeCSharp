namespace Solutions;
/// <summary>
/// 1663. 具有给定数值的最小字符串
/// difficulty: Medium
/// https://leetcode.cn/problems/smallest-string-with-a-given-numeric-value/
/// </summary>
public class GetSmallestString_1663_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, 27, "aay" };
            yield return new object[] { 5, 73, "aaszz" };
        }
    }

    [Data]
    public string GetSmallestString(int n, int k)
    {
        StringBuilder sb = new StringBuilder();
        //确定最多多少个a
        while ((--n) * 26 >= k--)
        {
            sb.Append('a');
        }
        //确定最多多少个z
        int m = k / 26;
        //剩余差值
        int t = k - m * 26;
        sb.Append((char)('a' + t));
        for (int i = 0; i < m; i++)
        {
            sb.Append('z');
        }
        return sb.ToString();
    }
}