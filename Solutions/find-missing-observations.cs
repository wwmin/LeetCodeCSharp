namespace Solutions;
/// <summary>
/// 2028. 找出缺失的观测数据
/// difficulty: Medium
/// https://leetcode.cn/problems/find-missing-observations/
/// </summary>
public class MissingRolls_2028_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[3,2,4,3]").ToArray(), 4, 2, StringTo<int[]>("[6,6]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,5,6]").ToArray(), 3, 4, StringTo<int[]>("[2,3,2,2]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), 6, 4, StringTo<int[]>("[]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1]").ToArray(), 3, 1, StringTo<int[]>("[5]").ToArray() };
        }
    }

    [Data]
    public int[] MissingRolls(int[] rolls, int mean, int n)
    {
        int m = rolls.Length;
        int sum = (m + n) * mean;
        int sum_m = rolls.Sum();
        int missingSum = sum - sum_m;
        if (missingSum < n || missingSum > 6 * n) return Array.Empty<int>();
        //难点在于如何根据总和求1-6整数的每个数
        int quotient = missingSum / n;
        int remainder = missingSum % n;
        int[] missing = new int[n];
        for (int i = 0; i < n; i++)
        {
            missing[i] = quotient + (i < remainder ? 1 : 0);
        }
        return missing;
    }
}