namespace Solutions;
/// <summary>
/// 1444. 将数字变成 0 的操作次数
/// </summary>
public class NumberOfStepsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 14, 6 };
            yield return new object[] { 8, 4 };
            yield return new object[] { 123, 12 };
        }
    }

    [Data]
    public int NumberOfSteps(int num)
    {
        int ans = 0;
        while (num > 0)
        {
            if (num % 2 == 0) num >>= 1;
            else num--;
            ans++;
        }
        return ans;
    }
}