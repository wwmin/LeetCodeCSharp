namespace Solutions;
/// <summary>
/// 372. 超级次方
/// </summary>
public class SuperPowSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 2, StringTo<int[]>("[3]").ToArray(), 8 };
            yield return new object[] { 2, StringTo<int[]>("[1,0]").ToArray(), 1024 };
            yield return new object[] { 1, StringTo<int[]>("[4,3,3,8,5,2]").ToArray(), 1 };
            yield return new object[] { 2147483647, StringTo<int[]>("[2,0,0]").ToArray(), 1198 };
        }
    }

    const int MOD = 1337;
    [Data]
    public int SuperPow(int a, int[] b)
    {
        int ans = 1;
        for (int i = b.Length - 1; i >= 0; --i)
        {
            ans = (int)((long)ans * Pow(a, b[i]) % MOD);
            a = Pow(a, 10);
        }
        return ans;
    }

    public int Pow(int x, int n)
    {
        int res = 1;
        while (n != 0)
        {
            if (n % 2 != 0)
            {
                res = (int)((long)res * x % MOD);
            }
            x = (int)((long)x * x % MOD);
            n /= 2;
        }
        return res;
    }
}