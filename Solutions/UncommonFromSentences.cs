namespace Solutions;
/// <summary>
/// 920. 两句话中的不常见单词
/// </summary>
public class UncommonFromSentencesSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "this apple is sweet", "this apple is sour", StringTo<string[]>("[\"sweet\",\"sour\"]").ToArray() };
            yield return new object[] { "apple apple", "banana", StringTo<string[]>("[\"banana\"]").ToArray() };
        }
    }

    [Data]
    public string[] UncommonFromSentences(string s1, string s2)
    {
        string[] sa1 = s1.Split(" ");
        string[] sa2 = s2.Split(" ");
        Dictionary<string, int> map1 = new Dictionary<string, int>();
        Dictionary<string, int> map2 = new Dictionary<string, int>();
        for (int i = 0; i < sa1.Length; i++)
        {
            if (map1.ContainsKey(sa1[i]))
            {
                map1[sa1[i]]++;
            }
            else
            {
                map1.Add(sa1[i], 1);
            }
        }
        for (int i = 0; i < sa2.Length; i++)
        {
            if (map2.ContainsKey(sa2[i]))
            {
                map2[sa2[i]]++;
            }
            else
            {
                map2.Add(sa2[i], 1);
            }
        }
        var keys1 = map1.Keys.ToArray();
        var keys2 = map2.Keys.ToArray();
        IList<string> ans = new List<string>();
        foreach (var key in keys1)
        {
            if (map1[key] == 1 && !map2.ContainsKey(key))
            {
                ans.Add(key);
            }
        }
        foreach (var key in keys2)
        {
            if (map2[key] == 1 && !map1.ContainsKey(key))
            {
                ans.Add(key);
            }
        }
        return ans.ToArray();
    }
}