﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 47. 全排列 II
/// </summary>
public class PermuteUniqueSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { StringTo<int[]>("[1,2,3]"), StringTo<List<int[]>>("[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]") };
            //yield return new object[] { StringTo<int[]>("[1,1,2]"), StringTo<List<int[]>>("[[1,1,2],[1,2,1],[2,1,1]]") };
            yield return new object[] { StringTo<int[]>("[3,3,0,3]"), StringTo<List<int[]>>("[[0,3,3,3],[3,0,3,3],[3,3,0,3],[3,3,3,0]]") };
        }
    }
    [Data]
    public IList<IList<int>> PermuteUnique(int[] nums)
    {
        List<IList<int>> res = new List<IList<int>>();
        Array.Sort(nums);
        Stack<int> path = new Stack<int>();
        int len = nums.Length;
        bool[] used = new bool[len];
        DFS(nums, len, 0, path, used, res);
        return res;
    }

    private void DFS(int[] nums, int len, int depth, Stack<int> path, bool[] used, List<IList<int>> res)
    {
        if (depth == len)
        {
            res.Add(new List<int>(path.ToList()));
            return;
        }
        for (int i = 0; i < len; i++)
        {
            if (used[i] || (i > 0 && nums[i] == nums[i - 1] && !used[i - 1])) continue;
            used[i] = true;
            path.Push(nums[i]);
            DFS(nums, len, depth + 1, path, used, res);
            used[i] = false;
            path.Pop();
        }
    }
}
