﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5991. 按符号重排数组
    /// </summary>
    public class RearrangeArraySolution
    {
        public int[] RearrangeArray(int[] nums)
        {
            int n = nums.Length;
            int[] ans = new int[n];
            int mid = n / 2;
            List<int> positive = new List<int>(mid);
            List<int> nagetive = new List<int>(mid);
            for (int i = 0; i < n; i++)
            {
                if (nums[i] >= 0) positive.Add(nums[i]);
                else nagetive.Add(nums[i]);
            }
            for (int i = 0; i < n; i++)
            {
                if (i % 2 == 0)
                {
                    ans[i] = positive[i / 2];
                }
                else
                {
                    ans[i] = nagetive[i / 2];
                }
            }
            return ans;
        }
    }
}