namespace Solutions;
/// <summary>
/// 507. 完美数
/// </summary>
public class CheckPerfectNumberSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 28, true };
            yield return new object[] { 6, true };
            yield return new object[] { 496, true };
            yield return new object[] { 8128, true };
            yield return new object[] { 2, false };
        }
    }

    [Data]
    public bool CheckPerfectNumber(int num)
    {
        if (num == 1) return false;
        int sum = 1;
        for (int d = 2; d * d <= num; d++)
        {
            if (num % d == 0)
            {
                sum += d;
                if (d * d < num)
                {
                    sum += num / d;
                }
            }
        }
        return sum == num;
    }
}