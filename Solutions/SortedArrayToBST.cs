/// <summary>
/// 108. 将有序数组转换为二叉搜索树
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class SortedArrayToBSTSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return null;
            //此处需要手动修改数组转TreeNode的实现
            //yield return new object[] { StringTo<int[]>("[-10,-3,0,5,9]").ToArray(), instance.SortedArrayToBST(StringTo<int[]>("[0,-3,9,-10,null,5]")) };
            //yield return new object[] { StringTo<int[]>("[1,3]").ToArray(), ToTreeNodeWithString("[3,1]") };
        }
    }
    [Data]
    public TreeNode SortedArrayToBST(int[] nums)
    {

        return Helper(nums, 0, nums.Length - 1);
    }

    private TreeNode Helper(int[] nums, int left, int right)
    {
        if (left > right) return null;

        //int mid = (left+right)/2;
        //使用left + (right - left) / 2 计算中间值,免去left+right值溢出的风险
        //总是选择中间位置左边的数字作为根节点
        int mid = left + (right - left) / 2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = Helper(nums, left, mid - 1);
        root.right = Helper(nums, mid + 1, right);
        return root;
    }
}