﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 46. 全排列
/// </summary>
public class PermuteSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3]"), StringTo<List<int[]>>("[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]") };
        }
    }
    [Data]
    public IList<IList<int>> Permute(int[] nums)
    {
        int len = nums.Length;
        List<IList<int>> res = new List<IList<int>>();
        Stack<int> curr = new Stack<int>();
        bool[] used = new bool[len];
        DFS(nums, len, 0, curr, used, res);
        return res;
    }

    //深度优先算法
    private void DFS(int[] nums, int len, int depth, Stack<int> path, bool[] used, List<IList<int>> res)
    {
        if (depth == len)
        {
            res.Add(new Stack<int>(path).ToList());
            return;
        }
        for (int i = 0; i < nums.Length; i++)
        {
            if (used[i]) continue;
            path.Push(nums[i]);
            used[i] = true;
            DFS(nums, len, depth + 1, path, used, res);
            path.Pop();
            used[i] = false;
        }
    }
}
