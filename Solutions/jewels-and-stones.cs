namespace Solutions;
/// <summary>
/// 782. 宝石与石头
/// https://leetcode.cn/problems/jewels-and-stones/
/// </summary>
public class NumJewelsInStonesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "aA", "aAAbbbb", 3 };
            yield return new object[] { "z", "ZZ", 0 };
        }
    }

    [Data]
    public int NumJewelsInStones(string jewels, string stones)
    {
        int ans = 0;
        Dictionary<char, int> dic = new Dictionary<char, int>();
        for (int i = 0; i < jewels.Length; i++)
        {
            dic[jewels[i]] = 0;
        }
        for (int i = 0; i < stones.Length; i++)
        {
            if (dic.ContainsKey(stones[i]))
            {
                //dic[stones[i]]++;
                ans++;
            }
        }
        return ans;
    }
}