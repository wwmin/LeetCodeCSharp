namespace Solutions;
/// <summary>
/// 913. 随机翻转矩阵
/// </summary>
public class Solution_913Solution
{
    private readonly int m;
    private readonly int n;
    private int total;
    Dictionary<int, int> dic = new Dictionary<int, int>();
    Random random = new Random();
    public Solution_913Solution(int m, int n)
    {
        this.m = m;
        this.n = n;
        this.total = m * n;
    }

    public int[] Flip()
    {
        int x = random.Next(total);
        total--;
        int idx = dic.ContainsKey(x) ? dic[x] : x;
        int value = dic.ContainsKey(total) ? dic[total] : total;
        if (dic.ContainsKey(x))
        {
            dic[x] = value;
        }
        else
        {
            dic.Add(x, value);
        }
        return new int[] { idx / n, idx % n };
    }

    public void Reset()
    {
        total = m * n;
        dic.Clear();
    }
}

/**
 * Your Solution_913Solution
 object will be instantiated and called as such:
 * Solution_913Solution
 obj = new Solution_913Solution
(m, n);
 * int[] param_1 = obj.Flip();
 * obj.Reset();
 */