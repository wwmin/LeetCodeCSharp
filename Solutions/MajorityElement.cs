namespace Solutions;
/// <summary>
/// 169. 多数元素
/// </summary>
public class MajorityElementSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[3,2,3]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[2,2,1,1,1,2,2]").ToArray(), 2 };
        }
    }

    [Data]
    public int MajorityElement(int[] nums)
    {
        int n = nums.Length;
        if (n == 1) return nums[0];
        int n2 = n / 2;
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < n; i++)
        {
            if (map.ContainsKey(nums[i]))
            {
                map[nums[i]]++;
                if (map[nums[i]] > n2)
                {
                    return nums[i];
                }
            }
            else
            {
                map.Add(nums[i], 1);
            }
        }
        return default;
    }
}