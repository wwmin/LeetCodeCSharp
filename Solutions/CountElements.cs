﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5989. 元素计数
    /// </summary>
    public class CountElementsSolution
    {
        public int CountElements(int[] nums)
        {
            int min = int.MaxValue;
            int max = int.MinValue;
            Dictionary<int, int> map = new Dictionary<int, int>();
            for (int i = 0; i < nums.Length; i++)
            {
                if (map.ContainsKey(nums[i]))
                {
                    map[nums[i]]++;
                }
                else
                {
                    map.Add(nums[i], 1);
                    min = Math.Min(nums[i], min);
                    max = Math.Max(nums[i], max);
                }
            }
            int minNum = map[min];
            int maxNum = map[max];
            int keys = map.Keys.Count;
            if (keys <= 2) return 0;
            return nums.Length - map[min] - map[max];
        }
    }
}