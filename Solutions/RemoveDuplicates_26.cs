namespace Solutions;
/// <summary>
/// 26. 删除有序数组中的重复项
/// </summary>
public class RemoveDuplicates_26Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[]").ToArray(), 0 };
            yield return new object[] { StringTo<int[]>("[1,1,2]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[0,0,1,1,1,2,2,3,3,4]").ToArray(), 5 };
        }
    }

    [Data]
    public int RemoveDuplicates_26(int[] nums)
    {
        int n = nums.Length;
        if (n == 0) return 0;
        int index = 1;
        for (int i = 1; i < n; i++)
        {
            if (nums[i] != nums[index - 1])
            {
                nums[index++] = nums[i];
            }
        }
        return index;
    }
}