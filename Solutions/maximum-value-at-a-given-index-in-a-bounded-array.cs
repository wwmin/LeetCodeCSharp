namespace Solutions;
/// <summary>
/// 1802. 有界数组中指定下标处的最大值
/// difficulty: Medium
/// https://leetcode.cn/problems/maximum-value-at-a-given-index-in-a-bounded-array/
/// </summary>
public class MaxValue_1802_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, 2, 6, 2 };
            yield return new object[] { 6, 1, 10, 3 };
        }
    }

    [Data]
    public int MaxValue(int n, int index, int maxSum)
    {
        int l = index;
        int r = index;
        int ans = 1;
        //整个数组一开始全部填充为1
        //rest记录全部填充1后，剩下1的个数
        int rest = maxSum - n;
        while (l > 0 || r < n - 1)
        {
            var len = r - l + 1;
            if (rest >= len)
            {
                //当前[l,r]范围全部+1
                rest -= len;
                ans++;
                //往左右量变扩
                l = Math.Max(0, l - 1);
                r = Math.Min(n - 1, r + 1);
            }
            else
            {
                break;
            }
        }
        //扩大到整个数组之后，剩余的值“雨露均沾”一下
        ans += (int)Math.Floor((double)rest / n);
        return ans;
    }
}