namespace Solutions;
/// <summary>
/// 2132. 将一维数组转变成二维数组
/// </summary>
public class Construct2DArraySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), 2, 2, StringTo<List<int[]>>("[[1,2],[3,4]]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), 1, 3, StringTo<List<int[]>>("[[1,2,3]]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,2]").ToArray(), 1, 1, StringTo<List<int[]>>("[]").ToArray() };
            yield return new object[] { StringTo<int[]>("[3]").ToArray(), 1, 2, StringTo<List<int[]>>("[]").ToArray() };
        }
    }

    [Data]
    public int[][] Construct2DArray(int[] original, int m, int n)
    {
        if (original.Length != m * n) return new int[0][];
        int[][] ans = new int[m][];
        for (int i = 0, k = 0; i < m; i++)
        {
            ans[i] = new int[n];
            for (int j = 0; j < n; j++)
            {
                //ans[i][j] = original[(i * n) + j];
                //使用自增id优化 计算得到第几个数
                ans[i][j] = original[k++];
            }
        }
        return ans;
    }
}