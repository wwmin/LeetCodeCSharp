namespace Solutions;
/// <summary>
/// 203. 移除链表元素
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class RemoveElementsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1,2,6,3,4,5,6]"), 6, ToListNodeWithString("[1,2,3,4,5]") };
            yield return new object[] { ToListNodeWithString("[]"), 1, ToListNodeWithString("[]") };
            yield return new object[] { ToListNodeWithString("[7,7,7,7]"), 7, ToListNodeWithString("[]") };
        }
    }
    [Data]
    public ListNode RemoveElements(ListNode head, int val)
    {
        ListNode ans = new ListNode(0, head);
        ListNode cur = ans;
        while (cur.next != null)
        {
            if (cur.next.val == val)
            {
                cur.next = cur.next.next;
            }
            else
            {
                cur = cur.next;
            }
        }
        return ans.next;
    }
}