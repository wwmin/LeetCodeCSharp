namespace Solutions;
/// <summary>
/// 453. 最小操作次数使数组元素相等
/// </summary>
public class MinMovesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[1,1,1]").ToArray(), 0 };
        }
    }

    [Data]
    public int MinMoves(int[] nums)
    {
        int ans = 0;
        int min = nums.Min();
        foreach (var num in nums)
        {
            ans += num - min;
        }
        return ans;
    }
}