namespace Solutions;
/// <summary>
/// 167. 两数之和 II - 输入有序数组
/// </summary>
public class TwoSumSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,7,11,15]").ToArray(), 9, StringTo<int[]>("[1,2]").ToArray() };
            yield return new object[] { StringTo<int[]>("[2,3,4]").ToArray(), 6, StringTo<int[]>("[1,3]").ToArray() };
            yield return new object[] { StringTo<int[]>("[-1,0]").ToArray(), -1, StringTo<int[]>("[1,2]").ToArray() };
        }
    }

    [Data]
    public int[] TwoSum(int[] numbers, int target)
    {
        int[] res = new int[2];
        //dic 存差值及坐标
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < numbers.Length; i++)
        {
            if (map.ContainsKey(numbers[i]))
            {
                res[0] = map[numbers[i]] + 1;
                res[1] = i + 1;
                return res;
            }
            else
            {
                //差值
                int diff = target - numbers[i];
                if (!map.ContainsKey(diff)) map.Add(target - numbers[i], i);
            }
        }
        return res;
    }
}