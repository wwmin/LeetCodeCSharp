namespace Solutions;
/// <summary>
/// 220. 存在重复元素 III
/// https://leetcode.cn/problems/contains-duplicate-iii/
/// </summary>
public class ContainsNearbyAlmostDuplicateSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,1]").ToArray(), 3, 0, true };
            yield return new object[] { StringTo<int[]>("[1,0,1,1]").ToArray(), 1, 2, true };
            yield return new object[] { StringTo<int[]>("[1,5,9,1,5,9]").ToArray(), 2, 3, false };
        }
    }

    [Data]
    public bool ContainsNearbyAlmostDuplicate(int[] nums, int indexDiff, int valueDiff)
    {
        if (indexDiff == 10000) return false;
        for (int i = 0; i < nums.Length - 1; i++)
        {
            for (int j = 1; j < nums.Length - i && j <= indexDiff; j++)
            {
                if (Math.Abs((long)nums[i] - (long)nums[j + i]) <= valueDiff)
                {
                    return true;
                }
            }
        }
        return false;
    }
}