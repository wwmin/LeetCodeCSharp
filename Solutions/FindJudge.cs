namespace Solutions;
/// <summary>
/// 1039. 找到小镇的法官
/// </summary>
public class FindJudgeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 2, StringTo<List<int[]>>("[[1,2]]").ToArray(), 2 };
            yield return new object[] { 3, StringTo<List<int[]>>("[[1,3],[2,3]]").ToArray(), 3 };
            yield return new object[] { 3, StringTo<List<int[]>>("[[1,3],[2,3],[3,1]]").ToArray(), -1 };
            yield return new object[] { 3, StringTo<List<int[]>>("[[1,2],[2,3]]").ToArray(), -1 };
            yield return new object[] { 4, StringTo<List<int[]>>("[[1,3],[1,4],[2,3],[2,4],[4,3]]").ToArray(), 3 };
        }
    }
    /*
     预备知识
     本题需要用到有向图中节点的入度和出度的概念。在有向图中，一个节点的入度是指向该节点的边的数量；而一个节点的出度是从该节点出发的边的数量。
     */
    [Data]
    public int FindJudge(int n, int[][] trust)
    {
        IList<int>[] g = new List<int>[n];
        for (int i = 0; i < n; i++)
        {
            //初始化拓扑图
            g[i] = new List<int>();
        }
        int[] inDegree = new int[n];
        foreach (int[] r in trust)
        {
            g[r[0] - 1].Add(r[1] - 1);
            inDegree[r[1] - 1]++;
        }
        for (int i = 0; i < n; i++)
        {
            //如果入度值 == n-1 且他不指向任何人，则说明是法官
            if (inDegree[i] == n - 1 && g[i].Count == 0)
            {
                return i + 1;
            }
        }
        return -1;
    }
}