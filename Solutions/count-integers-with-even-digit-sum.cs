namespace Solutions;
/// <summary>
/// 2180. 统计各位数字之和为偶数的整数个数
/// difficulty: Easy
/// https://leetcode.cn/problems/count-integers-with-even-digit-sum/
/// </summary>
public class CountEven_2180_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, 2 };
            yield return new object[] { 30, 14 };
        }
    }

    [Data]
    public int CountEven(int num)
    {
        int count = 0;
        for (int i = 1; i <= num; i++)
        {
            if (IsValidNum(i)) count++;
        }
        return count;
    }

    private static bool IsValidNum(int num)
    {
        int sum = 0;
        while (num > 0)
        {
            //num%10取出最后一位
            sum += num % 10;
            //num/10去掉最后一位
            num /= 10;
        }
        return sum % 2 == 0;
    }
}