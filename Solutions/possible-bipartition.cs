namespace Solutions;
/// <summary>
/// 922. 可能的二分法
/// https://leetcode.cn/problems/possible-bipartition/
/// difficulty: Media
/// </summary>
public class PossibleBipartitionSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, StringTo<List<int[]>>("[[1,2],[1,3],[2,4]]").ToArray(), true };
            yield return new object[] { 3, StringTo<List<int[]>>("[[1,2],[1,3],[2,3]]").ToArray(), false };
            yield return new object[] { 5, StringTo<List<int[]>>("[[1,2],[2,3],[3,4],[4,5],[1,5]]").ToArray(), false };
        }
    }

    [Data]
    public bool PossibleBipartition(int n, int[][] dislikes)
    {
        int[] color = new int[n + 1];
        IList<int>[] g = new IList<int>[n + 1];
        for (int i = 0; i <= n; ++i)
        {
            g[i] = new List<int>();
        }
        foreach (int[] p in dislikes)
        {
            g[p[0]].Add(p[1]);
            g[p[1]].Add(p[0]);
        }
        for (int i = 1; i <= n; ++i)
        {
            if (color[i] == 0 && !DFS(i, 1, color, g))
            {
                return false;
            }
        }
        return true;
    }

    public bool DFS(int curnode, int nowcolor, int[] color, IList<int>[] g)
    {
        color[curnode] = nowcolor;
        foreach (int nextnode in g[curnode])
        {
            if (color[nextnode] != 0 && color[nextnode] == color[curnode])
            {
                return false;
            }
            if (color[nextnode] == 0 && !DFS(nextnode, 3 ^ nowcolor, color, g))
            {
                return false;
            }
        }
        return true;
    }
}