namespace Solutions;
/// <summary>
/// 263. 丑数
/// </summary>
public class IsUglySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 6, true };
            yield return new object[] { 8, true };
            yield return new object[] { 14, false };
            yield return new object[] { 1, true };
        }
    }

    [Data]
    public bool IsUgly(int n)
    {
        if (n <= 0) return false;
        while (n % 2 == 0) n = n / 2;
        while (n % 3 == 0) n = n / 3;
        while (n % 5 == 0) n = n / 5;
        return n == 1;
    }
}