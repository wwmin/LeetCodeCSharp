namespace Solutions;
/// <summary>
/// 942. 增减字符串匹配
/// difficulty: Easy
/// https://leetcode.cn/problems/di-string-match/
/// </summary>
public class DiStringMatch_942_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{"IDID", StringTo<int[]>("[0,4,1,3,2]").ToArray()};
            yield return new object[]{"III", StringTo<int[]>("[0,1,2,3]").ToArray()};
            yield return new object[]{"DDI", StringTo<int[]>("[3,2,0,1]").ToArray()};
        }
    }

    [Data]
    public int[] DiStringMatch(string s)
    {
        var result = new int[s.Length + 1];
        var min = 0;
        var max = s.Length;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == 'I')
            {
                result[i] = min++;
            }
            else
            {
                result[i] = max--;
            }
        }
        result[^1] = min;
        return result;
    }
}