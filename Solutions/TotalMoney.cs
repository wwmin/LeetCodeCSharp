namespace Solutions;
/// <summary>
/// 1817. 计算力扣银行的钱
/// </summary>
public class TotalMoneySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, 10 };
            yield return new object[] { 10, 37 };
            yield return new object[] { 20, 96 };
        }
    }

    [Data]
    public int TotalMoney(int n)
    {
        int w = n / 7;
        int left = n % 7;
        int oneWeekMoney = (1 + 7) * 7 / 2;
        int allWeekBaseMoney = (w - 1) * w / 2 * 7;
        int all = allWeekBaseMoney + oneWeekMoney * w + (w + 1 + w + left) * left / 2;
        return all;
    }
}