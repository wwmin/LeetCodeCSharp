namespace Solutions;
/// <summary>
/// 349. 两个数组的交集
/// </summary>
public class IntersectionSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,2,1]").ToArray(), StringTo<int[]>("[2,2]").ToArray(), StringTo<int[]>("[2]").ToArray() };
            yield return new object[] { StringTo<int[]>("[4,9,5]").ToArray(), StringTo<int[]>("[9,4,9,8,4]").ToArray(), StringTo<int[]>("[9,4]").ToArray() };
        }
    }

    [Data]
    public int[] Intersection(int[] nums1, int[] nums2)
    {
        IList<int> ans = new List<int>();
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < nums1.Length; i++)
        {
            if (map.ContainsKey(nums1[i])) continue;
            map.Add(nums1[i], 1);
        }
        for (int i = 0; i < nums2.Length; i++)
        {
            if (map.ContainsKey(nums2[i]) && map[nums2[i]] == 1) map[nums2[i]]++;
        }

        var keys = map.Keys.ToArray();
        foreach (var key in keys)
        {
            if (map[key] == 2) ans.Add(key);
        }
        return ans.ToArray();
    }
}