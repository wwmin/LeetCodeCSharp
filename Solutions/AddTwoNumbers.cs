﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 2. 两数相加
/// </summary>
public class AddTwoNumbersSolution
{
    public void Test()
    {
        var l1 = new ListNode(2, new ListNode(4, new ListNode(3)));
        var l2 = new ListNode(5, new ListNode(6, new ListNode(4)));
        var sum = AddTwoNumbers(l1, l2);
        Console.WriteLine(sum);


    }
    public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
    {
        ListNode sum = new ListNode(0, null);
        SumNode(l1, l2, sum);
        return sum;
    }

    private void SumNode(ListNode l1, ListNode l2, ListNode sum)
    {
        int s = sum.val;
        if (l1 != null) s += l1.val;
        if (l2 != null) s += l2.val;
        int nextVal = 0;
        if (s >= 10)
        {
            nextVal = 1;
            sum.val = s - 10;
        }
        else sum.val = s;

        if (nextVal > 0 || (l1 != null && l1.next != null) || (l2 != null && l2.next != null))
        {
            sum.next = new ListNode(nextVal, null);
            SumNode(l1?.next, l2?.next, sum.next);
        }
    }

    private void GetVal(ListNode ln, string s)
    {
        if (ln != null) s = ln.val + "";
        if (ln != null && ln.next != null) GetVal(ln.next, s);
    }
}

