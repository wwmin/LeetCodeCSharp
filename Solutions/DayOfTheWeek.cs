namespace Solutions;
/// <summary>
/// 1289. 一周中的第几天
/// </summary>
public class DayOfTheWeekSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 31, 8, 2019, "Saturday" };
            yield return new object[] { 18, 7, 1999, "Sunday" };
            yield return new object[] { 15, 8, 1993, "Sunday" };
        }
    }

    [Data]
    public string DayOfTheWeek(int day, int month, int year)
    {
        var date = new DateTime(year, month, day);
        return date.DayOfWeek.ToString();
    }
}