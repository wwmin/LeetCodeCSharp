namespace Solutions;
/// <summary>
/// 2112. 学生分数的最小差值
/// </summary>
public class MinimumDifferenceSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[90]").ToArray(), 1, 0 };
            yield return new object[] { StringTo<int[]>("[9,4,1,7]").ToArray(), 2, 2 };
        }
    }

    [Data]
    public int MinimumDifference(int[] nums, int k)
    {
        Array.Sort(nums);
        int right = k - 1;
        int min = int.MaxValue;
        for (int left = 0; right < nums.Length; left++, right++)
        {
            min = Math.Min(min, nums[right] - nums[left]);
        }
        return min;
    }
}