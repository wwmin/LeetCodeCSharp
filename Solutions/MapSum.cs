namespace Solutions;
/// <summary>
/// 677. 键值映射
/// </summary>
public class MapSum
{
    Dictionary<string, int> dic;
    public MapSum()
    {
        dic = new Dictionary<string, int>();
    }

    public void Insert(string key, int val)
    {
        if (dic.ContainsKey(key))
        {
            dic[key] = val;
        }
        else
        {
            dic.Add(key, val);
        }
    }

    public int Sum(string prefix)
    {
        int res = 0;
        foreach (string key in dic.Keys)
        {
            if (key.StartsWith(prefix))
            {
                res += dic[key];
            }
        }
        return res;
    }
}

/**
 * Your MapSum object will be instantiated and called as such:
 * MapSum obj = new MapSum();
 * obj.Insert(key,val);
 * int param_2 = obj.Sum(prefix);
 */