namespace Solutions;
/// <summary>
/// 219. 存在重复元素 II
/// </summary>
public class ContainsNearbyDuplicateSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,1]").ToArray(), 3, true };
            yield return new object[] { StringTo<int[]>("[1,0,1,1]").ToArray(), 1, true };
            yield return new object[] { StringTo<int[]>("[1,2,3,1,2,3]").ToArray(), 2, false };
        }
    }

    [Data]
    public bool ContainsNearbyDuplicate(int[] nums, int k)
    {
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length; i++)
        {
            if (map.ContainsKey(nums[i]))
            {
                if (i - map[nums[i]] <= k) return true;
                else
                {
                    map[nums[i]] = i;
                }
            }
            else
            {
                map.Add(nums[i], i);
            }
        }
        return false;
    }
}