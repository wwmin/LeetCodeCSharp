namespace Solutions;
/// <summary>
/// 1047. K 次取反后最大化的数组和
/// </summary>
public class LargestSumAfterKNegationsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[4,2,3]").ToArray(), 1, 5 };
            yield return new object[] { StringTo<int[]>("[3,-1,0,2]").ToArray(), 3, 6 };
            yield return new object[] { StringTo<int[]>("[2,-3,-1,5,-4]").ToArray(), 2, 13 };
            yield return new object[] { StringTo<int[]>("[5,6,9,-3,3]").ToArray(), 2, 20 };
        }
    }
    [Data]
    public int LargestSumAfterKNegations(int[] nums, int k)
    {
        Array.Sort(nums);
        if (nums[0] > 0)
        {
            if (k % 2 == 1) nums[0] = -nums[0];
            return nums.Sum();
        }
        int n = nums.Length;
        if (nums[n - 1] < 0)
        {
            if (k > n)
            {
                if ((k - n) % 2 == 1) nums[n - 1] = -nums[n - 1];
                return -nums.Sum();
            }
            int ans = 0;
            for (int j = 0; j < k; j++)
            {
                nums[j] = -nums[j];
                ans += -nums[j];
            }
            for (int j = k; j < n; j++)
            {
                ans += nums[j];
            }
            return ans;
        }
        //有小于0和大于等于零的情况

        int i = 0;
        while (i < k && i < n)
        {
            if (nums[i] < 0) nums[i] = -nums[i];
            else break;
            i++;
        }
        if (i >= k)
        {
            return nums.Sum();
        }
        //k>i
        int leftK = k - i;
        k = leftK % 2;
        if (k == 1)
        {
            if (nums[i] > nums[i - 1]) nums[i - 1] = -nums[i - 1];
            else nums[i] = -nums[i];
        }
        return nums.Sum();
    }

    private int sum(int[] nums)
    {
        return nums.Sum();
    }
}