namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 488. 祖玛游戏
/// </summary>
public class FindMinStepSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "WRRBBW", "RB", -1 };
            yield return new object[] { "WWRRBBWW", "WRBRW", 2 };
            yield return new object[] { "G", "GGGGG", 2 };
            yield return new object[] { "RBYYBBRRB", "YRBGB", 3 };
        }
    }
    //int INF = 0x3f3f3f3f;
    string b;
    int m;
    Dictionary<string, int> map = new Dictionary<string, int>();
    [Data]
    public int FindMinStep(string board, string hand)
    {
        b = board;
        m = b.Length;
        //1<<m => 2^m次方
        return default;
    }


}