﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 
/// </summary>
public class AllPathsSourceTargetSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2],[3],[3],[]]").ToArray(), StringTo<List<int[]>>("[[0,1,3],[0,2,3]]") };
        }
    }
    [Data]
    public IList<IList<int>> AllPathsSourceTarget(int[][] graph)
    {
        List<IList<int>> res = new List<IList<int>>();
        var curPath = new List<int>() { 0 };
        DFS(0, curPath, graph, res);
        return res;
    }

    //深度搜索优先 DFS
    private void DFS(int start, List<int> path, int[][] graph, List<IList<int>> ans)
    {
        for (int i = 0; i < graph[start].Length; i++)
        {
            var c = graph[start][i];
            path.Add(c);
            if (c == graph.Length - 1)
            {
                //保存答案,需要拷贝
                ans.Add(new List<int>(path));
            }
            //start就是当前找到的新值,用新值继续搜索
            DFS(c, path, graph, ans);
            path.RemoveAt(path.Count - 1);//将新加入的弹出,继续寻找下一个可能的平行深度路线
        }
    }
}