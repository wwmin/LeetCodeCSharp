namespace Solutions;
/// <summary>
/// 793. 在LR字符串中交换相邻字符
/// https://leetcode.cn/problems/swap-adjacent-in-lr-string/
/// </summary>
public class CanTransformSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "RXXLRXRXL", "XRLXXRRLX", true };
        }
    }

    [Data]
    public bool CanTransform(string start, string end)
    {
        //找规律
        int n = start.Length;
        int i = 0, j = 0;
        while (i < n && j < n)
        {
            while (i < n && start[i] == 'X')
            {
                i++;
            }
            while (j < n && end[j] == 'X')
            {
                j++;
            }
            if (i < n && j < n)
            {
                if (start[i] != end[j])
                {
                    return false;
                }
                char c = start[i];
                if ((c == 'L' && i < j) || (c == 'R' && i > j))
                {
                    return false;
                }
                i++;
                j++;
            }
        }
        while (i < n)
        {
            if (start[i] != 'X')
            {
                return false;
            }
            i++;
        }
        while (j < n)
        {
            if (end[j] != 'X')
            {
                return false;
            }
            j++;
        }
        return true;
    }
}