namespace Solutions;
/// <summary>
/// 146. LRU 缓存
/// https://leetcode.cn/problems/lru-cache/
/// </summary>
public class LRUCache
{


    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"LRUCache\",\"put\",\"put\",\"get\",\"put\",\"get\",\"put\",\"get\",\"get\",\"get\"]", "[[2],[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]", "[null,null,null,1,null,-1,null,-1,3,4]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
    }
    public LRUCache(int capacity)
    {

    }

    public int Get(int key)
    {
        return default;
    }

    public void Put(int key, int value)
    {

    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.Get(key);
 * obj.Put(key,value);
 */