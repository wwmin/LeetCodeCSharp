namespace Solutions;
/// <summary>
/// 80. 删除有序数组中的重复项 II
/// </summary>
public class RemoveDuplicatesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,1,1,2,2,3]").ToArray(), 5 };
            yield return new object[] { StringTo<int[]>("[0,0,1,1,1,1,2,3,3]").ToArray(), 7 };
        }
    }

    [Data]
    public int RemoveDuplicates(int[] nums)
    {
        //双指针
        int n = nums.Length;
        if (n <= 2) return n;
        int index = 2;
        for (int i = 2; i < n; i++)
        {
            if (nums[i] != nums[index - 2])
            {
                nums[index++] = nums[i];
            }
        }
        return index;
    }
}