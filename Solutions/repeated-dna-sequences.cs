namespace Solutions;
/// <summary>
/// 187. 重复的DNA序列
/// difficulty: Medium
/// https://leetcode.cn/problems/repeated-dna-sequences/
/// </summary>
public class FindRepeatedDnaSequences_187_Solution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT", StringTo<List<string>>("[\"AAAAACCCCC\",\"CCCCCAAAAA\"]") };
            yield return new object[] { "AAAAAAAAAAAAA", StringTo<List<string>>("[\"AAAAAAAAAA\"]") };
        }
    }

    [Data]
    public IList<string> FindRepeatedDnaSequences(string s)
    {
        int n = s.Length;
        IList<string> ans = new List<string>();
        Dictionary<string, int> dic = new Dictionary<string, int>();
        for (int i = 0; i + 10 <= n; i++)
        {
            var sub = s.Substring(i, 10);
            var has = dic.ContainsKey(sub);
            if (has)
            {
                if (dic[sub] == 0)
                {
                    ans.Add(sub);
                }
                dic[sub]++;
            }
            else
            {
                dic.Add(sub, 0);
            }
        }
        return ans;
    }
}