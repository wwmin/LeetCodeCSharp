namespace Solutions;
/// <summary>
/// 374. 猜数字大小
/// </summary>
/** 
 * Forward declaration of guess API.
 * @param  num   your guess
 * @return 	     -1 if num is lower than the guess number
 *			      1 if num is higher than the guess number
 *               otherwise return 0
 * int guess(int num);
 */

public class GuessNumberSolution
{
    private int guss = 6;
    private int guess(int num)
    {
        if (num == guss) return 0;
        if (num > guss) return 1;
        else return -1;
    }
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 10, 6 };
            //yield return new object[] { 1, 1 };
            //yield return new object[] { 2, 1 };
            //yield return new object[] { 2, 2 };
        }
    }
    [Data]
    public int GuessNumber(int n)
    {
        if (guess(n) == 0) return n;
        int left = 1;
        int right = n;
        while (left < right)
        {
            int mid = left + (right - left) / 2;
            int my_guess_res = guess(mid);
            if (my_guess_res <= 0)
            {
                right = mid;
            }
            else
            {
                left = mid + 1;
            }
        }
        return left;
    }
}