﻿namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5972. 统计隐藏数组数目
    /// </summary>
    public class NumberOfArraysSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { StringTo<int[]>("[1,-3,4]"), 1, 6, 2 };
                yield return new object[] { StringTo<int[]>("[3,-4,5,1,-2]"), -4, 5, 4 };
                yield return new object[] { StringTo<int[]>("[4,-7,2]"), 3, 6, 0 };
            }
        }
        [Data]
        public int NumberOfArrays(int[] differences, int lower, int upper)
        {
            //前缀和 思想
            /*
             对于每种隐藏数组，一旦确定开头，后续数组序列都是确定的，问题转化为lower到upper间的每种情况中，有多少是满足条件的；
            而条件的限制是全在范围内，因此累加diff数组，记录最低会减多少，最高会加多少，如果开头加上这个最大变化幅度在范围内就是可行答案​；
            会爆int，所以要用long long
             */
            long min = int.MaxValue;
            long max = int.MinValue;
            int n = differences.Length;
            long start = 0;
            for (int i = 0; i < n; i++)
            {
                //找出幅度最大和最小的值
                start += differences[i];
                min = Math.Min(min, start);
                max = Math.Max(max, start);
            }
            int ans = 0;
            for (int i = lower; i <= upper; i++)
            {
                if (i + min >= lower && i + max <= upper) ans++;
            }
            return ans;
        }

        //此方法超时
        public int NumberOfArrays1(int[] differences, int lower, int upper)
        {
            int ans = 0;
            int min = int.MaxValue;
            int max = int.MinValue;
            for (int i = 0; i < differences.Length; i++)
            {
                min = Math.Min(min, differences[i]);
                max = Math.Max(max, differences[i]);
            }
            if (min < lower - upper || max > upper - lower) return ans;
            int n = differences.Length + 1;
            for (int i = lower; i <= upper; i++)
            {
                //根据差值构造数组
                int[] arr = new int[n];
                arr[0] = i;
                int j = 0;
                bool isValid = true;
                while (j < differences.Length)
                {
                    arr[j + 1] = arr[j] + differences[j];
                    if (arr[j + 1] < lower || arr[j + 1] > upper)
                    {
                        isValid = false;
                        break;
                    }
                    j++;
                }
                if (isValid) ans++;
            }
            return ans;
        }
    }
}