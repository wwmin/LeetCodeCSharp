namespace Solutions;
/// <summary>
/// 1610. 可见点的最大数目
/// </summary>
public class VisiblePointsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<List<int>>>("[[2,1],[2,2],[3,3]]").ToArray(), 90, StringTo<List<int>>("[1,1]"), 3 };
            yield return new object[] { StringTo<List<List<int>>>("[[2,1],[2,2],[3,4],[1,1]]").ToArray(), 90, StringTo<List<int>>("[1,1]"), 4 };
            yield return new object[] { StringTo<List<List<int>>>("[[1,0],[2,1]]").ToArray(), 13, StringTo<List<int>>("[1,1]"), 1 };
        }
    }

    [Data]
    public int VisiblePoints(IList<IList<int>> points, int angle, IList<int> location)
    {
        //所在位置与点位置相同的个数
        int sameCnt = 0;
        //极坐标列表
        List<double> polarDegrees = new List<double>();
        //所在点x
        int locationX = location[0];
        //所在点y
        int locationY = location[1];
        for (int i = 0; i < points.Count; i++)
        {
            int x = points[i][0];
            int y = points[i][1];
            //如果所在点与坐标点重合，则统计加1
            if (x == locationX && y == locationY)
            {
                sameCnt++;
                continue;
            }
            //除所在点以外的其他极坐标点
            double degree = Math.Atan2(y - locationY, x - locationX);
            polarDegrees.Add(degree);
        }
        //对极坐标点排序
        polarDegrees.Sort();
        int m = polarDegrees.Count;
        //将所有点都加上360°后加到原始表后面，防止在坐标大于360°出现反转的情况
        for (int i = 0; i < m; i++)
        {
            polarDegrees.Add(polarDegrees[i] + 2 * Math.PI);
        }
        //保存最大极坐标个数
        int maxCnt = 0;
        //滑动窗口右侧坐标
        int right = 0;
        //可视角弧度转度°
        double toDegree = angle * Math.PI / 180;
        for (int i = 0; i < m; i++)
        {
            //当前极坐标点与旋转角度形成的夹角度数
            double curr = polarDegrees[i] + toDegree;
            //判断窗口右侧位置，条件：1.小于坐标总数 2.极坐标度数小于等于旋转最大度数值
            while (right < polarDegrees.Count && polarDegrees[right] <= curr)
            {
                right++;
            }
            //判断当前包含最大极点坐标个数
            maxCnt = Math.Max(maxCnt, right - i);
        }
        //最大极点个数与同原点个数相同的点个数和
        return maxCnt + sameCnt;
    }
}