namespace Solutions;
/// <summary>
/// 144. 二叉树的前序遍历
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class PreorderTraversalSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,null,2,3]"), StringTo<List<int>>("[1,2,3]") };
            yield return new object[] { ToTreeNodeWithString("[]"), StringTo<List<int>>("[]") };
            yield return new object[] { ToTreeNodeWithString("[1]"), StringTo<List<int>>("[1]") };
            yield return new object[] { ToTreeNodeWithString("[1,2]"), StringTo<List<int>>("[1,2]") };
            yield return new object[] { ToTreeNodeWithString("[1,null,2]"), StringTo<List<int>>("[1,2]") };
        }
    }
    [Data]
    public IList<int> PreorderTraversal(TreeNode root)
    {
        IList<int> res = new List<int>();
        preorder(root, res);
        return res;
    }

    private void preorder(TreeNode root, IList<int> res)
    {
        if (root == null) return;
        res.Add(root.val);
        preorder(root.left, res);
        preorder(root.right, res);
    }
}