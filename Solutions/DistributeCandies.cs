namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 575. 分糖果
/// </summary>
public class DistributeCandiesSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,1,2,2,3,3]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[1,1,2,3]").ToArray(), 2 };
        }
    }
    [Data]
    public int DistributeCandies(int[] candyType)
    {
        var halfN = candyType.Length / 2;
        var hs = candyType.ToHashSet().ToArray();
        if (hs.Length >= halfN) return halfN;
        return hs.Length;
    }
}
