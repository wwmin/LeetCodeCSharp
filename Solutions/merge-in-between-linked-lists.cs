namespace Solutions;
/// <summary>
/// 1669. 合并两个链表
/// difficulty: Medium
/// https://leetcode.cn/problems/merge-in-between-linked-lists/
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class MergeInBetween_1669_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[0,1,2,3,4,5]"), 3, 4, ToListNodeWithString("[1000000,1000001,1000002]"), ToListNodeWithString("[0,1,2,1000000,1000001,1000002,5]") };
            yield return new object[] { ToListNodeWithString("[0,1,2,3,4,5,6]"), 2, 5, ToListNodeWithString("[1000000,1000001,1000002,1000003,1000004]"), ToListNodeWithString("[0,1,1000000,1000001,1000002,1000003,1000004,6]") };
        }
    }
    [Data]
    public ListNode MergeInBetween(ListNode list1, int a, int b, ListNode list2)
    {
        //指针在0处
        var node = list1;
        //移动指针到a-1处
        for (int i = 0; i < a - 1; i++)
        {
            node = node.next;
        }
        //新指针指向a-1
        var node2 = node;
        //移动新指针到b+1
        for (int i = 0; i < b - a + 2; i++)
        {
            node2 = node2.next;
        }
        //指针指向list2开头处
        node.next = list2;
        //移动指针到list2的末尾
        while (node.next != null)
        {
            node = node.next;
        }
        //指针指向新指针b+1
        node.next = node2;
        return list1;
    }
}