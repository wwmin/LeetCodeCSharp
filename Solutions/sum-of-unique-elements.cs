namespace Solutions;
/// <summary>
/// 1848. 唯一元素的和
/// </summary>
public class SumOfUniqueSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,2]").ToArray(), 4 };
            yield return new object[] { StringTo<int[]>("[1,1,1,1,1]").ToArray(), 0 };
            yield return new object[] { StringTo<int[]>("[1,2,3,4,5]").ToArray(), 15 };
        }
    }

    [Data]
    public int SumOfUnique(int[] nums)
    {
        int ans = 0;
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length; i++)
        {
            if (map.ContainsKey(nums[i]))
            {
                map[nums[i]]++;
            }
            else
            {
                map.Add(nums[i], 1);
            }
        }
        var keys = map.Keys.ToArray();
        foreach (var key in keys)
        {
            if (map[key] == 1) ans += key;
        }
        return ans;
    }
}