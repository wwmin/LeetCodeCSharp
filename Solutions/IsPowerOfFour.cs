namespace Solutions;
/// <summary>
/// 342. 4的幂
/// </summary>
public class IsPowerOfFourSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 16, true };
            yield return new object[] { 5, false };
            yield return new object[] { 1, true };
        }
    }

    [Data]
    public bool IsPowerOfFour(int n)
    {
        while (n % 4 == 0)
        {
            n = n / 4;
        }
        return n == 1;
    }

    //使用二进制特性
    //先判断是否是2的幂,再判断是否是4的倍数
    /*
     * (10000)2
     唯一的 1 出现在第 44 个二进制位上，因此 n 是 44 的幂。
    由于题目保证了 n 是一个 3232 位的有符号整数，因此我们可以构造一个整数 mask，它的所有偶数二进制位都是 0，所有奇数二进制位都是 1。这样一来，我们将 n 和 mask 进行按位与运算，如果结果为 0，说明 n 二进制表示中的 1 出现在偶数的位置，否则说明其出现在奇数的位置。
     */
    public bool IsPowerOfFour2(int n)
    {
        return n > 0 && (n & (n - 1)) == 0 && (n & 0xaaaaaaaa) == 0;
    }

}