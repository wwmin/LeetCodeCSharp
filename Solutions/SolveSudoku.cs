﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 37. 解数独
/// </summary>
public class SolveSudokuSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { StringTo<List<char[]>>("[[\"5\",\"3\",\".\",\".\",\"7\",\".\",\".\",\".\",\".\"],[\"6\",\".\",\".\",\"1\",\"9\",\"5\",\".\",\".\",\".\"],[\".\",\"9\",\"8\",\".\",\".\",\".\",\".\",\"6\",\".\"],[\"8\",\".\",\".\",\".\",\"6\",\".\",\".\",\".\",\"3\"],[\"4\",\".\",\".\",\"8\",\".\",\"3\",\".\",\".\",\"1\"],[\"7\",\".\",\".\",\".\",\"2\",\".\",\".\",\".\",\"6\"],[\".\",\"6\",\".\",\".\",\".\",\".\",\"2\",\"8\",\".\"],[\".\",\".\",\".\",\"4\",\"1\",\"9\",\".\",\".\",\"5\"],[\".\",\".\",\".\",\".\",\"8\",\".\",\".\",\"7\",\"9\"]]").ToArray() };
            yield return new object[] { StringTo<List<char[]>>("[[\".\",\".\",\"9\",\"7\",\"4\",\"8\",\".\",\".\",\".\"],[\"7\",\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\"],[\".\",\"2\",\".\",\"1\",\".\",\"9\",\".\",\".\",\".\"],[\".\",\".\",\"7\",\".\",\".\",\".\",\"2\",\"4\",\".\"],[\".\",\"6\",\"4\",\".\",\"1\",\".\",\"5\",\"9\",\".\"],[\".\",\"9\",\"8\",\".\",\".\",\".\",\"3\",\".\",\".\"],[\".\",\".\",\".\",\"8\",\".\",\"3\",\".\",\"2\",\".\"],[\".\",\".\",\".\",\".\",\".\",\".\",\".\",\".\",\"6\"],[\".\",\".\",\".\",\"2\",\"7\",\"5\",\"9\",\".\",\".\"]]").ToArray() };
        }
    }
    private bool[,] line = new bool[9, 9];
    private bool[,] column = new bool[9, 9];
    private bool[,,] block = new bool[3, 3, 9];

    private bool valid = false;
    private List<int[]> spaces = new List<int[]>();
    [Data]
    public void SolveSudoku(char[][] board)
    {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (board[i][j] == '.')
                {
                    spaces.Add(new int[] { i, j });
                }
                else
                {
                    int digit = board[i][j] - '0' - 1;
                    line[i, digit] = true;
                    column[j, digit] = true;
                    block[i / 3, j / 3, digit] = true;
                }
            }
        }
        dfs(board, 0);
    }

    //深度优先
    private void dfs(char[][] board, int pos)
    {
        if (pos == spaces.Count)
        {
            valid = true;
            return;
        }
        int[] space = spaces[pos];
        int i = space[0];
        int j = space[1];
        for (int digit = 0; digit < 9 && !valid; ++digit)
        {
            if (!line[i, digit] && !column[j, digit] && !block[i / 3, j / 3, digit])
            {
                line[i, digit] = true;
                column[j, digit] = true;
                block[i / 3, j / 3, digit] = true;
                board[i][j] = (char)(digit + '0' + 1);
                dfs(board, pos + 1);
                line[i, digit] = false;
                column[j, digit] = false;
                block[i / 3, j / 3, digit] = false;
            }
        }
    }


    //仅能在第一遍出现一个确定值时 成功
    //[Data]
    public void SolveSudoku2(char[][] board)
    {
        PrintSodoku(board);
        Console.WriteLine("-------------------");
        //每个格都设成一个字典<(i,i),List<char>(9)>, 按照规则填充每个格的字典,如果字典值个数是1则表示已经确定,从字典值长度大于等于2开始,尝试取一个填入,然后依次确定其他项
        //如果没有任何数字可填入当前空格,则说明前面需要回溯,依次向前回溯,知道回溯到最后一个可能解,最后一组可能解都没有则说明数独不成立
        int noneZeroNum = 0;
        List<xyNums> leftNumsList = new List<xyNums>();
        List<(int i, int j)> nextNums = new List<(int, int)>();
        bool isFirst = true;
        while (true)
        {
            bool finded = false;
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    //if (board[i][j] != '.') grid.Add((i, j), new List<char>() { board[i][j] });
                    if (board[i][j] == '.')
                    {
                        //当前空格的可能数字的合集
                        var nums = leftNums(board, (i, j));
                        if (nums.Count == 1)
                        {
                            //回溯,将当前行,当前列所有可能值都去掉该数
                            board[i][j] = nums[0];
                            if (!isFirst) nextNums.Add((i, j));
                            finded = true;
                            break;
                        }
                        if (isFirst)
                        {
                            leftNumsList.Add(new xyNums { i = i, j = j, nums = nums });
                        };
                    }
                    else
                    {
                        noneZeroNum++;
                    }
                }
                if (finded)
                {
                    break;
                }
            }
            if (finded)
            {
                if (isFirst) leftNumsList.Clear();
                noneZeroNum = 0;
                continue;
            }
            else
            {
                if (noneZeroNum != 9 * 9)
                {
                    for (int i = 0; i < nextNums.Count; i++)
                    {
                        board[nextNums[i].i][nextNums[i].j] = '.';
                    }
                    nextNums.Clear();
                    isFirst = false;
                    //尝试填入不确定的数
                    leftNumsList = leftNumsList.OrderBy(p => p.nums.Count).ToList();
                    if (leftNumsList[0].nums.Count == 0) leftNumsList.RemoveAt(0);
                    if (leftNumsList.Count == 0) return;
                    var c = leftNumsList[0];
                    board[c.i][c.j] = c.nums[0];
                    c.nums.RemoveAt(0);
                }
                else
                {
                    break;
                }
            };
        }

        PrintSodoku(board);
    }

    private class xyNums
    {
        public int i { get; set; }
        public int j { get; set; }
        public List<char> nums { get; set; }
    }

    private List<char> leftNums(char[][] board, (int i, int j) ij)
    {
        var res = Enumerable.Range(0, 9).Select((p, i) => (i + 1).ToString().ToCharArray()[0]).ToList();

        //行
        var row = board[ij.i];
        for (int i = 0; i < 9; i++)
        {
            if (row[i] != '.')
            {
                res.Remove(row[i]);
            }
        }
        //列
        for (int r = 0; r < 9; r++)
        {
            var c = board[r][ij.j];
            if (c != '.')
            {
                res.Remove(c);
            }
        }
        //子数独
        int grid_i = (ij.i / 3) * 3;
        int grid_j = (ij.j / 3) * 3;
        for (int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                var c = board[grid_i + k][grid_j + l];
                if (c != '.')
                {
                    res.Remove(c);
                }
            }
        }
        return res;
    }

    private void PrintSodoku(char[][] board)
    {
        for (int i = 0; i < board.Length; i++)
        {
            for (int j = 0; j < board[i].Length; j++)
            {
                var c = board[i][j];
                if (c == '.') ConsoleHelper.Write(" + ", ConsoleColor.DarkBlue);
                else ConsoleHelper.Write(" " + board[i][j] + " ", ConsoleColor.Green);
            }
            Console.WriteLine();
        }
    }
}
