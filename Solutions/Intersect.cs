namespace Solutions;
/// <summary>
/// 350. 两个数组的交集 II
/// </summary>
public class IntersectSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,2,1]").ToArray(), StringTo<int[]>("[2,2]").ToArray(), StringTo<int[]>("[2,2]").ToArray() };
            yield return new object[] { StringTo<int[]>("[4,9,5]").ToArray(), StringTo<int[]>("[9,4,9,8,4]").ToArray(), StringTo<int[]>("[4,9]").ToArray() };
        }
    }

    [Data]
    public int[] Intersect(int[] nums1, int[] nums2)
    {
        IList<int> ans = new List<int>();
        Dictionary<int, int> map1 = new Dictionary<int, int>();
        Dictionary<int, int> map2 = new Dictionary<int, int>();
        foreach (int num in nums1)
        {
            if (map1.ContainsKey(num)) map1[num]++;
            else map1.Add(num, 1);
        }
        foreach (var num in nums2)
        {
            if (map2.ContainsKey(num)) map2[num]++;
            else map2.Add(num, 1);
        }
        var keys1 = map1.Keys.ToArray();
        foreach (var key1 in keys1)
        {
            if (map2.ContainsKey(key1))
            {
                int maxLength = Math.Min(map1[key1], map2[key1]);
                for (int i = 0; i < maxLength; i++)
                {
                    ans.Add(key1);
                }
            }
        }
        return ans.ToArray();
    }
}