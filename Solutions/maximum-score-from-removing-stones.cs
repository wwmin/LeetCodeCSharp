namespace Solutions;
/// <summary>
/// 1753. 移除石子的最大得分
/// difficulty: Medium
/// https://leetcode.cn/problems/maximum-score-from-removing-stones/
/// </summary>
public class MaximumScore_1753_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 2, 4, 6, 6 };
            yield return new object[] { 4, 4, 6, 7 };
            yield return new object[] { 1, 8, 8, 8 };
        }
    }

    [Data]
    public int MaximumScore(int a, int b, int c)
    {
        if (a + b < c) return a + b;
        if (a + c < b) return a + c;
        if (c + b < a) return b + c;
        return (a + b + c) / 2;
    }
}