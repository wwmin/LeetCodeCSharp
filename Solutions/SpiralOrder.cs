﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 54. 螺旋矩阵
/// </summary>
public class SpiralOrderSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2,3],[4,5,6],[7,8,9]]").ToArray(), StringTo<int[]>("[1,2,3,6,9,8,7,4,5]") };
            yield return new object[] { StringTo<List<int[]>>("[[1,2,3,4],[5,6,7,8],[9,10,11,12]]").ToArray(), StringTo<int[]>("[1,2,3,4,8,12,11,10,9,5,6,7]") };
            yield return new object[] { StringTo<List<int[]>>("[[3],[2]]").ToArray(), StringTo<int[]>("[3,2]") };
        }
    }
    [Data]
    public IList<int> SpiralOrder(int[][] matrix)
    {
        //螺旋方向: 右  下  左  上
        //螺旋方向: i+1 j+1 i-1 j-1
        int m = matrix.Length;
        int n = matrix[0].Length;
        int allN = m * n;
        int[] res = new int[allN];
        int k = 0;
        int left = 0;
        int right = matrix[0].Length - 1;
        int top = 0;
        int bottom = matrix.Length - 1;
        while (allN >= 1)
        {
            for (int i = left; i <= right && allN >= 1; i++)
            {
                res[k++] = matrix[top][i];
                allN--;
            }
            top++;
            for (int i = top; i <= bottom && allN >= 1; i++)
            {
                res[k++] = matrix[i][right];
                allN--;
            }
            right--;
            for (int i = right; i >= left && allN >= 1; i--)
            {
                res[k++] = matrix[bottom][i];
                allN--;
            }
            bottom--;
            for (int i = bottom; i >= top && allN >= 1; i--)
            {
                res[k++] = matrix[i][left];
                allN--;
            }
            left++;
        }

        return res;
    }
}
