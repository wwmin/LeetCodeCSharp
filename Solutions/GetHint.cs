namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 299. 猜数字游戏
/// </summary>
public class GetHintSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "1807", "7810", "1A3B" };
            yield return new object[] { "1123", "0111", "1A1B" };
            yield return new object[] { "1", "0", "0A0B" };
            yield return new object[] { "1", "1", "1A0B" };
        }
    }
    [Data]
    public string GetHint(string secret, string guess)
    {
        int bulls = 0;
        int cows = 0;
        int[] cntS = new int[10];
        int[] cntG = new int[10];
        for (int i = 0; i < secret.Length; i++)
        {
            if (secret[i] == guess[i])
            {
                bulls++;
            }
            else
            {
                cntS[secret[i] - '0']++;
                cntG[guess[i] - '0']++;
            }
        }
        for (int i = 0; i < 10; i++)
        {
            cows += Math.Min(cntS[i], cntG[i]);
        }
        return $"{bulls}A{cows}B";
    }
}