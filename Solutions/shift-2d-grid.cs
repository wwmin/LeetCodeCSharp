namespace Solutions;
/// <summary>
/// 1260. 二维网格迁移
/// difficulty: Easy
/// https://leetcode.cn/problems/shift-2d-grid/
/// </summary>
public class ShiftGrid_1260_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2,3],[4,5,6],[7,8,9]]").ToArray(), 1, StringTo<List<List<int>>>("[[9,1,2],[3,4,5],[6,7,8]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[3,8,1,9],[19,7,2,5],[4,6,11,10],[12,0,21,13]]").ToArray(), 4, StringTo<List<List<int>>>("[[12,0,21,13],[3,8,1,9],[19,7,2,5],[4,6,11,10]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,2,3],[4,5,6],[7,8,9]]").ToArray(), 9, StringTo<List<List<int>>>("[[1,2,3],[4,5,6],[7,8,9]]").ToArray() };
        }
    }

    [Data]
    public IList<IList<int>> ShiftGrid(int[][] grid, int k)
    {
        int m = grid.Length;
        int n = grid[0].Length;
        List<IList<int>> ans = new();
        for (int i = 0; i < m; i++)
        {
            // ans[i] = new List<int>(m);
            ans.Add(new List<int>(n));
            for (int j = 0; j < n; j++)
            {
                // ans[i][j] = grid[i][j];
                ans[i].Add(grid[i][j]);
            }
        }
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                int ii = i;
                int jj = j;
                jj = jj + k;
                int kk = jj / n;
                int ll = jj % n;
                ii = ii + kk;
                jj = ll;
                if (ii >= m)
                {
                    ii = ii % m;
                }
                ans[ii][jj] = grid[i][j];
            }
        }
        return ans;
    }
}