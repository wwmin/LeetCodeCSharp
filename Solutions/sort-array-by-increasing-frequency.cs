namespace Solutions;
/// <summary>
/// 1741. 按照频率将数组升序排序
/// https://leetcode.cn/problems/sort-array-by-increasing-frequency/
/// </summary>
public class FrequencySortSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,1,2,2,2,3]").ToArray(), StringTo<int[]>("[3,1,1,2,2,2]").ToArray() };
            yield return new object[] { StringTo<int[]>("[2,3,1,3,2]").ToArray(), StringTo<int[]>("[1,3,3,2,2]").ToArray() };
            yield return new object[] { StringTo<int[]>("[-1,1,-6,4,5,-6,1,4,1]").ToArray(), StringTo<int[]>("[5,-1,4,4,-6,-6,1,1,1]").ToArray() };
        }
    }

    [Data]
    public int[] FrequencySort(int[] nums)
    {
        Dictionary<int, int> dic = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length; i++)
        {
            if (dic.ContainsKey(nums[i])) dic[nums[i]]++;
            else dic.Add(nums[i], 1);
        }

        return default;
    }
}