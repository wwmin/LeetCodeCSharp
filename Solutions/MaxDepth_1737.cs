namespace Solutions;
/// <summary>
/// 1737. 括号的最大嵌套深度
/// </summary>
public class MaxDepth_1737Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "(1+(2*3)+((8)/4))+1", 3 };
            yield return new object[] { "(1)+((2))+(((3)))", 3 };
            yield return new object[] { "1+(2*3)/(2-1)", 1 };
            yield return new object[] { "1", 0 };
        }
    }

    [Data]
    public int MaxDepth_1737(string s)
    {
        int ans = 0;
        int curDepth = 0;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '(')
            {
                curDepth++;
                if (curDepth > ans) ans = curDepth;
            }
            else if (s[i] == ')')
            {
                curDepth--;
            }
        }
        return ans;
    }
}