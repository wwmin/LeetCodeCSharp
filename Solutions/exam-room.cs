namespace Solutions;
/// <summary>
/// 855. 考场就座
/// difficulty: Medium
/// https://leetcode.cn/problems/exam-room/
/// </summary>
public class ExamRoom
{

    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"ExamRoom\",\"seat\",\"seat\",\"seat\",\"seat\",\"leave\",\"seat\"]", "[[10],[],[],[],[],[4],[]]", "[null,0,9,4,2,null,5]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
    }

    int N;
    SortedSet<int> students; //SortedSet等同于Java的TreeSet
    public ExamRoom(int n)
    {
        this.N = n;
        students = new SortedSet<int>();
    }

    public int Seat()
    {
        int student = 0;
        if (students.Count > 0)
        {
            int dist = students.First();
            int? prev = null;
            foreach (var s in students)
            {
                if (prev != null)
                {
                    int d = (s - prev.Value) / 2;
                    if (d > dist)
                    {
                        dist = d;
                        student = prev.Value + d;
                    }
                }
                prev = s;
            }
            if (N - 1 - students.Last() > dist)
            {
                student = N - 1;
            }
        }
        //将学生添加到集合中
        students.Add(student);
        return student;
    }

    public void Leave(int p)
    {
        students.Remove(p);
    }
}

/**
 * Your ExamRoom object will be instantiated and called as such:
 * ExamRoom obj = new ExamRoom(n);
 * int param_1 = obj.Seat();
 * obj.Leave(p);
 */