namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 367. 有效的完全平方数
/// </summary>
public class IsPerfectSquareSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { 16, true };
            //yield return new object[] { 14, false };
            yield return new object[] { 2147483647, false };
        }
    }
    [Data]
    public bool IsPerfectSquare(int num)
    {
        var n = BinarySearch2(num);
        return n > 0;
    }
    //二分法
    private int BinarySearch(int num)
    {
        if (num == 1) return 1;
        int left = 0;
        int right = num;
        while (left <= right)
        {
            int middle = (right - left) / 2 + left;
            long square = (long)middle * middle;
            if (square == num) return middle;
            if (square > num)
            {
                right = middle - 1;
            }
            else
            {
                left = middle + 1;
            }
            Console.WriteLine(middle);
        }
        return default;
    }

    //二分法
    private int BinarySearch2(int num)
    {
        if (num == 1) return 1;
        int left = 0;
        int right = num;
        while (left <= right)
        {
            int middle = (left + right) / 2;
            long square = (long)middle * middle;
            if (square == num) return middle;
            if (square > num)
            {
                right = middle - 1;
            }
            else
            {
                left = middle + 1;
            }
            Console.WriteLine(middle);
        }
        return default;
    }
}