namespace Solutions;
/// <summary>
/// 852. 适龄的朋友
/// </summary>
public class NumFriendRequestsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[16,16]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[16,17,18]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[20,30,100,110,120]").ToArray(), 3 };
        }
    }

    [Data]
    public int NumFriendRequests(int[] ages)
    {
        int n = ages.Length;
        Array.Sort(ages);
        int left = 0, right = 0, ans = 0;
        foreach (var age in ages)
        {
            if (age < 15) continue;
            while (ages[left] <= 0.5 * age + 7)
            {
                ++left;
            }
            while (right + 1 < n && ages[right + 1] <= age)
            {
                ++right;
            }
            ans += right - left;
        }
        return ans;
    }
}