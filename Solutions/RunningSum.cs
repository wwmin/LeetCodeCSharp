﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 1480. 一维数组的动态和
/// </summary>
public class RunningSumSolution
{

    [InlineData(new int[] { 1, 2, 3, 4 }, new int[] { 1, 3, 6, 10 })]
    public int[] RunningSum(int[] nums)
    {
        int n = nums.Length;
        int[] res = new int[n];
        res[0] = nums[0];
        for (int i = 1; i < n; i++)
        {
            res[i] = SumSub(res[i - 1], nums[i]);
        }
        return res;
    }

    private int SumSub(int sumLeft, int i_value)
    {
        return sumLeft + i_value;
    }
}