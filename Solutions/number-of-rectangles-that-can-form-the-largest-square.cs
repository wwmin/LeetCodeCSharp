namespace Solutions;
/// <summary>
/// 1843. 可以形成最大正方形的矩形数目
/// </summary>
public class CountGoodRectanglesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[5,8],[3,9],[5,12],[16,5]]").ToArray(), 3 };
            yield return new object[] { StringTo<List<int[]>>("[[2,3],[3,7],[4,3],[3,7]]").ToArray(), 3 };
        }
    }

    [Data]
    public int CountGoodRectangles(int[][] rectangles)
    {
        int max = 0;
        int ans = 0;
        foreach (int[] rect in rectangles)
        {
            int edge = Math.Min(rect[0], rect[1]);
            if (edge >= max)
            {
                if (edge == max)
                {
                    ans++;
                }
                else
                {
                    ans = 1;
                    max = edge;
                }
            }
        }
        return ans;
    }
}