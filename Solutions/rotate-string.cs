namespace Solutions;
/// <summary>
/// 812. 旋转字符串
/// </summary>
public class RotateStringSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abcde", "cdeab", true };
            yield return new object[] { "abcde", "abced", false };
        }
    }

    [Data]
    public bool RotateString(string s, string goal)
    {
        if (s.Length != goal.Length) return false;
        if (s.Length == 0) return true;
        var ss = s + s;

        return ss.Contains(goal);
    }
}