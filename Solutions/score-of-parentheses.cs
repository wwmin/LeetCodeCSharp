using wwm.LeetCodeHelper.Servers;

namespace Solutions;
/// <summary>
/// 886. 括号的分数
/// https://leetcode.cn/problems/score-of-parentheses/
/// </summary>
public class ScoreOfParenthesesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "()", 1 };
            yield return new object[] { "(())", 2 };
            yield return new object[] { "()()", 2 };
            yield return new object[] { "(()(()))", 6 };
        }
    }

    [Data]
    public int ScoreOfParentheses(string s)
    {
        Stack<int> st = new Stack<int>();
        st.Push(0);
        foreach (char c in s)
        {
            if (c == '(')
            {
                st.Push(0);
            }
            else
            {
                int v = st.Pop();
                int top = st.Pop() + Math.Max(2 * v, 1);
                st.Push(top);
            }
        }
        return st.Peek();
    }
}