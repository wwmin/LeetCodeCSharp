namespace Solutions;
/// <summary>
/// 91. 解码方法
/// difficulty: Medium
/// https://leetcode.cn/problems/decode-ways/
/// </summary>
public class NumDecodings_91_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "12", 2 };
            yield return new object[] { "226", 3 };
            yield return new object[] { "06", 0 };
        }
    }

    [Data]
    public int NumDecodings(string s)
    {
        int n = s.Length;
        s = ' ' + s;
        char[] cs = s.ToCharArray();
        int[] f = new int[n + 1];
        f[0] = 1;
        for (int i = 1; i <= n; i++)
        {
            //a: 代表【当前位置】单独形成item
            //b: 代表【当前位置】与【前一位置】共同形成item
            int a = cs[i] - '0';
            int b = (cs[i - 1] - '0') * 10 + (cs[i] - '0');
            //如果a属于有效值，那么f[i]可以由f[i-1]转义过来
            if (1 <= a && a <= 9) f[i] = f[i - 1];
            //如果b属于有效值，那么f[i]可以由f[i-2]或者f[i-1]&f[i-2]转义过来
            if (10 <= b && b <= 26) f[i] += f[i - 2];
        }
        return f[n];
    }
}