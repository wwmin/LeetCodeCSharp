namespace Solutions;
/// <summary>
/// 876. 一手顺子
/// </summary>
public class IsNStraightHandSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,6,2,3,4,7,8]").ToArray(), 3, true };
            yield return new object[] { StringTo<int[]>("[8,10,12]").ToArray(), 3, false };
            yield return new object[] { StringTo<int[]>("[1,1,2,2,3,3]").ToArray(), 2, false };
            yield return new object[] { StringTo<int[]>("[1,2,3,4,5]").ToArray(), 4, false };
        }
    }

    [Data]
    public bool IsNStraightHand(int[] hand, int groupSize)
    {
        int n = hand.Length;
        if (n % groupSize != 0)
        {
            return false;
        }
        Array.Sort(hand);
        Dictionary<int, int> cnt = new Dictionary<int, int>();
        foreach (int x in hand)
        {
            if (!cnt.ContainsKey(x))
            {
                cnt.Add(x, 0);
            }
            cnt[x]++;
        }
        foreach (int x in hand)
        {
            if (!cnt.ContainsKey(x))
            {
                continue;
            }
            for (int j = 0; j < groupSize; j++)
            {
                int num = x + j;
                if (!cnt.ContainsKey(num))
                {
                    return false;
                }
                cnt[num]--;
                if (cnt[num] == 0)
                {
                    cnt.Remove(num);
                }
            }
        }
        return true;
    }
}