﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 443. 压缩字符串
/// </summary>
public class CompressSolution
{
    [InlineData(new char[] { 'a' }, 1)]
    [InlineData(new char[] { 'a', 'a', 'b', 'b', 'c', 'c', 'c' }, 6)]
    [InlineData(new char[] { 'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'c', 'c' }, 6)]
    public int Compress(char[] chars)
    {
        int n = chars.Length;
        int read = 0;
        int write = 0;
        int left = 0;
        for (read = 0; read < n; read++)
        {
            if (read == n - 1 || chars[read] != chars[read + 1])
            {
                //写入字符
                chars[write] = chars[read];
                write++;
                //写入数字
                int num = read - left + 1;
                if (num > 1)
                {
                    string ns = num.ToString();
                    for (int j = 0; j < ns.Length; j++)
                    {
                        chars[write] = ns[j];
                        write++;
                    }
                }

                left = read + 1;
            }
        }
        return write;
    }


    //[InlineData(new char[] { 'a' }, 1)]
    //[InlineData(new char[] { 'a', 'a', 'b', 'b', 'c', 'c', 'c' }, 6)]
    //[InlineData(new char[] { 'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b' }, 4)]
    public int Compress_正解(char[] chars)
    {
        int n = chars.Length;
        int read = 0;
        int write = 0;
        int left = 0;
        for (read = 0; read < n; read++)
        {
            if (read == n - 1 || chars[read] != chars[read + 1])
            {
                chars[write] = chars[read];
                write++;
                int num = read - left + 1;
                #region 将数字写入数组中
                if (num > 1)
                {
                    int anchar = write;
                    while (num > 0)
                    {
                        //数字倒序写入,故后面需要反转数组部分
                        chars[write++] = (char)(num % 10 + '0');
                        num /= 10;
                    }
                    Reverse(chars, anchar, write - 1);
                }
                #endregion
                left = read + 1;
            }
        }
        return write;
    }

    private void Reverse(char[] chars, int left, int right)
    {
        while (left < right)
        {
            char temp = chars[left];
            chars[left] = chars[right];
            chars[right] = temp;
            left++;
            right--;
        }
    }



    /// <summary>
    /// 没有原地修改 也不算
    /// </summary>
    /// <param name="chars"></param>
    /// <returns></returns>
    //[InlineData(new char[] { 'a' }, 1)]
    //[InlineData(new char[] { 'a', 'a', 'b', 'b', 'c', 'c', 'c' }, 6)]
    //[InlineData(new char[] { 'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b' }, 4)]
    public int Compress3(char[] chars)
    {
        int newCharLength = 0;
        char curChar = default;
        StringBuilder newChars = new StringBuilder();
        for (int i = 0; i < chars.Length; i++)
        {
            var c = chars[i];
            if (c != curChar)
            {
                curChar = c;
                if (newCharLength > 1)
                {
                    newChars.Append(newCharLength.ToString());
                }
                newChars.Append(c);
                newCharLength = 1;
            }
            else
            {
                newCharLength++;
            }
        }

        if (newCharLength > 1)
        {
            newChars.Append(newCharLength.ToString());
        }
        return newChars.Length;
    }

    /// <summary>
    /// 未修改数组不算
    /// </summary>
    /// <param name="chars"></param>
    /// <returns></returns>
    //[InlineData(new char[] { 'a' }, 1)]
    //[InlineData(new char[] { 'a', 'a', 'b', 'b', 'c', 'c', 'c' }, 6)]
    //[InlineData(new char[] { 'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b' }, 4)]
    public int Compress2(char[] chars)
    {
        int res = 1;
        int newCharIndex = 1;
        char curChar = chars[0];
        for (int i = 1; i < chars.Length; i++)
        {
            if (curChar != chars[i])
            {
                if (newCharIndex > 1)
                {
                    res += newCharIndex.ToString().Length;
                }
                res++;
                curChar = chars[i];
                newCharIndex = 1;
            }
            else
            {
                newCharIndex++;
            }
        }
        if (newCharIndex > 1)
        {
            res += newCharIndex.ToString().Length;
        }
        return res;
    }
}
