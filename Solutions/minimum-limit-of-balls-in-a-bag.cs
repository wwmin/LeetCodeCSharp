namespace Solutions;
/// <summary>
/// 1760. 袋子里最少数目的球
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-limit-of-balls-in-a-bag/
/// </summary>
public class MinimumSize_1760_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[9]").ToArray(), 2, 3 };
            yield return new object[] { StringTo<int[]>("[2,4,8,2]").ToArray(), 4, 2 };
            yield return new object[] { StringTo<int[]>("[7,17]").ToArray(), 2, 7 };
        }
    }

    [Data]
    public int MinimumSize(int[] nums, int maxOperations)
    {
        int left = 1, right = nums.Max();
        int ans = 0;
        while (left <= right)
        {
            int y = (left + right) / 2;
            long ops = 0;
            foreach (int x in nums)
            {
                ops += (x - 1) / y;
            }
            if (ops <= maxOperations)
            {
                ans = y;
                right = y - 1;
            }
            else
            {
                left = y + 1;
            }
        }
        return ans;
    }
}