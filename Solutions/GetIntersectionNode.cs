namespace Solutions;
/// <summary>
/// 160. 相交链表
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int x) { val = x; }
 * }
 */
public class GetIntersectionNodeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("8"), ToListNodeWithString("[4,1,8,4,5]"), ToListNodeWithString("Intersectedat'8'") };
            yield return new object[] { ToListNodeWithString("2"), ToListNodeWithString("[1,9,1,2,4]"), ToListNodeWithString("Intersectedat'2'") };
            yield return new object[] { ToListNodeWithString("0"), ToListNodeWithString("[2,6,4]"), ToListNodeWithString("null") };
        }
    }
    [Data]
    public ListNode GetIntersectionNode(ListNode headA, ListNode headB)
    {
        Dictionary<ListNode, bool> map = new Dictionary<ListNode, bool>();
        while (headA != null)
        {
            map.Add(headA, true);
            headA = headA.next;
        }
        while (headB != null)
        {
            if (map.ContainsKey(headB)) return headB;
            headB = headB.next;
        }
        return null;
    }
}