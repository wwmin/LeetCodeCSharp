namespace Solutions;
/// <summary>
/// 2299. 强密码检验器 II
/// difficulty: Easy
/// https://leetcode.cn/problems/strong-password-checker-ii/
/// </summary>
public class StrongPasswordCheckerII_2299_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "IloveLe3tcode!", true };
            yield return new object[] { "Me+You--IsMyDream", false };
            yield return new object[] { "1aB!", false };
        }
    }

    [Data]
    public bool StrongPasswordCheckerII(string password)
    {
        if (password.Length < 8) return false;
        var special = "!@#$%^&*()-+";
        var hasLower = false;
        var hasUpper = false;
        var hasNumber = false;
        var hasSpecial = false;
        for (int i = 0; i < password.Length; i++)
        {
            if (i > 0 && password[i] == password[i - 1])
            {
                return false;
            }
            if (hasLower == false)
            {
                hasLower = char.IsLower(password[i]);
            }
            if (hasUpper == false)
            {
                hasUpper = char.IsUpper(password[i]);
            }
            if (hasNumber == false)
            {
                hasNumber = char.IsNumber(password[i]);
            }
            if (hasSpecial == false)
            {
                hasSpecial = special.Contains(password[i]);
            }
        }
        return hasLower && hasUpper && hasNumber && hasSpecial;
    }
}