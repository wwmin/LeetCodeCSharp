﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 60. 排列序列
/// </summary>
public class GetPermutationSolution
{
    [InlineData(3, 3, "213")]
    public string GetPermutation(int n, int k)
    {
        int[] arr = Enumerable.Range(0, n).Select((p, i) => i + 1).ToArray();
        List<List<int>> res = new List<List<int>>();
        List<int> curr = new List<int>();
        bool[] used = new bool[n];
        bool finded = false;
        DFS(res, arr, curr, used, k, ref finded);

        return string.Join("", res[res.Count - 1]);
    }

    private void DFS(List<List<int>> res, int[] arr, List<int> curr, bool[] used, int k, ref bool finded)
    {
        if (curr.Count == arr.Length)
        {
            res.Add(new List<int>(curr));
            if (res.Count == k) finded = true;
            return;
        }
        for (int i = 0; i < arr.Length; i++)
        {
            if (finded) break;
            if (used[i]) continue;
            used[i] = true;
            var c = arr[i];
            curr.Add(c);
            DFS(res, arr, curr, used, k, ref finded);
            curr.RemoveAt(curr.Count - 1);
            used[i] = false;
        }
    }
}