namespace Solutions;
/// <summary>
/// 257. 二叉树的所有路径
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class BinaryTreePathsSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,2,3,null,5]"), StringTo<List<string>>("[\"1->2->5\",\"1->3\"]") };
            yield return new object[] { ToTreeNodeWithString("[1]"), StringTo<List<string>>("[\"1\"]") };
        }
    }
    [Data]
    public IList<string> BinaryTreePaths(TreeNode root)
    {
        List<string> ans = new List<string>();
        DFS(root, "", ans);
        return ans;
    }

    private void DFS(TreeNode root, string path, List<string> paths)
    {
        if (root != null)
        {
            path += root.val;
            if (root.left == null && root.right == null)
            {
                //当前是叶子节点
                paths.Add(path);
            }
            else
            {
                path += "->";
                DFS(root.left, path, paths);
                DFS(root.right, path, paths);
            }
        }
    }
}