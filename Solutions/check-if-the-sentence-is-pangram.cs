namespace Solutions;
/// <summary>
/// 1832. 判断句子是否为全字母句
/// difficulty: Easy
/// https://leetcode.cn/problems/check-if-the-sentence-is-pangram/
/// </summary>
public class CheckIfPangram_1832_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "thequickbrownfoxjumpsoverthelazydog", true };
            yield return new object[] { "leetcode", false };
        }
    }

    [Data]
    public bool CheckIfPangram(string sentence)
    {
        HashSet<char> set = new HashSet<char>();
        for (int i = 0; i < sentence.Length; i++)
        {
            set.Add(sentence[i]);
        }

        return set.Count == 26;
    }
}