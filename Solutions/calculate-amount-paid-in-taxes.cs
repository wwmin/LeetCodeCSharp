namespace Solutions;
/// <summary>
/// 2303. 计算应缴税款总额
/// difficulty: Easy
/// https://leetcode.cn/problems/calculate-amount-paid-in-taxes/
/// </summary>
public class CalculateTax_2303_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[3,50],[7,10],[12,25]]").ToArray(), 10, 2.65000 };
            yield return new object[] { StringTo<List<int[]>>("[[1,0],[4,25],[5,50]]").ToArray(), 2, 0.25000 };
            yield return new object[] { StringTo<List<int[]>>("[[2,50]]").ToArray(), 0, 0.00000 };
        }
    }

    [Data]
    public double CalculateTax(int[][] brackets, int income)
    {
        var ans = 0d;
        for (int i = 0; i < brackets.Length; i++)
        {
            var bracket = brackets[i];
            var tax = bracket[1] / 100d;
            var baseIncome = i == 0 ? 0 : brackets[i - 1][0];
            var incomeRange = bracket[0] - baseIncome;
            if (income > incomeRange)
            {
                ans += incomeRange * tax;
                income -= incomeRange;
            }
            else
            {
                ans += income * tax;
                break;
            }
        }
        return ans;
    }
}