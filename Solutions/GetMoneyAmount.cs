namespace Solutions;
/// <summary>
/// 375. 猜数字大小 II
/// </summary>
public class GetMoneyAmountSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 10, 16 };
            yield return new object[] { 1, 0 };
            yield return new object[] { 2, 1 };
        }
    }
    [Data]
    public int GetMoneyAmount(int n)
    {
        int[,] f = new int[n + 1, n + 1];
         return default;
    }
}