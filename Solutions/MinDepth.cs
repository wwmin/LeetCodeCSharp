namespace Solutions;
/// <summary>
/// 111. 二叉树的最小深度
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class MinDepthSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { ToTreeNodeWithString("[3,9,20,null,null,15,7]"), 2 };
            yield return new object[] { ToTreeNodeWithString("[2,null,3,null,4,null,5,null,6]"), 5 };
        }
    }
    [Data]
    public int MinDepth(TreeNode root)
    {
        if (root == null) return 0;
        Queue<TreeNode> queue = new Queue<TreeNode>();
        queue.Enqueue(root);
        int ans = 1;
        while (queue.Count > 0)
        {
            int size = queue.Count;
            for (int i = 0; i < size; i++)
            {
                TreeNode node = queue.Dequeue();
                if (node.left == null && node.right == null)
                {
                    return ans;
                }
                if (node.left != null) queue.Enqueue(node.left);
                if (node.right != null) queue.Enqueue(node.right);
            }
            ans++;
        }
        return ans;
    }
}