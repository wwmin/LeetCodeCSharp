namespace Solutions;
/// <summary>
/// 728. 自除数
/// https://leetcode.cn/problems/self-dividing-numbers/solution/
/// </summary>
public class SelfDividingNumbersSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, 22, StringTo<List<int>>("[1,2,3,4,5,6,7,8,9,11,12,15,22]") };
            yield return new object[] { 47, 85, StringTo<List<int>>("[48,55,66,77]") };
        }
    }

    [Data]
    public IList<int> SelfDividingNumbers(int left, int right)
    {
        List<int> res = new List<int>();
        for (int i = left; i <= right; i++)
        {
            if (isSelfDividingNumber(i))
            {
                res.Add(i);
            }
        }
        return res;
    }

    private bool isSelfDividingNumber(int num)
    {
        int num_new = num;
        while (num_new > 0)
        {
            var m = num_new % 10;
            if (m == 0) return false;
            if (num % m == 0)
            {
                num_new = num_new / 10;
                continue;
            }
            else return false;
        }
        return true;
    }
}