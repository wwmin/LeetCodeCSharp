namespace Solutions;
/// <summary>
/// 940. 水果成篮
/// https://leetcode.cn/problems/fruit-into-baskets/
/// </summary>
public class TotalFruitSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,1]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[0,1,2,2]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[1,2,3,2,2]").ToArray(), 4 };
            yield return new object[] { StringTo<int[]>("[3,3,3,1,2,1,1,2,3,3,4]").ToArray(), 5 };
        }
    }

    [Data]
    public int TotalFruit(int[] fruits)
    {
        int n = fruits.Length;
        Dictionary<int, int> cnt = new Dictionary<int, int>();

        int left = 0, ans = 0;
        for (int right = 0; right < n; right++)
        {
            cnt.TryAdd(fruits[right], 0);
            cnt[fruits[right]]++;
            while (cnt.Count > 2)
            {
                cnt[fruits[left]]--;
                if (cnt[fruits[left]] == 0)
                {
                    cnt.Remove(fruits[left]);
                }
                left++;
            }
            ans = Math.Max(ans, right - left + 1);
        }
        return ans;
    }
}