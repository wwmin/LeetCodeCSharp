namespace Solutions;
/// <summary>
/// 749. 最短补全词
/// </summary>
public class ShortestCompletingWordSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "1s3 PSt", StringTo<string[]>("[\"step\",\"steps\",\"stripe\",\"stepple\"]").ToArray(), "steps" };
            yield return new object[] { "1s3 456", StringTo<string[]>("[\"looks\",\"pest\",\"stew\",\"show\"]").ToArray(), "pest" };
            yield return new object[] { "Ah71752", StringTo<string[]>("[\"suggest\",\"letter\",\"of\",\"husband\",\"easy\",\"education\",\"drug\",\"prevent\",\"writer\",\"old\"]").ToArray(), "husband" };
            yield return new object[] { "OgEu755", StringTo<string[]>("[\"enough\",\"these\",\"play\",\"wide\",\"wonder\",\"box\",\"arrive\",\"money\",\"tax\",\"thus\"]").ToArray(), "enough" };
            yield return new object[] { "iMSlpe4", StringTo<string[]>("[\"claim\",\"consumer\",\"student\",\"camera\",\"public\",\"never\",\"wonder\",\"simple\",\"thought\",\"use\"]").ToArray(), "simple" };
        }
    }

    [Data]
    public string ShortestCompletingWord(string licensePlate, string[] words)
    {
        var map = charMap(licensePlate);
        var keys = map.Keys.ToArray();
        string ans = "";
        for (int i = 0; i < words.Length; i++)
        {
            var word = words[i];
            var word_map = charMap(word);
            //比较两个字典 word_map 是否包含 map
            bool isContain = true;
            for (int j = 0; j < keys.Length; j++)
            {
                if (!word_map.ContainsKey(keys[j])) { isContain = false; break; }
                if (word_map[keys[j]] < map[keys[j]]) { isContain = false; break; }
            }
            if (isContain && (ans.Length == 0 || word.Length < ans.Length)) ans = word;
        }
        return ans;
    }

    private Dictionary<char, int> charMap(string str)
    {
        Dictionary<char, int> map = new Dictionary<char, int>();
        for (int i = 0; i < str.Length; i++)
        {
            var c = str[i];
            if (c == ' ' || (c >= '0' && c <= '9')) continue;
            if (c < 'a') c = (char)(c + 32);
            if (map.ContainsKey(c)) map[c]++;
            else map.Add(c, 1);
        }
        return map;
    }
}