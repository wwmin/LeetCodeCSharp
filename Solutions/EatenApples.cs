using LeetCodeCSharp.Solutions;

namespace Solutions;
/// <summary>
/// 1824. 吃苹果的最大数目 TODO: 使用优先队列
/// </summary>
public class EatenApplesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,5,2]").ToArray(), StringTo<int[]>("[3,2,1,4,2]").ToArray(), 7 };
            yield return new object[] { StringTo<int[]>("[3,0,0,0,0,2]").ToArray(), StringTo<int[]>("[3,0,0,0,0,2]").ToArray(), 5 };
        }
    }

    [Data]
    public int EatenApples(int[] apples, int[] days)
    {
        return default;
    }
}