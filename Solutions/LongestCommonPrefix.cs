﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 14. 最长公共前缀
/// </summary>
public class LongestCommonPrefixSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { new string[] { "flower", "flow", "flight" }, "fl" };
            yield return new object[] { new string[] { "dog", "racecar", "car" }, "" };
        }
    }
    [Data]
    public string LongestCommonPrefix(string[] strs)
    {
        int minLength = strs.Min(p => p.Length);
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i < minLength)
        {
            if (strs.Select(p => p[i]).All(p => p == strs[0][i]))
            {
                sb.Append(strs[0][i]);
            }
            else
            {
                break;
            }
            i++;
        }
        return sb.ToString();
    }
}
