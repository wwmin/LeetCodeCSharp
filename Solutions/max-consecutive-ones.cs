namespace Solutions;
/// <summary>
/// 485. 最大连续 1 的个数
/// </summary>
public class FindMaxConsecutiveOnesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,1,0,1,1,1]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[1,0,1,1,0,1]").ToArray(), 2 };
        }
    }

    [Data]
    public int FindMaxConsecutiveOnes(int[] nums)
    {
        int res = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] == 1)
            {
                int n = 0;
                while (i < nums.Length)
                {
                    if (nums[i] == 0) break;
                    i++;
                    n++;
                }
                res = n > res ? n : res;
            }
        }
        return res;
    }
}