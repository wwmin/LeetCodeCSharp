namespace Solutions;
/// <summary>
/// 1819. 序列中不同最大公约数的数目
/// difficulty: Hard
/// https://leetcode.cn/problems/number-of-different-subsequences-gcds/
/// </summary>
public class CountDifferentSubsequenceGCDs_1819_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[6,10,3]").ToArray(), 5 };
            yield return new object[] { StringTo<int[]>("[5,15,40,5,6]").ToArray(), 7 };
        }
    }

    [Data]
    public int CountDifferentSubsequenceGCDs(int[] nums) {
        int maxVal = nums.Max();
        bool[] occured = new bool[maxVal + 1];
        foreach (int num in nums) {
            occured[num] = true;
        }
        int ans = 0;
        for (int i = 1; i <= maxVal; i++) {
            int subGcd = 0;
            for (int j = i; j <= maxVal; j += i) {
                if (occured[j]) {
                    if (subGcd == 0) {
                        subGcd = j;
                    } else {
                        subGcd = GCD(subGcd, j);
                    }
                    if (subGcd == i) {
                        ans++;
                        break;
                    }
                }
            }
        }
        return ans;
    }

    public int GCD(int num1, int num2) {
        while (num2 != 0) {
            int temp = num1;
            num1 = num2;
            num2 = temp % num2;
        }
        return num1;
    }
}