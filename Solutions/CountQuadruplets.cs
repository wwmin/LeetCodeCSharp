﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 5863. 统计特殊四元组
/// </summary>
public class CountQuadrupletsSolution
{
    [InlineData(new int[] { 1, 2, 3, 6 }, 1)]
    [InlineData(new int[] { 3, 3, 6, 4, 5 }, 0)]
    [InlineData(new int[] { 1, 1, 1, 3, 5 }, 4)]
    public int CountQuadruplets(int[] nums)
    {
        int res = 0;
        int n = nums.Length;
        for (int n1 = 0; n1 < n - 3; n1++)
        {
            for (int n2 = n1 + 1; n2 < n - 2; n2++)
            {
                for (int n3 = n2 + 1; n3 < n - 1; n3++)
                {
                    for (int n4 = n3 + 1; n4 < n; n4++)
                    {
                        if (nums[n1] + nums[n2] + nums[n3] == nums[n4])
                        {
                            res++;
                        }
                    }
                }
            }
        }
        return res;
    }
}
