namespace Solutions;
/// <summary>
/// 2128. 反转单词前缀
/// </summary>
public class ReversePrefixSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abcdefd", 'd', "dcbaefd" };
            yield return new object[] { "xyxzxe", 'z', "zxyxxe" };
            yield return new object[] { "abcd", 'z', "abcd" };
        }
    }

    [Data]
    public string ReversePrefix(string word, char ch)
    {
        int n = word.Length;
        StringBuilder sb = new StringBuilder();
        int maxIndex = -1;
        for (int i = 0; i < word.Length; i++)
        {
            sb.Append(word[i]);
            if (word[i] == ch)
            {
                maxIndex = i;
                break;
            }
        }
        if (maxIndex == -1) return word;
        int mid = maxIndex >> 1;
        for (int i = 0; i <= mid; i++)
        {
            char t = sb[i];
            sb[i] = sb[maxIndex - i];
            sb[maxIndex - i] = t;
        }
        if (maxIndex == n - 1) return sb.ToString();

        return sb.ToString() + word.Substring(maxIndex + 1, n - maxIndex - 1);
    }
}