namespace Solutions;
/// <summary>
/// 419. 甲板上的战舰
/// </summary>
public class CountBattleshipsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<char[]>>("[[\'X\',\'.\',\'.\',\'X\'],[\'.\',\'.\',\'.\',\'X\'],[\'.\',\'.\',\'.\',\'X\']]").ToArray(), 2 };
            yield return new object[] { StringTo<List<char[]>>("[[\'.\']]").ToArray(), 0 };
        }
    }

    [Data]
    public int CountBattleships(char[][] board)
    {
        int ans = 0;
        for (int i = 0; i < board.Length; i++)
        {
            for (int j = 0; j < board[i].Length; j++)
            {
                if (board[i][j] == 'X')
                {
                    bool left = false;
                    bool top = false;
                    if (i > 0)
                    {
                        if (board[i - 1][j] == '.') left = true;
                    }
                    else left = true;
                    if (j > 0)
                    {
                        if (board[i][j - 1] == '.') top = true;
                    }
                    else top = true;
                    if (left == top && left == true)
                    {
                        ans++;
                    }
                }
            }
        }
        return ans;
    }
}