﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 31. 下一个排列
/// </summary>
public class NextPermutationSolution
{
    /*
     例如 2, 6, 3, 5, 4, 1 这个排列， 我们想要找到下一个刚好比他大的排列，
     于是可以从后往前看 我们先看后两位 4, 1 能否组成更大的排列，
     答案是不可以，同理 5, 4, 1也不可以 直到3, 5, 4, 1这个排列，因为 3 < 5， 
     我们可以通过重新排列这一段数字，来得到下一个排列 因为我们需要使得新的排列尽量小，
     所以我们从后往前找第一个比3更大的数字，发现是4 然后，我们调换3和4的位置，
     得到4, 5, 3, 1这个数列 因为我们需要使得新生成的数列尽量小，
     于是我们可以对5, 3, 1进行排序，可以发现在这个算法中，我们得到的末尾数字一定是倒序排列的，
     于是我们只需要把它反转即可 最终，我们得到了4, 1, 3, 5这个数列 完整的数列则是2, 6, 4, 1, 3, 5
     */
    [InlineData(new int[] { 1, 2, 3 }, new int[] { 1, 3, 2 })]
    [InlineData(new int[] { 1, 3, 2 }, new int[] { 2, 1, 3 })]
    [InlineData(new int[] { 2, 3, 1 }, new int[] { 3, 1, 2 })]
    [InlineData(new int[] { 4, 2, 0, 2, 3, 2, 0 }, new int[] { 4, 2, 0, 3, 0, 2, 2 })]
    public int[] NextPermutation(int[] nums)
    {
        if (nums.Length <= 1) return nums;
        //查找下一个更大的数
        int n = nums.Length;
        bool finded = false;
        //向左侧找第一个比右侧临近大的值,一旦找到大的后替换,软后重新排列剩下的
        int left = n - 2;
        while (left >= 0)
        {
            var c = nums[left];
            var cb = nums[left + 1];
            if (c < cb)
            {
                //再从右侧找到第一个比当前值大的,然后进行替换
                int right = n - 1;
                while (left < right)
                {
                    var rc = nums[right];
                    if (rc > c)
                    {
                        //替换
                        nums[left] = rc;
                        nums[right] = c;
                        Array.Sort(nums, left + 1, n - left - 1);
                        break;
                    }
                    right--;
                }
                finded = true;
            }
            if (finded) break;
            left--;
        }
        if (!finded)
        {
            //没找到则降序排列
            Array.Sort(nums);
        }
        return nums;
    }

    /// <summary>
    /// 快速排序
    /// </summary>
    /// <param name="dataArray"></param>
    /// <param name="left"></param>
    /// <param name="right"></param>
    private void QuickSort(int[] dataArray, int left, int right)
    {
        if (left < right)
        {
            int i = left;
            int j = right;
            int x = dataArray[i];
            while (i < j)
            {
                //从右向左找到第一个小于x的数
                while (i < j && dataArray[j] >= x)
                {
                    j--;
                }
                if (i < j) dataArray[i++] = dataArray[j];
                while (i < j && dataArray[i] < x)
                {
                    i++;
                }
                if (i < j)
                {
                    dataArray[j--] = dataArray[i];
                }
            }
            dataArray[i] = x;
            //递归调用
            QuickSort(dataArray, left, i - 1);
            QuickSort(dataArray, i + 1, right);
        }
    }
}
