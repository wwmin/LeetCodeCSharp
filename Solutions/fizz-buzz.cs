namespace Solutions;
/// <summary>
/// 412. Fizz Buzz
/// </summary>
public class FizzBuzzSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, StringTo<List<string>>("[\"1\",\"2\",\"Fizz\"]") };
            yield return new object[] { 5, StringTo<List<string>>("[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\"]") };
            yield return new object[] { 15, StringTo<List<string>>("[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\",\"Fizz\",\"7\",\"8\",\"Fizz\",\"Buzz\",\"11\",\"Fizz\",\"13\",\"14\",\"FizzBuzz\"]") };
        }
    }

    [Data]
    public IList<string> FizzBuzz(int n)
    {
        string[] ans = new string[n];
        for (int i = 1; i <= n; i++)
        {
            bool is3 = false;
            bool is5 = false;
            if (i % 3 == 0)
            {
                is3 = true;
            }
            if (i % 5 == 0)
            {
                is5 = true;
            }
            if (is3 && is5) ans[i - 1] = "FizzBuzz";
            else if (is3) ans[i - 1] = "Fizz";
            else if (is5) ans[i - 1] = "Buzz";
            else ans[i - 1] = i.ToString();
        }
        return ans;
    }
}