namespace Solutions;
/// <summary>
/// 318. 最大单词长度乘积
/// </summary>
public class MaxProductSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"abcw\",\"baz\",\"foo\",\"bar\",\"xtfn\",\"abcdef\"]").ToArray(), 16 };
            yield return new object[] { StringTo<string[]>("[\"a\",\"ab\",\"abc\",\"d\",\"cd\",\"bcd\",\"abcd\"]").ToArray(), 4 };
            yield return new object[] { StringTo<string[]>("[\"a\",\"aa\",\"aaa\",\"aaaa\"]").ToArray(), 0 };
        }
    }
    private Dictionary<char, bool> a_dic = new Dictionary<char, bool>();
    private Dictionary<char, bool> b_dic = new Dictionary<char, bool>();
    [Data]
    public int MaxProduct(string[] words)
    {
        int ans = 0;
        int n = words.Length;
        for (int i = 0; i < n - 1; i++)
        {
            var cur_word = words[i];
            int cur_n = cur_word.Length;
            for (var j = i + 1; j < n; j++)
            {
                var next_word = words[j];
                int next_n = next_word.Length;
                int a = 0;
                a_dic.Clear();
                bool has_repetition = false;
                while (a < cur_n)
                {
                    if (has_repetition) break;
                    int b = 0;
                    b_dic.Clear();
                    var cur_char = cur_word[a];
                    if (a_dic.ContainsKey(cur_char))
                    {
                        a++;
                        continue;
                    }
                    else
                    {
                        a_dic.Add(cur_char, true);
                    };
                    while (b < next_n)
                    {
                        var next_char = next_word[b];
                        if (b_dic.ContainsKey(next_char))
                        {
                            b++;
                            if (b == next_n) break;
                            continue;
                        }
                        else
                        {
                            b_dic.Add(next_char, true);
                        }
                        if (cur_char == next_char) { has_repetition = true; break; }
                        b++;
                    }
                    a++;
                }
                if (has_repetition == false)
                {
                    int len = cur_n * next_n;
                    if (len > ans) ans = len;
                }
            }
        }
        return ans;
    }
}