﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 15. 三数之和
/// </summary>
public class ThreeSumSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { new int[] { -1, 0, 1, 2, -1, -4 }, new List<List<int>> { new List<int> { -1, -1, 2 }, new List<int> { -1, 0, 1 } } };
            //yield return new object[] { new int[] { 0, 0, 0 }, new List<List<int>> { new List<int> { 0, 0, 0 } } };
            //yield return new object[] { new int[] { -1, 0, 1, 2, -1, -4, -2, -3, 3, 0, 4 }, null };
            //yield return new object[] { new int[] { -1, 0, 1, 2, -1 }, null };
            //yield return new object[] { new int[] { -1, 1, 2, -1}, null };
        }
    }
    [Data]
    public IList<IList<int>> ThreeSum(int[] nums)
    {
        IList<IList<int>> res = new List<IList<int>>();
        if (nums.Length < 3) return res;
        var numList = nums.ToList();
        numList.Sort();
        // 最小值大于 0 或者 最大值小于 0，说明没有有效答案
        if (numList[0] > 0 || numList[^1] < 0)
        {
            return res;
        }

        int nl = nums.Length;
        for (int i = 0; i < nl; i++)
        {
            // 如果当前值大于 0，和右侧的值再怎么加也不会等于 0，所以直接退出
            if (numList[i] > 0) return res;
            // 当前循环的值和上次循环的一样，就跳过，避免重复值
            if (i > 0 && numList[i] == numList[i - 1]) continue;
            int left = i + 1;
            int right = nl - 1;
            while (left < right)
            {
                int temp = numList[left] + numList[i] + numList[right];
                if (temp > 0) right--;
                if (temp < 0) left++;
                if (temp == 0)
                {
                    res.Add(new List<int>() { numList[left], numList[i], numList[right] });
                    //跳过相同值
                    while (left < right && numList[left] == numList[left + 1])
                    {
                        left++;
                    }
                    //跳过相同值
                    while (left < right && numList[right] == numList[right - 1])
                    {
                        right--;
                    }
                    left++;
                    right--;
                }
            }
        }
        return res;
    }

    public IList<IList<int>> ThreeSum2(int[] nums)
    {
        IList<IList<int>> res = new List<IList<int>>();
        if (nums.Length < 3) return res;
        var numList = nums.ToList();
        numList.Sort();
        for (int i = 0; i < nums.Length - 2 && numList[i] <= 0; i++)
        {
            //if (nums.Length - i < 3) break;
            for (int j = nums.Length - 1; j > i + 2 && numList[j] >= 0; j--)
            {
                //if (j - i < 2) break;
                //for (int k = i + 1; k < j; k++)
                //{
                //    if (numList[i] + numList[j] + numList[k] == 0)
                //    {
                //        var list = new List<int> { numList[i], numList[j], numList[k] };
                //        list.Sort();
                //        if (res.Any(p => p[0] == list[0] && p[1] == list[1] && p[2] == list[2])) continue;
                //        res.Add(list);
                //    }
                //}
                var n = -(numList[i] + numList[j]);

                var ni = BinarySearch(numList.ToArray(), i + 1, j - 1, n);
                if (ni > -1)
                {
                    var list = new List<int> { numList[i], numList[j], numList[ni] };
                    list.Sort();
                    if (res.Any(p => p[0] == list[0] && p[1] == list[1] && p[2] == list[2])) continue;
                    res.Add(list);
                }
            }
        }
        return res;
    }

    /// <summary>
    /// 二分查找递归实现
    /// </summary>
    /// <param name="arr">数组</param>
    /// <param name="low">开始索引 0</param>
    /// <param name="high">结束索引 </param>
    /// <param name="key">要查找的对象</param>
    /// <returns>返回索引</returns>
    public static int BinarySearch(int[] arr, int low, int high, int key)
    {
        int mid = (low + high) / 2;//中间索引
        if (low > high)
            return -1;
        else
        {
            if (arr[mid] == key)
                return mid;
            else if (arr[mid] > key)
                return BinarySearch(arr, low, mid - 1, key);
            else
                return BinarySearch(arr, mid + 1, high, key);
        }
    }
}
