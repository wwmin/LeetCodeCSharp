namespace Solutions;
/// <summary>
/// 110. 平衡二叉树
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class IsBalancedSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[3,9,20,null,null,15,7]"), true };
            yield return new object[] { ToTreeNodeWithString("[1,2,2,3,3,null,null,4,4]"), false };
            yield return new object[] { ToTreeNodeWithString("[]"), true };
        }
    }
    [Data]
    public bool IsBalanced(TreeNode root)
    {
        if (root == null) return true;
        return Math.Abs(height(root.left) - height(root.right)) <= 1 && IsBalanced(root.left) && IsBalanced(root.right);
    }

    private int height(TreeNode root)
    {
        if (root == null) return 0;
        return Math.Max(height(root.left), height(root.right)) + 1;
    }
}