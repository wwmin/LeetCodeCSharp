namespace Solutions;
/// <summary>
/// 231. 2 的幂
/// </summary>
public class IsPowerOfTwoSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, true };
            yield return new object[] { 16, true };
            yield return new object[] { 3, false };
            yield return new object[] { 4, true };
            yield return new object[] { 5, false };
        }
    }

    [Data]
    public bool IsPowerOfTwo(int n)
    {
        for (int i = 0; i < 32; i++)
        {
            int p = (int)Math.Pow(2, i);
            if (p > n) return false;
            if (p == n) return true;
        }
        return false;
    }
}