﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 23. 合并K个升序链表
/// </summary>
public class MergeKListsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeListWithString("[]").ToArray(), ToListNodeWithString("[]") };
            yield return new object[] { ToListNodeListWithString("[[]]").ToArray(), ToListNodeWithString("[]") };
            yield return new object[] { ToListNodeListWithString("[[1],[2,3]]").ToArray(), ToListNodeWithString("[1,2,3]") };
            yield return new object[] { ToListNodeListWithString("[[1,4,5],[1,3,4],[2,6]]").ToArray(), ToListNodeWithString("[1,1,2,3,4,4,5,6]") };
        }
    }
    [Data]
    public ListNode MergeKLists(ListNode[] lists)
    {

        ListNode lastNotNull = new ListNode();
        MergeLastNotNull(lists, lastNotNull);
        return lastNotNull.next;
    }

    private void MergeLastNotNull(ListNode[] lists, ListNode lastNotNull)
    {
        if (lists.All(p => p == null)) return;
        for (int i = 0; i < lists.Length; i++)
        {
            //查找最小的那个节点
            ListNode minValNode = null;
            int minIndex = -1;
            for (int j = 0; j < lists.Length; j++)
            {
                if (lists[j] == null) continue;
                if (minValNode == null)
                {
                    minValNode = lists[j];
                    minIndex = j;
                }
                else if (minValNode.val > lists[j].val)
                {
                    minValNode = lists[j];
                    minIndex = j;
                }
            }
            if (minIndex == -1) return;
            lists[minIndex] = lists[minIndex].next;
            lastNotNull.next = new ListNode(minValNode.val);
            minValNode = minValNode.next;
            lastNotNull = lastNotNull.next;
            MergeLastNotNull(lists, lastNotNull);
        }
    }
}