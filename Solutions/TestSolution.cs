﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 测试自动执行
/// </summary>
public class TestSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { new Student() { age = 1, name = "a", classNames = new List<string> { "语文", "数学", "英语" } }, new Student() { age = 2, name = "aa", classNames = new List<string> { "语文", "数学", "英语", "综合" } } };
        }
    }

    // [InlineData(0)]
    // [InlineData(0, 1)]
    public int testA(int a)
    {
        return a;
    }



    [Data]
    public Student testB(Student s)
    {
        s.age++;
        s.name = s.name + s.name;
        s.classNames.Add("综合");
        return s;
    }
}

public class Student
{
    public string name { get; set; }
    public int age { get; set; }

    public List<string> classNames { get; set; }
}
