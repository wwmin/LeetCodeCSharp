﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 63. 不同路径 II
/// </summary>
public class UniquePathsWithObstaclesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,0,0,0],[0,1,0,0],[0,0,0,0],[0,0,1,0],[0,0,0,0]]").ToArray(), 7 };
            yield return new object[] { StringTo<List<int[]>>("[[0,0],[1,1],[0,0]]").ToArray(), 0 };
            yield return new object[] { StringTo<List<int[]>>("[[0,1],[1,0]]").ToArray(), 0 };
            yield return new object[] { StringTo<List<int[]>>("[[0]]").ToArray(), 1 };
            yield return new object[] { StringTo<List<int[]>>("[[1,0]]").ToArray(), 0 };
            yield return new object[] { StringTo<List<int[]>>("[[0,0,0],[0,1,0],[0,0,0]]").ToArray(), 2 };
            yield return new object[] { StringTo<List<int[]>>("[[0,1],[0,0]]").ToArray(), 1 };
        }
    }
    [Data]
    public int UniquePathsWithObstacles(int[][] obstacleGrid)
    {
        int m = obstacleGrid.Length;
        int n = obstacleGrid[0].Length;
        int[,] f = new int[m, n];
        for (int i = 0; i < m; i++)
        {
            f[i, 0] = 1;
        }
        for (int j = 0; j < n; j++)
        {
            f[0, j] = 1;
        }
        List<int[]> obs = new List<int[]>();
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (obstacleGrid[i][j] == 1)
                {
                    f[i, j] = 0;
                    //如果障碍物放在了最上边或最嘴边,那么需要把障碍物右面的步骤默认都置为0
                    if (i == 0)
                    {
                        int k = j;
                        while (k < n)
                        {
                            f[i, k++] = 0;
                        }
                    }
                    if (j == 0)
                    {
                        int k = i;
                        while (k < m)
                        {
                            f[k++, j] = 0;
                        }
                    }
                    obs.Add(new int[] { i, j });
                }
            }
        }

        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++)
            {
                bool finded = false;
                for (int k = 0; k < obs.Count; k++)
                {
                    var c = obs[k];
                    if (c[0] == i && c[1] == j) { finded = true; break; }
                }
                if (finded) continue;
                f[i, j] = f[i - 1, j] + f[i, j - 1];
            }
        }
        return f[m - 1, n - 1];
    }
}
