namespace Solutions;
/// <summary>
/// 1824. 最少侧跳次数
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-sideway-jumps/
/// </summary>
public class MinSideJumps_1824_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<int[]>("[0,1,2,3,0]").ToArray(), 2};
            yield return new object[]{StringTo<int[]>("[0,1,1,3,3,0]").ToArray(), 0};
            yield return new object[]{StringTo<int[]>("[0,2,1,0,3,0]").ToArray(), 2};
        }
    }
    const int INF = 0x3fffffff;
    [Data]
    public int MinSideJumps(int[] obstacles)
    {
              int n = obstacles.Length - 1;
        int[] d = new int[3];
        Array.Fill(d, 1);
        d[1] = 0;
        for (int i = 1; i <= n; i++) {
            int minCnt = INF;
            for (int j = 0; j < 3; j++) {
                if (j == obstacles[i] - 1) {
                    d[j] = INF;
                } else {
                    minCnt = Math.Min(minCnt, d[j]);
                }
            }
            for (int j = 0; j < 3; j++) {
                if (j == obstacles[i] - 1) {
                    continue;
                }
                d[j] = Math.Min(d[j], minCnt + 1);
            }
        }
        return Math.Min(Math.Min(d[0], d[1]), d[2]);

  }
}