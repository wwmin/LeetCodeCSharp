namespace Solutions;
/// <summary>
/// 594. 最长和谐子序列
/// </summary>
public class FindLHSSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,3,2,2,5,2,3,7]").ToArray(), 5 };
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[1,1,1,1]").ToArray(), 0 };
            yield return new object[] { StringTo<int[]>("[1,3,5,7,9,11,13,15,17]").ToArray(), 0 };
            yield return new object[] { StringTo<int[]>("[1,2,3,3,1,-14,13,4]").ToArray(), 3 };
        }
    }

    [Data]
    public int FindLHS(int[] nums)
    {
        int ans = 0;
        Array.Sort(nums);
        int begin = 0;
        for (int end = 0; end < nums.Length; end++)
        {
            while (nums[end] - nums[begin] > 1) begin++;
            if (end < nums.Length - 1 && nums[end + 1] == nums[end]) continue;
            if (nums[end] - nums[begin] == 1)
            {
                ans = Math.Max(ans, end - begin + 1);
            }
        }

        return ans;
    }

    //方法1
    //[Data]
    public int FindLHS_1(int[] nums)
    {
        int ans = 0;
        Dictionary<int, int> map = new Dictionary<int, int>();
        int n = nums.Length;
        for (int i = 0; i < n; i++)
        {
            if (map.ContainsKey(nums[i])) map[nums[i]]++;
            else map.Add(nums[i], 1);
        }
        int[] keys = map.Keys.ToArray();
        Array.Sort(keys);
        int map_len = keys.Length;
        for (int i = 0; i < map_len - 1; i++)
        {
            var curr_key = keys[i];
            var next_key = keys[i + 1];
            if (next_key - curr_key > 1) continue;
            ans = Math.Max(ans, map[curr_key] + map[next_key]);
        }

        return ans;
    }
}