﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 18. 四数之和
/// </summary>
public class FourSumSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { new int[] { 0, 0, 0, 0 }, 0, new int[][] { new int[] { 0, 0, 0, 0 } } };
            //yield return new object[] { new int[] { 2, 2, 2, 2, 2 }, 8, new int[][] { new int[] { 2, 2, 2, 2 } } };
            //yield return new object[] { new int[] { 1, 0, -1, 0, -2, 2 }, 0, new int[][] { new int[] { -2, -1, 1, 2 }, new int[] { -2, 0, 0, 2 }, new int[] { -1, 0, 0, 1 } } };
            //yield return new object[] { new int[] { -3, -2, -1, 0, 0, 1, 2, 3 }, 0,
            //    new int[][]{
            //    new int[] { -3, -2, 2, 3 },new int[] { -3, -1, 1, 3 },new int[] { -3, 0, 0, 3 },new int[]{-3,0,1,2 },new int[]{-2,-1,0,3 },
            //    new int[]{-2,-1,1,2 },new int[]{-2,0,0,2 },new int[]{-1,0,0,1 } } };

            yield return new object[] { StringTo<int[]>("[-5,-4,-3,-2,-1,0,0,1,2,3,4,5]"), 0,
                    StringTo<List<List<int>>>("[[-5,-4,4,5],[-5,-3,3,5],[-5,-2,2,5],[-5,-2,3,4],[-5,-1,1,5],[-5,-1,2,4],[-5,0,0,5],[-5,0,1,4],[-5,0,2,3],[-4,-3,2,5],[-4,-3,3,4],[-4,-2,1,5],[-4,-2,2,4],[-4,-1,0,5],[-4,-1,1,4],[-4,-1,2,3],[-4,0,0,4],[-4,0,1,3],[-3,-2,0,5],[-3,-2,1,4],[-3,-2,2,3],[-3,-1,0,4],[-3,-1,1,3],[-3,0,0,3],[-3,0,1,2],[-2,-1,0,3],[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]") };
        }
    }
    [Data]
    public IList<IList<int>> FourSum(int[] nums, int target)
    {
        List<List<int>> res = new List<List<int>>();
        if (nums.Length < 4) return res.ToArray();
        int nl = nums.Length;
        Array.Sort(nums);
        for (int i = 0; i < nl - 3; i++)
        {
            for (int j = i + 1; j < nl - 2; j++)
            {
                //如果最左侧大于0且左侧两个数之和大于目标值,则右侧再怎么加都不会等于target,所以直接退出
                if (nums[i] > 0 && nums[i] + nums[j] > target) return res.ToArray();
                if (j > i + 1 && nums[j] == nums[j - 1]) continue;
                int left = j + 1;
                int right = nl - 1;
                while (left < right)
                {
                    int temp = nums[i] + nums[j] + nums[left] + nums[right];
                    if (temp > target) right--;
                    else if (temp < target) left++;
                    else if (temp == target)
                    {
                        bool noSame = true;
                        for (int k = 0; k < res.Count; k++)
                        {
                            var c = res[k];
                            if (c[0] != nums[i]) continue;
                            if (c[1] != nums[j]) continue;
                            if (c[2] != nums[left]) continue;
                            if (c[3] != nums[right]) continue;
                            noSame = false;
                        }
                        if (noSame) res.Add(new List<int> { nums[i], nums[j], nums[left], nums[right] });
                        //跳过相同值
                        while (left < right && nums[left] == nums[left + 1])
                        {
                            left++;
                        }
                        while (left < right && nums[right] == nums[right - 1])
                        {
                            right--;
                        }
                        left++;
                        right--;
                    }
                }
            }
        }
        return res.ToArray();
    }
}