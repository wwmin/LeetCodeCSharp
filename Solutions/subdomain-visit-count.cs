namespace Solutions;
/// <summary>
/// 829. 子域名访问计数
/// https://leetcode.cn/problems/subdomain-visit-count/
/// </summary>
public class SubdomainVisitsSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"9001 discuss.leetcode.com\"]").ToArray(), StringTo<List<string>>("[\"9001 leetcode.com\",\"9001 discuss.leetcode.com\",\"9001 com\"]") };
            yield return new object[] { StringTo<string[]>("[\"900 google.mail.com\",\"50 yahoo.com\",\"1 intel.mail.com\",\"5 wiki.org\"]").ToArray(), StringTo<List<string>>("[\"901 mail.com\",\"50 yahoo.com\",\"900 google.mail.com\",\"5 wiki.org\",\"5 org\",\"1 intel.mail.com\",\"951 com\"]") };
        }
    }

    [Data]
    public IList<string> SubdomainVisits(string[] cpdomains)
    {
        List<cpclass> cps = new List<cpclass>(cpdomains.Length);
        Dictionary<string, int> domain_visited_dic = new Dictionary<string, int>();
        foreach (var cp in cpdomains)
        {
            var cpc = new cpclass(cp);
            cps.Add(cpc);
            if (cpc.d1 != "")
            {
                if (domain_visited_dic.ContainsKey(cpc.d1)) domain_visited_dic[cpc.d1] += cpc.visited;
                else domain_visited_dic[cpc.d1] = cpc.visited;
            }
            if (cpc.d2 != "")
            {
                if (domain_visited_dic.ContainsKey(cpc.d2)) domain_visited_dic[cpc.d2] += cpc.visited;
                else domain_visited_dic[cpc.d2] = cpc.visited;
            }
            if (cpc.d3 != "")
            {
                if (domain_visited_dic.ContainsKey(cpc.d3)) domain_visited_dic[cpc.d3] += cpc.visited;
                else domain_visited_dic[cpc.d3] = cpc.visited;
            }
        }
        List<string> res = new List<string>();
        foreach (var key in domain_visited_dic.Keys)
        {
            var s = domain_visited_dic[key] + " " + key;
            res.Add(s);
        }
        return res;
    }

    class cpclass
    {
        public cpclass(string s)
        {
            var i = s.IndexOf(' ');
            if (i == -1) return;

            this.visited = Convert.ToInt32(s.Substring(0, i));
            var d = s.Substring(i + 1);
            if (d.Length == 0) return;
            var dd = d.Split('.');
            if (dd.Length == 2)
            {
                this.d3 = dd[1];
                this.d2 = d;
                this.d1 = "";

            }
            else if (dd.Length == 3)
            {
                this.d3 = dd[2];
                this.d2 = dd[1] + "." + dd[2];
                this.d1 = d;
            }
        }

        public int visited { get; set; }

        public string d3 { get; set; }
        public string d2 { get; set; }
        public string d1 { get; set; }
    }
}