namespace Solutions;
/// <summary>
/// 783. 二叉搜索树中的搜索
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class SearchBSTSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[4,2,7,1,3]"), 2, ToTreeNodeWithString("[2,1,3]") };
        }
    }
    private TreeNode ans;
    [Data]
    public TreeNode SearchBST(TreeNode root, int val)
    {
        ans = null;
        DFS(root, val);
        return ans;
    }

    private void DFS(TreeNode node, int val)
    {
        if (ans != null) return;
        if (node == null) return;
        if (node.val == val)
        {
            ans = node;
            return;
        }
        if (node.left != null) DFS(node.left, val);
        if (node.right != null) DFS(node.right, val);
    }
}