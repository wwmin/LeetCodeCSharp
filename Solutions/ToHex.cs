namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 405. 数字转换为十六进制数
/// </summary>
public class ToHexSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 26, "1a" };
            yield return new object[] { -1, "ffffffff" };
        }
    }
    [Data]
    public string ToHex(int num)
    {
        return default;
    }
}