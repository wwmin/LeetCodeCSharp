namespace Solutions;
/// <summary>
/// 463. 岛屿的周长
/// </summary>
public class IslandPerimeterSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]").ToArray(), 16 };
            yield return new object[] { StringTo<List<int[]>>("[[1]]").ToArray(), 4 };
            yield return new object[] { StringTo<List<int[]>>("[[1,0]]").ToArray(), 4 };
        }
    }

    [Data]
    public int IslandPerimeter(int[][] grid)
    {
        var all = 0;
        var edges = 0;
        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[0].Length; j++)
            {
                if (grid[i][j] == 1)
                {
                    all++;
                    //check up
                    if (i > 0 && grid[i - 1][j] == 1)
                    {
                        edges++;
                    }
                    //check right
                    if (j < grid[0].Length - 1 && grid[i][j + 1] == 1)
                    {
                        edges++;
                    }
                    //check down 
                    if (i < grid.Length - 1 && grid[i + 1][j] == 1)
                    {
                        edges++;
                    }
                    //check left
                    if (j > 0 && grid[i][j - 1] == 1)
                    {
                        edges++;
                    }
                }
            }
        }
        return all * 4 - edges;
    }
}