namespace Solutions;
/// <summary>
/// 802. 第 K 个最小的素数分数
/// </summary>
public class KthSmallestPrimeFractionSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,5]").ToArray(), 3, StringTo<int[]>("[2,5]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,7]").ToArray(), 1, StringTo<int[]>("[1,7]").ToArray() };
        }
    }
    [Data]
    public int[] KthSmallestPrimeFraction(int[] arr, int k)
    {
        int n = arr.Length;
        List<int[]> result = new List<int[]>();
        //首先构造所有可能比较数的列表
        //取一个数,然后使用后面所有数作为分母的结果集
        for (int i = 0; i < n; i++)
        {
            for (int j = i + 1; j < n; j++)
            {
                result.Add(new int[] { arr[i], arr[j] });
            }
        }
        /*
         当我们比较两个分数 a/b 和c/d 时,我们可以直接对它们的值进行比较，但这会产生浮点数的计算，降低程序的效率，
         并且可能会引入浮点数误差。一种可行的替代方法是用：
         a*d<b*c
         来替代 a/b 和c/d  的判断，二者是等价的。
         */
        result.Sort((x, y) => x[0] * y[1] - y[0] * x[1]);
        return result[k - 1];
    }
}