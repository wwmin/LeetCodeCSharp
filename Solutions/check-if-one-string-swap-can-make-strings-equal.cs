namespace Solutions;
/// <summary>
/// 1915. 仅执行一次字符串交换能否使两个字符串相等
/// https://leetcode.cn/problems/check-if-one-string-swap-can-make-strings-equal/
/// </summary>
public class AreAlmostEqualSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "bank", "kanb", true };
            yield return new object[] { "attack", "defend", false };
            yield return new object[] { "kelb", "kelb", true };
            yield return new object[] { "abcd", "dcba", false };
        }
    }

    [Data]
    public bool AreAlmostEqual(string s1, string s2)
    {
        if (s1.Length != s2.Length) return false;

        int firstIndex = -1;
        int secondIndex = -1;
        for (int i = 0; i < s1.Length; i++)
        {
            if (s1[i] != s2[i])
            {
                if (secondIndex > -1) return false;
                if (firstIndex > -1) secondIndex = i;
                else firstIndex = i;
            }
        }
        if (secondIndex > -1)
        {
            if (s1[firstIndex] != s2[secondIndex] || s1[secondIndex] != s2[firstIndex]) return false;
        }
        else
        {
            if (firstIndex > -1) return false;
        }
        return true;
    }
}