﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 40. 组合总和 II
/// </summary>
public class CombinationSum2Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[10,1,2,7,6,1,5]"), 8, StringTo<List<int[]>>("[[1,1,6],[1,2,5],[1,7],[2,6]]") };
        }
    }
    [Data]
    public IList<IList<int>> CombinationSum2(int[] candidates, int target)
    {
        Array.Sort(candidates);
        List<IList<int>> res = new List<IList<int>>();
        List<int> curr = new List<int>();

        DFS(target, candidates, res, curr, 0);

        return res.Distinct().ToList();
    }

    private void DFS(int target, int[] candidates, List<IList<int>> res, List<int> curr, int i)
    {
        if (target < 0) return;
        if (target == 0)
        {
            res.Add(new List<int>(curr));
            return;
        }
        for (int start = i; start < candidates.Length; start++)
        {

            var c = candidates[start];
            if (c > target) continue;
            //去重,避免相同的情况筛选两侧,一次原生for循环,一次递归
            if (start > i && candidates[start] == candidates[start - 1]) continue;
            curr.Add(c);
            DFS(target - c, candidates, res, curr, start + 1);
            curr.RemoveAt(curr.Count - 1);
        }
    }
}
