namespace Solutions;
/// <summary>
/// 2139. 检测正方形
/// </summary>
public class DetectSquares
{


    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"DetectSquares\",\"add\",\"add\",\"add\",\"count\",\"count\",\"add\",\"count\"]", "[[],[[3,10]],[[11,2]],[[3,2]],[[11,10]],[[14,8]],[[11,2]],[[11,10]]]", "[null,null,null,null,1,0,null,2]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
    }
    Dictionary<int, Dictionary<int, int>> cnt;
    public DetectSquares()
    {
        cnt = new Dictionary<int, Dictionary<int, int>>();
    }

    public void Add(int[] point)
    {
        int x = point[0];
        int y = point[1];
        if (!cnt.ContainsKey(y))
        {
            cnt.Add(y, new Dictionary<int, int>());
        }
        Dictionary<int, int> yCnt = cnt[y];
        if (!yCnt.ContainsKey(x))
        {
            yCnt.Add(x, 0);
        }
        yCnt[x]++;
    }

    public int Count(int[] point)
    {
        int res = 0;
        int x = point[0];
        int y = point[1];
        if (!cnt.ContainsKey(y))
        {
            return 0;
        }
        Dictionary<int, int> yCnt = cnt[y];
        foreach (KeyValuePair<int, Dictionary<int, int>> pair in cnt)
        {
            int col = pair.Key;
            Dictionary<int, int> colCnt = pair.Value;
            if (col != y)
            {
                // 根据对称性，这里可以不用取绝对值
                int d = col - y;
                int cnt1 = colCnt.ContainsKey(x) ? colCnt[x] : 0;
                int cnt2 = colCnt.ContainsKey(x + d) ? colCnt[x + d] : 0;
                int cnt3 = colCnt.ContainsKey(x - d) ? colCnt[x - d] : 0;
                res += (colCnt.ContainsKey(x) ? colCnt[x] : 0) * (yCnt.ContainsKey(x + d) ? yCnt[x + d] : 0) * (colCnt.ContainsKey(x + d) ? colCnt[x + d] : 0);
                res += (colCnt.ContainsKey(x) ? colCnt[x] : 0) * (yCnt.ContainsKey(x - d) ? yCnt[x - d] : 0) * (colCnt.ContainsKey(x - d) ? colCnt[x - d] : 0);
            }
        }
        return res;
    }
}

/**
 * Your DetectSquares object will be instantiated and called as such:
 * DetectSquares obj = new DetectSquares();
 * obj.Add(point);
 * int param_2 = obj.Count(point);
 */