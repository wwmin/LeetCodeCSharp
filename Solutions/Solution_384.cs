namespace Solutions;
/// <summary>
/// 384. 打乱数组
/// </summary>
public class Solution_384Solution
{
    int[] nums;
    int[] original;

    public Solution_384Solution(int[] nums)
    {
        this.nums = nums;
        this.original = new int[nums.Length];
        Array.Copy(nums, 0, this.original, 0, nums.Length);
    }

    public int[] Reset()
    {
        Array.Copy(original, nums, nums.Length);
        return nums;
    }
    /// <summary>
    /// 洗牌算法
    /// </summary>
    /// <returns></returns>
    public int[] Shuffle()
    {
        int[] shuffled = new int[nums.Length];
        IList<int> list = new List<int>(nums.Length);
        for (int i = 0; i < nums.Length; i++)
        {
            list.Add(nums[i]);
        }
        Random random = new Random();
        for (int i = 0; i < nums.Length; i++)
        {
            //每次随机取一个数,直到list为空
            int j = random.Next(list.Count);
            shuffled[i] = list[j];
            list.Remove(list[j]);
        }
        Array.Copy(shuffled, nums, nums.Length);
        return nums;
    }
}

/**
 * Your SolutionSolution
 object will be instantiated and called as such:
 * SolutionSolution
 obj = new SolutionSolution
(nums);
 * int[] param_1 = obj.Reset();
 * int[] param_2 = obj.Shuffle();
 */