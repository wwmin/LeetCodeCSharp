namespace Solutions;
/// <summary>
/// 1610. 可见点的最大数目
/// difficulty: Hard
/// https://leetcode.cn/problems/maximum-number-of-visible-points/
/// </summary>
public class VisiblePoints_1610_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<List<List<int>>>("[[2,1],[2,2],[3,3]]").ToArray(), 90, StringTo<List<int>>("[1,1]"), 3};
            yield return new object[]{StringTo<List<List<int>>>("[[2,1],[2,2],[3,4],[1,1]]").ToArray(), 90, StringTo<List<int>>("[1,1]"), 4};
            yield return new object[]{StringTo<List<List<int>>>("[[1,0],[2,1]]").ToArray(), 13, StringTo<List<int>>("[1,1]"), 1};
        }
    }

    [Data]
    public int VisiblePoints(IList<IList<int>> points, int angle, IList<int> location)
    {
        return default;
    }
}